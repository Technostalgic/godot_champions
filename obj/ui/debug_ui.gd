class_name DebugUI
extends Control

@export var fps_label: Label

var fps_update_timer: float = 0

func _process(delta: float) -> void:
	handle_fps(delta)

func handle_fps(delta: float) -> void:
	fps_update_timer -= delta
	if fps_update_timer <= 0:
		fps_update_timer = 0.25
		fps_label.text = String.num(Engine.get_frames_per_second(), 2)
