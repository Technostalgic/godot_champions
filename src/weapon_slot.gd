class_name WeaponSlot
extends Node2D

var _equipped_weapons: Array[Weapon] = []
var _equipped_weapons_dirty: bool = true

func  get_equipped_weapons() -> Array[Weapon]:
	if _equipped_weapons_dirty:
		_equipped_weapons.clear()
		for child in get_children():
			if child is Weapon:
				_equipped_weapons.push_back(child)
		_equipped_weapons_dirty = false
	return _equipped_weapons

func _receive_aim(aim_vec: Vector2):
	for wep in get_equipped_weapons():
		wep._receive_aim(aim_vec)

func _receive_use_weapon_primary():
	for wep in get_equipped_weapons():
		wep._receive_trigger()
	
func _receive_use_weapon_secondary():
	for wep in get_equipped_weapons():
		if wep is ModableWeapon:
			wep._receive_alt()

func _receive_reload_weapon():
	for wep in get_equipped_weapons():
		if wep is BasicWeapon:
			wep._receive_reload()

func _on_gain_child(_child: Node):
	_equipped_weapons_dirty = true

func _on_lose_child(_child: Node):
	_equipped_weapons_dirty = true
