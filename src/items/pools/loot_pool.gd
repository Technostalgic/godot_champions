class_name LootPool
extends Resource

@export var entries: Array[PoolEntry] = []
@export_dir var load_directory: String = ""
@export var sub_pools: Array[LootPool] = []

var all_entries: Array[PoolEntry] = []
var total_weight: float = 0

func load_entries():
	all_entries = entries.duplicate(false)
	if load_directory.length() > 0:
		var o_size = all_entries.size()
		print_debug("loading loot pool from ", load_directory)
		var loot_files := DirAccess.get_files_at(load_directory)
		for loot_file in loot_files:
			if loot_file.get_extension() == "remap":
				loot_file = loot_file.substr(0, loot_file.length() - 6)
			var resource := load(load_directory.path_join(loot_file))
			if resource is PackedScene:
				var entry = PoolEntry.new()
				entry.item = resource
				entry.weight = 1
				all_entries.push_back(entry)
		print_debug("Loaded ", all_entries.size() - o_size, " loot items")

func append_subpools():
	for subpool in sub_pools:
		all_entries.append_array(subpool.get_all_entries())

func calculate_offsets():
	var cur_off: float = 0
	for entry in all_entries:
		entry.offset = cur_off
		cur_off += entry.weight
	total_weight = cur_off

func get_all_entries() -> Array[PoolEntry]:
	if total_weight <= 0:
		load_entries()
		append_subpools()
		calculate_offsets()
	return all_entries

func pick_random() -> PackedScene:
	var weighted_index = randf() * total_weight
	for entry in get_all_entries():
		weighted_index -= entry.weight
		if weighted_index <= 0:
			return entry.item
	return null
