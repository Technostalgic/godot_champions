class_name Coin
extends DynamicGravityBody

const COIN_LAYER: int = GlobalClass.CollisionLayer.ETHER_ITEMS
const COIN_MASK: int = (
	GlobalClass.CollisionLayer.TERRAIN |
	GlobalClass.CollisionLayer.HEROS |
	GlobalClass.CollisionLayer.MOBS |
	GlobalClass.CollisionLayer.PROPS
)

static var phys_mat: PhysicsMaterial = null
static var shape: CircleShape2D = null
static var active_coins: Array[Coin] = []
static var pool: Array[Coin] = []
static var pool_cursor: int = -1

@export var value: int = 1
@export var particles: ParticleEffect = null

static func get_phys_mat():
	if phys_mat == null:
		phys_mat = PhysicsMaterial.new()
		phys_mat.bounce = 0.3
	return phys_mat

static func get_shape():
	if shape == null:
		shape = CircleShape2D.new()
		shape.radius = 2
	return shape

# only coins at or below the cursor in the pool can be used, since, if a coin is pooled and 
# then created in the same frame, it will cause the game to crash, so this function should be 
# called consistently exactly one time per tick
static func update_pool_cursor():
	Coin.pool_cursor = Coin.pool.size() - 1

static func clear_active_coins():
	for coin in active_coins:
		coin.remove()
	active_coins.clear()

static func create(val: int = 1) -> Coin:
	var r: Coin = null
	if Coin.pool_cursor >= 0:
		r = Coin.pool[Coin.pool_cursor]
		Coin.pool.remove_at(pool_cursor)
		pool_cursor -= 1
	if r == null:
		r = Global.coin_prefab.instantiate()
	else:
		var parent = r.get_parent()
		if is_instance_valid(parent):
			parent.remove_child(r)
	r.value = val
	r.linear_velocity = Vector2.ZERO
	Coin.active_coins.push_back(r)
	return r

func pick_up(target_inv: Inventory):
	target_inv.gold += value
	particles.burst()
	remove()

func remove():
	var parent = get_parent()
	if is_instance_valid(parent):
		parent.remove_child(self)
		pool.push_back(self)
