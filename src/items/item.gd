class_name Item
extends Node

@export var title: String = "Item"
@export var icon: Texture2D = null
@export var cost: int = 25
@export_multiline var description: String = "It's an item"

func get_stack_size() -> int:
	return 1

func use(actor: Actor) -> void:
	pass
