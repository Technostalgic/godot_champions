class_name ItemContainer
extends DynamicGravityBody

@export var contained_item: Item = null
@export var icon_graphic: Sprite2D = null

func _enter_tree():
	child_entered_tree.connect(_on_child_enter_tree)
	child_exiting_tree.connect(_on_child_exit_tree)

func _on_child_enter_tree(child: Node):
	if child is Item:
		contained_item = child
		update_icon()

func _on_child_exit_tree(child: Node):
	if child is Item:
		if contained_item == child:
			contained_item = null
			update_icon()

func update_icon():
	if !is_instance_valid(contained_item):
		icon_graphic.texture = null
		return
	icon_graphic.texture = contained_item.icon

func set_item(item: Item):
	if is_instance_valid(contained_item):
		contained_item.queue_free()
	add_child(item)

func pick_up(target_inv: Inventory):
	if is_instance_valid(contained_item):
		target_inv.add_item(contained_item)
	else:
		push_error("Item Container contains null or invalid item")
	contained_item = null
	queue_free()
