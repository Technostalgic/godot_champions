class_name Chest
extends DynamicGravityBody

@export var loot_pool: LootPool = null
@export var item_count: int = 3
@export var interactible: Interactible = null
@export var item_container: Node2D = null
@export var sprite: Sprite2D = null
@export var particle_effect: ParticleEffect = null
@export var items_show_tooltip: bool = true

var specific_items: Array[Item] = []
var spawned_items: Array[Interactible] = []
var opened: bool = false
var target_player_id: int = -1

func _ready() -> void:
	interactible.on_interact.connect(_interact)
	if loot_pool == null:
		_gather_specific_items()

func _process(_delta: float) -> void:
	for itm in spawned_items:
		itm.global_rotation = 0

func _interact(actor: Actor):
	if target_player_id >= 0:
		if actor is Hero:
			if actor.player_id != target_player_id:
				return
	if !opened:
		open()

func _item_interacted(actor: Actor, item: Item):
	var inv = Inventory.find_inventory_child(actor)
	if inv != null:
		inv.add_item(item)
	destroy_items()

func _gather_specific_items():
	for child in get_children():
		if child is Item:
			specific_items.push_back(child)
	if specific_items.size() > 0:
		item_count = specific_items.size()
		loot_pool = null

func open():
	opened = true
	spawn_items()
	sprite.frame = 1
	interactible.queue_free()
	particle_effect.queue_free()

func create_item() -> Interactible:
	var inter = Interactible.new()
	var col = Area2D.new()
	var shape = CircleShape2D.new()
	var shape_col = CollisionShape2D.new()
	shape.radius = 4
	shape_col.shape = shape
	col.add_child(shape_col)
	col.collision_mask = interactible.collider.collision_mask
	inter.add_child(col)
	inter.collider = col
	inter.show_interaction_tooltip = items_show_tooltip
	var itm: Item = null
	if loot_pool == null:
		itm = specific_items.pick_random()
		specific_items.erase(itm)
	else: 
		itm = loot_pool.pick_random().instantiate() as Item
	var itm_spr := ItemSprite.create_from(itm)
	inter.add_child(itm_spr)
	inter.on_interact.connect(_item_interacted.bind(itm))
	inter.on_interact_hold.connect(itm.use)
	return inter

func spawn_items():
	var spacing: float = 24
	var x_off = spacing * (item_count - 1) * 0.5
	for i in range(item_count):
		var itm = create_item()
		itm.position.x = i * spacing - x_off
		itm.position.y = -6.0 * (1.0 if i % 2 == 0 else 1.5)
		spawned_items.push_back(itm)
		item_container.add_child(itm)

func destroy_items():
	for item in spawned_items:
		item.queue_free()
	spawned_items.clear()
