extends Consumable

@export var recover_percent: float = 0.65

func use(actor: Actor):
	super.use(actor)
	var wep := ModableWeapon.get_weapon(actor)
	if wep != null:
		var max_ammo := wep.get_weapon_stats().max_ammo
		wep.current_ammo += max_ammo * recover_percent
		if wep.current_ammo > max_ammo:
			wep.current_ammo = max_ammo
