class_name Consumable
extends Item

signal used(actor: Actor)

@export var auto_use: bool = true

func use(user: Actor):
	used.emit(user)
	queue_free()
