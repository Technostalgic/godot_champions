class_name ItemSprite
extends Sprite2D

static func create_from(itm: Item) -> ItemSprite:
	var spr = ItemSprite.new()
	spr.item = itm
	return spr

var show_label: bool :
	get: return _show_label
	set(b):
		_show_label = b
		if _label.visible != b:
			if b: _label.show()
			else: _label.hide()

var item: Item :
	get: return _item
	set(i):
		if i == null:
			if is_instance_valid(_item):
				_item.queue_free()
				_item = null
			return
		var itm_parent = i.get_parent()
		if is_instance_valid(itm_parent):
			if itm_parent != self:
				i.reparent(self)
		else: add_child(i)

var _item: Item = null
var _label: Label = null
var _show_label: bool = false

func _init():
	_create_label()
	scale = Vector2.ONE * 0.25

func _enter_tree() -> void:
	child_entered_tree.connect(_child_entered_tree)
	child_exiting_tree.connect(_child_exiting_tree)

func _child_entered_tree(child: Node) -> void:
	if child is Item:
		if is_instance_valid(item):
			push_error("Item already exists in item sprite")
			item.queue_free()
		_item = child
		_refresh_icon()
		_refresh_label()

func _child_exiting_tree(child: Node) -> void:
	if _item == child:
		_item = null
		_refresh_icon()
		_refresh_label()

func _create_label():
	_label = Label.new()
	add_child(_label)
	_label.size.x = 90
	_label.autowrap_mode = TextServer.AUTOWRAP_WORD
	_label.set_anchors_preset(Control.PRESET_CENTER_BOTTOM)
	_label.horizontal_alignment = HORIZONTAL_ALIGNMENT_CENTER
	_label.position.x = -75
	_label.position.y = -65
	_label.add_theme_constant_override("line_spacing", -6)

func _refresh_icon():
	if !is_instance_valid(_item):
		texture = null
		return
	texture = _item.icon

func _refresh_label():
	if !is_instance_valid(_item):
		_label.hide()
		return
	_label.text = _item.title
