extends Consumable

@export var recover_percent: float = 0.3

func use(actor: Actor):
	super.use(actor)
	var health := Health.get_health(actor)
	if health != null && health.health < health.max_health:
		health.health += recover_percent * health.max_health
		if health.health > health.max_health:
			health.health = health.max_health
