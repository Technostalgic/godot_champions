class_name Mob
extends Actor

@export_storage var scene_parent: PackedScene = null
var wave_credit_cost: float = 0

func _enter_tree() -> void:
	linear_velocity = Vector2.ZERO
	angular_velocity = 0
	dead = false

func is_alive():
	return !dead

func remove():
	remove_wounds()
	dead = true
	MobManager.pool_mob(self)

func kill():
	spawn_coins()
	on_kill.emit()
	dead = true

func spawn_coins():
	var gold: int = 1 + roundi(wave_credit_cost * 2 * randf())
	while gold > 0:
		var val = 1
		if gold > 5 && gold % 2 == 0:
			val = 5
		gold -= val
		var coin = Coin.create(val)
		var rand_vel = Vector2.from_angle(randf_range(-PI, PI)) * randf() * 150
		coin.global_position = global_position
		Global.root_2d_world.add_child(coin)
		coin.linear_velocity = linear_velocity + rand_vel
