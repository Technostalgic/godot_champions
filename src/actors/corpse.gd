class_name Corpse
extends Actor

## the percent of health that the corpse has compared to the actor it was spawned from
@export var corpse_health_factor: float = 0.5
@export_storage var parent_scene: PackedScene = null

var parent_actor: Actor = null
var max_lifetime: float = 5
var lifetime: float = 0.0

func _ready() -> void:
	on_hit.connect(_on_hit)
	pass

func _enter_tree() -> void:
	super._enter_tree()
	lifetime = 0
	modulate.a = 1.0

func _process(delta: float) -> void:
	var fade_time := 1.5
	lifetime += delta
	if lifetime > max_lifetime - fade_time:
		var fade_delta := (lifetime - (max_lifetime - fade_time)) / fade_time
		modulate.a = 1 - fade_delta
		if lifetime > max_lifetime:
			remove()

func _on_hit(_proj: Projectile, _damage: float, _point: Vector2) -> void:
	lifetime = 0
	modulate.a = 1

func spawn_from(actor: Actor) -> void:
	parent_actor = actor
	actor.get_parent().add_child(self)
	global_position = actor.global_position
	global_rotation = actor.global_rotation
	global_scale = actor.global_scale
	reset_physics_interpolation()
	linear_velocity = actor.linear_velocity
	angular_velocity = actor.angular_velocity
	var actor_wound_count := actor.wounds.size()
	for i in actor_wound_count:
		var index := actor_wound_count - i - 1
		steal_wound(index, actor)
	if is_instance_valid(health):
		health.max_health = actor.health.max_health * corpse_health_factor
		health.health = health.max_health
	if is_instance_valid(actor._last_projectile_struck):
		hit(actor._last_projectile_struck, 0, global_transform * actor._last_struck_pos)

func remove():
	remove_wounds()
	CorpseManager.pool_corpse(self)
