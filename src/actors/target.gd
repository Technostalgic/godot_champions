class_name Target
extends Actor

class RespawnContainer:
	extends Node
	var target: Target = null
	var timer: float = 0
	
	func _physics_process(delta: float):
		timer -= delta
		if timer <= 0:
			respawn()
	
	func respawn():
		if target != null:
			target.linear_velocity = Vector2.ZERO
			add_sibling(target)
			target = null

@export var respawn_time: float = 1.5

var container: RespawnContainer = null

func _ready():
	container = RespawnContainer.new()
	super._ready()

func remove():
	remove_wounds()
	container.target = self
	container.timer = respawn_time
	if !container.is_inside_tree():
		add_sibling(container)
	get_parent().remove_child(self)
	health.health = health.max_health

func is_alive() -> bool:
	return is_inside_tree()
