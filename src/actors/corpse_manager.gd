extends Node

@export var all_corpses: Array[PackedScene] = []

## Dictionary<PackedScene, Corpse>
var _corpse_map: Dictionary = {}

## Dictionary<PackedScene, Array[Corpse]>
var _corpse_pools: Dictionary = {}

func _create_corpse_map():
	_corpse_map.clear()
	for corpse_scene in all_corpses:
		if _corpse_map.has(corpse_scene):
			push_error("mob instance already exists")
			continue
		var corpse := corpse_scene.instantiate() as Corpse
		corpse.parent_scene = corpse_scene
		_corpse_map[corpse_scene] = corpse

func clear_corpses() -> void:
	for pool in _corpse_pools:
		for corpse in _corpse_pools[pool]:
			if is_instance_valid(corpse):
				corpse.queue_free()
	_corpse_pools.clear()

func get_base_corpse(corpse_scene: PackedScene) -> Corpse:
	if _corpse_map.is_empty():
		_create_corpse_map()
	if !_corpse_map.has(corpse_scene):
		push_error("scene is not included in corpse manager")
		return null
	return _corpse_map[corpse_scene]

func pick_corpse(corpse_scene: PackedScene) -> Corpse:
	if !_corpse_pools.has(corpse_scene):
		return get_base_corpse(corpse_scene).duplicate()
	var pool := _corpse_pools[corpse_scene] as Array[Corpse]
	if pool.size() <= 0:
		return get_base_corpse(corpse_scene)
	return pool.pop_back() as Corpse

func pool_corpse(corpse: Corpse) -> void:
	if corpse.parent_scene == null:
		push_error("corpse was not picked from pool")
		corpse.queue_free()
	if !_corpse_pools.has(corpse.parent_scene):
		_corpse_pools[corpse.parent_scene] = [] as Array[Corpse]
	var pool := _corpse_pools[corpse.parent_scene] as Array[Corpse]
	if corpse.is_inside_tree():
		corpse.get_parent().remove_child(corpse)
	pool.push_back(corpse)
