extends Node

@export var all_mobs: Array[MobInfo] = []
@export var mob_spawn_fx: PackedScene = null

## Dictionary<int, Array[Mob]>
var _mob_flag_map: Dictionary = {}

## Dictionary<PackedScene, Mob>
var _mob_map: Dictionary = {}

## Dictionary<PackedScene, Array[Mob]>
var _mob_pools: Dictionary = {}

func _ready() -> void:
	_create_mob_map()

func _create_mob_map():
	_mob_map.clear()
	for mob_inf in all_mobs:
		if _mob_map.has(mob_inf):
			push_error("mob instance already exists")
			continue
		var mob := mob_inf.mob_scene.instantiate()
		mob.scene_parent = mob_inf.mob_scene
		_mob_map[mob_inf.mob_scene] = mob

func clear_mobs() -> void:
	for pool in _mob_pools:
		for mob in _mob_pools[pool]:
			if is_instance_valid(mob):
				mob.queue_free()
	_mob_pools.clear()

func get_base_mob(mob_scene: PackedScene) -> Mob:
	if _mob_map.is_empty():
		_create_mob_map()
	if !_mob_map.has(mob_scene):
		push_error("scene is not included in mob manager")
		return null
	return _mob_map[mob_scene]

func pick_mob(mob_scene: PackedScene) -> Mob:
	if !_mob_pools.has(mob_scene):
		return get_base_mob(mob_scene).duplicate()
	var pool := _mob_pools[mob_scene] as Array[Mob]
	if pool.size() <= 0:
		return get_base_mob(mob_scene).duplicate()
	return pool.pop_back()

func pool_mob(mob: Mob) -> void:
	if mob.scene_parent == null:
		push_error("mob was not picked from pool")
		mob.queue_free()
	if !_mob_pools.has(mob.scene_parent):
		_mob_pools[mob.scene_parent] = [] as Array[Mob]
	var pool := _mob_pools[mob.scene_parent] as Array[Mob]
	if mob.is_inside_tree():
		mob.get_parent().remove_child(mob)
	pool.push_back(mob)

func get_filter_flag_mobs(
	incl_flags: MobFlags.Flag, 
	excl_flags: MobFlags.Flag, 
	filter_pool: Array[MobInfo] = all_mobs
) -> Array[MobInfo]:
	var use_all_mobs: bool = filter_pool == all_mobs
	var key: int = 0 if !use_all_mobs else incl_flags | (excl_flags << 32)
	if !use_all_mobs || !_mob_flag_map.has(key):
		var arr: Array[MobInfo] = []
		for mob_inf in filter_pool:
			if mob_inf.mob_flags.contains_any_flags(incl_flags):
				if !mob_inf.mob_flags.contains_any_flags(excl_flags):
					arr.push_back(mob_inf)
		if use_all_mobs:
			_mob_flag_map[key] = arr
		else:
			return arr
	return _mob_flag_map[key] as Array[MobInfo]
