class_name Hero
extends Actor

@export var collider: CollisionShape2D = null
@export var input_behavior: InputBehavior = null
@export var interactor: Interactor = null
@export var weapon_slot: WeaponSlot = null
@export var shooting: Shooting = null

var player_id: int = 0
var color: Color = Color.WHITE

func get_equipped_weapon() -> Weapon:
	if is_instance_valid(weapon_slot):
		if weapon_slot.get_equipped_weapons().size() > 0:
			return weapon_slot.get_equipped_weapons()[0]
	return null

func configure(player_config: GlobalClass.PlayerConfig):
	player_id = player_config.player_id
	color = GlobalClass.PLAYER_COLORS[player_id]
	color.a = 0.5
	collider.debug_color = color
	input_behavior.set_controller_id(player_config.controller_id)
	input_behavior.set_ui_player_id(player_config.player_id)

func is_alive() -> bool:
	return !dead || health.health > 0

func kill():
	var was_alive = is_alive()
	super.kill()
	if was_alive:
		remove()

func remove():
	remove_wounds()
	var parent = get_parent()
	if is_instance_valid(parent):
		dead = true
		parent.remove_child(self)
