class_name HitEffect
extends Node

class EffectInfo:
	# whether or not the projectile should be kept alive
	var keep_alive: bool = false
	# if the effect loop should be broken, keeping any other effects from happening
	var break_effects: bool = false
	# if the projectile should ignore the collision for this object
	var ignore_collision: bool = false
	
	static func FALSE() -> EffectInfo: return EffectInfo.new()
	
	static func TRUE() -> EffectInfo:
		var m := EffectInfo.new()
		m.keep_alive = true
		m.break_effects = true
		m.ignore_collision = true
		return m
	
	static func KEEP_ALIVE_AND_BREAK() -> EffectInfo:
		var m := EffectInfo.new()
		m.keep_alive = true
		m.break_effects = true
		return m

class StatString:
	static func create(lbl: String, val: String) -> HitEffect.StatString:
		var r = HitEffect.StatString.new()
		r.label = lbl
		r.value = val
		return r
	var label: String = ""
	var value: String = ""

var level: int = 0
var parent_proj: Projectile = null

func _enter_tree():
	find_parent_proj()
	if is_instance_valid(parent_proj):
		parent_proj.hit_effects.push_back(self)

func _exit_tree():
	if is_instance_valid(parent_proj):
		parent_proj.hit_effects.erase(self)

# override to set the weapon mod level of the effect
func set_level(lvl: int):
	level = lvl

# override to be called whenever the projectile is fired
func reset(_proj: Projectile):
	pass

# override to do hit effect and return true if projectile should be kept alive, 
# otherwise return false
func hit_effect(
	_proj: Projectile, _obj: CollisionObject2D, _point: Vector2, _normal: Vector2
) -> EffectInfo:
	return EffectInfo.new()

func find_parent_proj():
	var parent = get_parent()
	if parent is WeaponMod:
		set_level(parent.get_stack_size())
		parent.hit_effects.push_back(self)
		parent_proj = null
	else:
		parent_proj = get_parent() as Projectile

func get_stat_strings() -> Array[StatString]:
	return []
