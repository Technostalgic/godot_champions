class_name DamageAura
extends HitEffect

@export var aura_prefab: PackedScene = null
@export var additional_damage: float = 16
@export var additional_radius: float = 5
@export var additional_damage_increment: float = 10

var target_proj: Projectile = null
var aura: Area2D = null
var aura_targets: Array[ProjectileTarget] = []
var aura_damage_add: float = 0

func _enter_tree():
	super._enter_tree()
	if target_proj == null:
		var pot_proj = get_parent()
		if pot_proj is Projectile:
			target_proj = pot_proj
	if aura == null:
		create_aura()

func _exit_tree():
	super._exit_tree()
	aura_targets.clear()

func _physics_process(delta: float):
	if !is_instance_valid(target_proj):
		return
	var dps: float = target_proj.proj_stats.aoe_damage + aura_damage_add
	for target in aura_targets:
		target.apply_damage(target_proj, dps * delta, target_proj.global_position)

func _on_enter_aura(body: Node2D):
	if body is ProjectileTarget:
		aura_targets.push_back(body)

func _on_exit_aura(body: Node2D):
	if body is ProjectileTarget:
		aura_targets.erase(body)

func set_level(lvl: int):
	super.set_level(lvl)
	aura_damage_add = additional_damage + (additional_damage_increment * (lvl - 1))

func reset(proj: Projectile):
	target_proj = proj
	aura_targets.clear()
	super.reset(proj)

func create_aura():
	if target_proj is Projectile:
		aura = aura_prefab.instantiate() as Area2D
		target_proj.add_child.call_deferred(aura)
		aura.scale = Vector2.ONE * (target_proj.proj_stats.aoe_size + 10)
		aura.body_entered.connect(_on_enter_aura)
		aura.body_exited.connect(_on_exit_aura)
		aura.collision_layer = 1 << 5
		aura.collision_mask = 12

func get_stat_strings() -> Array[HitEffect.StatString]:
	return [
		HitEffect.StatString.create("Aura Dmg", str(aura_damage_add))
	]

