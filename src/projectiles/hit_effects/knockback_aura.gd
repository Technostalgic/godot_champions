class_name KnockbackAura
extends HitEffect

@export var aura_prefab: PackedScene = null
@export var additional_radius: float = 5

var target_proj: Projectile = null
var aura: Area2D = null
var aura_targets: Array[ProjectileTarget] = []

func _enter_tree():
	super._enter_tree()
	if target_proj == null:
		var pot_proj = get_parent()
		if pot_proj is Projectile:
			target_proj = pot_proj
	if aura == null:
		create_aura()

func _exit_tree():
	super._exit_tree()
	aura_targets.clear()

func _physics_process(_delta: float):
	if !is_instance_valid(target_proj):
		return
	var kb := target_proj.proj_stats.knockback
	for target in aura_targets:
		var dir := target.global_position - target_proj.global_position
		if target_proj is DynamicProjectile:
			if target_proj.velocity.length_squared() > 0:
				dir = target_proj.velocity
		dir = dir.normalized()
		target.apply_central_force(dir * kb * 20)

func _on_enter_aura(body: Node2D):
	if body is ProjectileTarget:
		aura_targets.push_back(body)

func _on_exit_aura(body: Node2D):
	if body is ProjectileTarget:
		aura_targets.erase(body)

func reset(proj: Projectile):
	target_proj = proj
	aura_targets.clear()
	super.reset(proj)

func create_aura():
	if target_proj is Projectile:
		aura = aura_prefab.instantiate() as Area2D
		target_proj.add_child.call_deferred(aura)
		aura.scale = Vector2.ONE * (target_proj.proj_stats.aoe_size + 8)
		aura.body_entered.connect(_on_enter_aura)
		aura.body_exited.connect(_on_exit_aura)
		aura.collision_layer = 1 << 5 # hero aoe
		aura.collision_mask = 1 << 2 | 1 << 3 # mobs and props
