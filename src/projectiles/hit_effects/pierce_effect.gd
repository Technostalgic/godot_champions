extends HitEffect

@export var pierce_count: int = 1

var total_pierces: int = 0
var objects_pierced: Array[CollisionObject2D]
var pierces_left: int = 0

func set_level(lvl: int):
	super.set_level(lvl)
	total_pierces = pierce_count + lvl - 1

func reset(_proj: Projectile):
	pierces_left = total_pierces
	objects_pierced.clear()

func hit_effect(
	proj: Projectile, obj: CollisionObject2D, point: Vector2, _normal: Vector2
) -> HitEffect.EffectInfo:

	if obj is ProjectileTarget:
		
		# if the collider has not yet been hit by the prokectile, hit it
		if !objects_pierced.has(obj):
			pierces_left -= 1
			if pierces_left >= 0:
				proj.global_position = point
				if proj is Bullet:
					proj.reset_positioning()
					proj.orient_collision()
				objects_pierced.push_back(obj)
				return HitEffect.EffectInfo.KEEP_ALIVE_AND_BREAK()

		# if it's only colliding with an object that's already been pierced, 
		# ignore the collision completely
		else:
			return HitEffect.EffectInfo.TRUE()

	return HitEffect.EffectInfo.FALSE()

func get_stat_strings() -> Array[HitEffect.StatString]:
	return [
		HitEffect.StatString.create("Pierces", str(total_pierces))
	]
