extends HitEffect

@export var additional_life: float = 1.5

var total_additional_life: float = 0

func set_level(lvl: int):
	super.set_level(lvl)
	total_additional_life = additional_life + (0.5 * (lvl - 1))

func reset(proj: Projectile):
	proj.total_max_lifetime += total_additional_life

func hit_effect(
	_proj: Projectile, _obj: CollisionObject2D, _point: Vector2, _normal: Vector2
) -> HitEffect.EffectInfo:
	return HitEffect.EffectInfo.TRUE()

func get_stat_strings() -> Array[HitEffect.StatString]:
	return [
		HitEffect.StatString.create("Extra Time", str(total_additional_life))
	]
