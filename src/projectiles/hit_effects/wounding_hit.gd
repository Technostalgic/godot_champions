extends HitEffect

@export var wound_sets: Array[WoundSet]
@export var min_wound_fraction: float = 1

## Dictionary<Wound.Type, Array[PackedScene]>
var _wound_map: Dictionary = {} :
	get:
		if _wound_map.is_empty():
			_wound_map = Wounds.create_map_from_sets(wound_sets)
		return _wound_map

func get_wound(type: Wound.Type) -> Wound:
	return Wounds.pick_wound_from_map(_wound_map, type)

func hit_effect(
	proj: Projectile, obj: CollisionObject2D, point: Vector2, normal: Vector2
) -> HitEffect.EffectInfo:
	if obj is ProjectileTarget:
		if obj.wound_fraction() >= min_wound_fraction:
			var wound := get_wound(obj.wound_type)
			if wound != null:
				obj.add_wound(wound, proj, point, normal)
	return HitEffect.EffectInfo.new()
