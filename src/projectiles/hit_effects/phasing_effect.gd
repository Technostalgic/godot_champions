extends HitEffect

@export var phase_dist: float = 16

var total_phase_dist

func set_level(lvl: int):
	super.set_level(lvl)
	total_phase_dist = phase_dist * lvl

func hit_effect(
	proj: Projectile, obj: CollisionObject2D, point: Vector2, normal: Vector2
) -> HitEffect.EffectInfo:
	
	if obj is ProjectileTarget:
		return HitEffect.EffectInfo.FALSE()
	
	if proj is DynamicProjectile:
		var dir = proj.velocity.normalized()
		var target_pos = point + dir * total_phase_dist
		
		var space_state = PhysicsServer2D.space_get_direct_state(proj.get_world_2d().space)
		var ray = PhysicsRayQueryParameters2D.create(
			target_pos,
			point,
			1 << 0)
		ray.hit_from_inside = true
		var result = space_state.intersect_ray(ray)
		if !result.is_empty():
			if result.normal.length_squared() <= 0.5:
				return HitEffect.EffectInfo.FALSE()
		
			var pos: Vector2 = result.position + dir
			proj.global_position = pos
			if proj is Bullet:
				proj.reset_positioning()
				proj.orient_collision()
			return HitEffect.EffectInfo.KEEP_ALIVE_AND_BREAK()
	
	return HitEffect.EffectInfo.FALSE()

func get_stat_strings() -> Array[HitEffect.StatString]:
	return [
		HitEffect.StatString.create("Phase Dist", str(total_phase_dist))
	]
