class_name StickyEffect
extends HitEffect

var _projectile: Projectile = null
var _stuck_pos: Vector2 = Vector2.ZERO
var _stuck_vel: Vector2 = Vector2.ZERO
var stuck_collider: CollisionObject2D = null
var stuck_wound: ContainerWound = null :
	set(value):
		if is_instance_valid(stuck_wound) && value != stuck_wound:
			if stuck_wound.removed.is_connected(_on_wound_removed):
				stuck_wound.removed.disconnect(_on_wound_removed)
		stuck_wound = value

func _ready() -> void:
	_projectile = get_parent() as Projectile

func _on_wound_removed():
	stuck_wound = null
	stuck_collider = null
	if _projectile is DynamicProjectile:
		_projectile.velocity = _stuck_vel

func reset(_proj: Projectile):
	stuck_collider = null
	stuck_wound = null

func get_wound() -> ContainerWound:
	# TODO get custom container wound if possible
	return Wounds.pick_basic_container()

func _physics_process(delta: float) -> void:
	if stuck_wound != null:
		_stuck_vel = (_projectile.global_position - _stuck_pos) / delta
		_stuck_pos = _projectile.global_position

func hit_effect(
	proj: Projectile, obj: CollisionObject2D, point: Vector2, normal: Vector2
) -> HitEffect.EffectInfo:
	
	if stuck_collider == null:
		if proj is DynamicProjectile:
			proj.velocity.x = 0
			proj.velocity.y = 0
		stuck_wound = get_wound()
		if obj is ProjectileTarget:
			obj.add_wound(stuck_wound, proj, point, normal)
		else:
			Global.root_2d_world.add_child(stuck_wound)
			stuck_wound.reset_physics_interpolation()
		stuck_wound.global_rotation = lerp_angle(normal.angle() + PI, proj.global_rotation, 0.75)
		stuck_wound.global_position = point
		stuck_wound.add_object_to_container(proj)
		stuck_collider = obj
		stuck_wound.removed.connect(_on_wound_removed)
		_stuck_vel = Vector2.ZERO
		if proj is DynamicProjectile:
			_stuck_vel += proj.velocity * 0.5
			if obj is RigidBody2D:
				_stuck_vel += obj.linear_velocity
		_stuck_pos = point
		return HitEffect.EffectInfo.KEEP_ALIVE_AND_BREAK()
	
	# ignore collision if stuck already object
	return HitEffect.EffectInfo.TRUE()
