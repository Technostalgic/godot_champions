extends HitEffect

@export var max_life_dif: float = 0.2

func reset(proj: Projectile):
	proj.total_max_lifetime += randf() * max_life_dif
	super.reset(proj)
