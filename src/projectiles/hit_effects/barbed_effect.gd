extends StickyEffect

@export var extra_time: float = 1
@export var time_increment: float = 0.35

var total_extra_time: float = 0

func set_level(lvl: int):
	super.set_level(lvl)
	total_extra_time = extra_time + (time_increment * (lvl - 1))

func hit_effect(
	proj: Projectile, obj: CollisionObject2D, point: Vector2, normal: Vector2
) -> HitEffect.EffectInfo:
	
	if obj is ProjectileTarget:
		var was_stuck = stuck_collider != null
		var info := super.hit_effect(proj, obj, point, normal)
		
		# when projectile is initially stuck
		if !was_stuck && stuck_collider != null:
			proj.total_max_lifetime += extra_time
		
		return info
	
	return HitEffect.EffectInfo.FALSE()

func get_stat_strings() -> Array[HitEffect.StatString]:
	return [
		HitEffect.StatString.create("Extra Time", str(total_extra_time))
	]
