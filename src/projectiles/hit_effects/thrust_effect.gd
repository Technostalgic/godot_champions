extends HitEffect

@export var thrust_factor: float = 10
@export var thrust_delay: float = 0.075
@export var factor_increment: float = 5

var total_thrust_factor: float = 0
var total_thrust_delay: float = 0
var target_speed: float = 0

func _physics_process(delta):
	if is_instance_valid(parent_proj):
		if parent_proj.lifetime >= thrust_delay:
			handle_thrust(delta)

func reset(proj: Projectile):
	total_thrust_factor = thrust_factor + (level - 1) * factor_increment
	total_thrust_delay = thrust_delay
	if is_instance_valid(proj.proj_owner):
		proj.global_rotation = proj.proj_owner.global_rotation
	if proj is DynamicProjectile:
		target_speed = proj.velocity.length()
		proj.velocity *= 0.25

func handle_thrust(delta: float):
	var damping = 0.95
	if parent_proj is DynamicProjectile:
		parent_proj.velocity *= damping
		var thrust := Vector2.from_angle(parent_proj.global_rotation)
		thrust *= total_thrust_factor * target_speed
		parent_proj.apply_acceleration(thrust, delta)

func set_level(lvl: int):
	super.set_level(lvl)
	total_thrust_factor = thrust_factor + (lvl - 1) * factor_increment

func get_stat_strings() -> Array[StatString]:
	return [
		HitEffect.StatString.create("Thrust Factor", str(total_thrust_factor))
	]
