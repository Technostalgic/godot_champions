extends HitEffect

@export var bounces: int = 1

var bounces_left: int = 0
var max_bounces: int = 0

func set_level(lvl: int):
	super.set_level(lvl)
	max_bounces = bounces + (lvl - 1)

func reset(_proj: Projectile):
	bounces_left = max_bounces

func hit_effect(
	proj: Projectile, obj: CollisionObject2D, point: Vector2, normal: Vector2
) -> HitEffect.EffectInfo:
	
	if proj is DynamicProjectile:
		bounces_left -= 1
		if bounces_left >= 0:
			proj.velocity = proj.velocity - 2 * proj.velocity.dot(normal) * normal
			proj.global_rotation = proj.velocity.angle()
			if proj is Bullet:
				var pos = point + normal
				proj.global_position = pos
				proj.reset_positioning()
				proj.orient_collision()
		# only bounce if the projectile doesn't hit it's target
		if obj is ProjectileTarget:
			if obj.allegiance != proj.get_allegiance():
				return HitEffect.EffectInfo.FALSE()
		if bounces_left >= 0:
			return HitEffect.EffectInfo.KEEP_ALIVE_AND_BREAK()
	
	return HitEffect.EffectInfo.FALSE()

func get_stat_strings() -> Array[HitEffect.StatString]:
	return [
		HitEffect.StatString.create("Bounces", str(max_bounces))
	]
