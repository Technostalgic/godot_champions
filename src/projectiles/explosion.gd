class_name Explosion
extends Projectile

static func explode_at(explosion: Explosion, point: Vector2):
	explosion.position = point
	explosion.scale.x = explosion.proj_stats.aoe_size
	explosion.scale.y = explosion.proj_stats.aoe_size
	Global.root_2d_world.add_child(explosion)

func _enter_tree():
	total_max_lifetime = max_lifetime
	super._enter_tree()

func _on_body_enter(body: Node2D):
	_body_enter_defer.call_deferred(body)

func _body_enter_defer(body: Node2D):
	if body is CollisionObject2D:
		var point := GravityField.project_on_collider(body, global_position)
		try_hit_collider(
			body,
			point, 
			(point - body.global_position).normalized()
		)

func _on_area_enter(area: Area2D):
	if area is InteractionTriggerCollider:
		area.trigger(proj_owner.wep_owner as Actor)

func handle_lifetime(delta: float):
	super.handle_lifetime(delta)
	if lifetime >= max_lifetime:
		remove_projectile()

func hit_collider(collider: CollisionObject2D, point: Vector2, normal: Vector2):
	hit_collider_damage(proj_stats.aoe_damage, collider, point, normal)

func collide_with(obj: CollisionObject2D, point: Vector2, normal: Vector2) -> bool:
	var skip := false
	if obj is ProjectileTarget:
		skip = super.collide_with(obj, point, normal)
	else:
		var force: Vector2 = (point - global_position).normalized() * proj_stats.knockback * 2
		obj.apply_impulse(force, point - obj.global_position)
	return skip

func detonate_projectile(_point: Vector2):
	pass

func detonate_projectile_immediate(point: Vector2):
	_detonation_point = point
	remove_projectile_immediate()
