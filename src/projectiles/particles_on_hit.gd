extends Node

@export var particle_burst: ParticleBurst = null
@export var emit_on_target_hit: bool = false

func _ready():
	var parent := get_parent()
	if parent is Projectile:
		parent.on_collide.connect(_on_hit)

func _on_hit(obj: CollisionObject2D, point: Vector2, normal: Vector2):
	if !emit_on_target_hit:
		if obj is ProjectileTarget:
			return
	particle_burst.burst(Transform2D(normal.angle(), point))
