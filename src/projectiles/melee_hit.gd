class_name MeleeHit
extends Projectile

@export var collision: Area2D = null
@export var knockback: float = 75
@export var remove_on_hit: bool = true

var direction: Vector2 = Vector2.ZERO
var target: ProjectileTarget = null

func _physics_process(delta):
	super._physics_process(delta)

func _on_hit(node: Node2D):
	if node is ProjectileTarget:
		if target != null:
			if node != target:
				return
		on_hit.emit(node)
		node.hit(self, proj_stats.damage, global_position)
		if remove_on_hit:
			detonate_projectile(global_position)
	GroundDetection.new()
