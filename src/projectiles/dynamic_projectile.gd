class_name DynamicProjectile
extends Projectile

var velocity: Vector2 = Vector2.ZERO
var _last_vel: Vector2 = Vector2.ZERO

func fixed_step(delta: float) -> void:
	_last_vel = velocity
	apply_damping(delta)
	super.fixed_step(delta)

func apply_damping(delta: float):
	if proj_stats.damping <= 0:
		return
	var damp = pow(1.01, -proj_stats.damping * delta * 50)
	velocity *= damp

func collide_with(obj: CollisionObject2D, point: Vector2, normal: Vector2) -> bool:
	if obj is RigidBody2D:
		if !(obj is ProjectileTarget):
			var force: Vector2 = velocity.normalized() * proj_stats.knockback
			obj.apply_impulse(force, point - obj.global_position)
	return super.collide_with(obj, point, normal)

func apply_acceleration(acc: Vector2, delta: float):
	var parent = get_parent()
	if parent is CollisionObject2D:
		if parent is RigidBody2D:
			var force := acc
			if parent is ProjectileTarget:
				force = force / parent.mass * parent.knockback_multiplier
			(parent as RigidBody2D).apply_force(force, global_position - parent.global_position)
	else:
		velocity += acc * delta
