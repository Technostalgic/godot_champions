class_name Projectile
extends Node2D

signal on_collide(obj: CollisionObject2D, point: Vector2, normal: Vector2)
signal on_hit(self_proj: Projectile, target: CollisionObject2D)
signal on_detonate(self_proj: Projectile)
signal released_from(wep: Weapon)
signal stepped(delta: float)

@export var title: String = "Projectile"
@export var texture: Texture = null
@export var max_lifetime: float = 2.5

var total_max_lifetime: float = 0
var is_crit: bool = false
var lifetime: float = 0
var proj_owner: Weapon = null
var proj_stats: Stats.ProjectileStats
var is_alive: bool = true
var hit_effects: Array[HitEffect] = []
var _detonation_queued: bool = false
var _detonation_point: Vector2 = Vector2.ZERO

func _enter_tree() -> void:
	reset_physics_interpolation.call_deferred()

func _physics_process(delta: float) -> void:
	if !is_alive:
		if _detonation_queued: detonate_projectile_immediate(_detonation_point)
		else: remove_projectile_immediate()
		return
	fixed_step(delta)

func fixed_step(delta: float) -> void:
	handle_lifetime(delta)
	stepped.emit(delta)

func try_hit_collider(col: CollisionObject2D, point: Vector2, normal: Vector2):
	var skip := false
	if col is ProjectileTarget:
		var allegiance = get_allegiance()
		if (
			allegiance != ProjectileTarget.Allegiance.NUETRAL &&
			col.allegiance != ProjectileTarget.Allegiance.NUETRAL
		):
			if col.allegiance == get_allegiance():
				skip = true
	if !skip:
		skip = collide_with(col, point, normal)
		if !skip:
			hit_collider(col, point, normal)

func hit_collider(collider: CollisionObject2D, point: Vector2, normal: Vector2):
	hit_collider_damage(proj_stats.damage, collider, point, normal)

func hit_collider_damage(dmg: float, collider: CollisionObject2D, point: Vector2, _norm: Vector2):
	if collider is ProjectileTarget:
		var damage = dmg
		var crit_mult: float = 1
		if is_instance_valid(proj_owner):
			if is_instance_valid(proj_owner.get_weapon_stats()):
				crit_mult = proj_owner.get_weapon_stats().crit_mult
		if is_crit:
			damage *= crit_mult
		collider.hit(self, damage, point)
	on_hit.emit(self, collider)

func handle_lifetime(delta: float):
	lifetime += delta
	if lifetime >= total_max_lifetime:
		detonate_projectile(global_position)

func release_from(weapon: Weapon):
	var shot_stats = weapon.get_shot_stats()
	total_max_lifetime = max_lifetime
	is_crit = shot_stats.crit_chance > randf()
	is_alive = true
	lifetime = 0
	proj_owner = weapon
	proj_stats = shot_stats.projectile_stats
	_detonation_queued = false
	for fx in hit_effects:
		fx.reset(self)
	released_from.emit(weapon)

func get_allegiance() -> ProjectileTarget.Allegiance:
	if !is_instance_valid(proj_owner):
		return ProjectileTarget.Allegiance.NUETRAL
	if !is_instance_valid(proj_owner.wep_owner):
		return ProjectileTarget.Allegiance.NUETRAL
	return proj_owner.wep_owner.allegiance

# return true if collision should be ignored
func collide_with(obj: CollisionObject2D, point: Vector2, normal: Vector2) -> bool:
	var stay_alive := false
	var ignore_collision := false
	for effect in hit_effects:
		var info := effect.hit_effect(self, obj, point, normal)
		if info.keep_alive:
			stay_alive = true
		if info.ignore_collision:
			ignore_collision = true
		if info.break_effects:
			break
	if !ignore_collision:
		on_collide.emit(obj, point, normal)
		if !stay_alive:
			detonate_projectile(point)
	return ignore_collision

func burst_projectile(point: Vector2):
	if proj_stats.aoe_size < 1 || proj_stats.aoe_damage < 1:
		return
	var explosion = Global.default_explosion_prefab.instantiate() as Explosion
	explosion.proj_stats = proj_stats
	explosion.proj_owner = proj_owner
	Explosion.explode_at(explosion, point)

func detonate_projectile(point: Vector2):
	_detonation_queued = true
	_detonation_point = point
	global_position = point
	remove_projectile()

func detonate_projectile_immediate(point: Vector2):
	on_detonate.emit(self)
	burst_projectile(point)
	remove_projectile_immediate()

func remove_projectile():
	is_alive = false

func remove_projectile_immediate():
	if is_instance_valid(proj_owner):
		get_parent().remove_child(self)
		proj_owner.on_projectile_removed(self)
	else:
		queue_free()
