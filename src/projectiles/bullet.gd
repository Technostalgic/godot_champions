class_name Bullet
extends DynamicProjectile

@export var collision: RayCast2D = null
@export var blood_hit_effects: Array[ParticleBurst] = []

var _last_calc_position: Vector2 = Vector2.ZERO
var _next_calc_position: Vector2 = Vector2.ZERO

func _ready() -> void:
	collision.enabled = false

func reset_positioning():
	_last_calc_position = global_position
	_next_calc_position = _last_calc_position

func fixed_step(delta: float) -> void:
	_last_calc_position = global_position
	_next_calc_position = _last_calc_position + velocity * delta
	orient_collision()
	global_position = _next_calc_position
	var col_ct := 0
	if collision.is_colliding():
		col_ct = 1
	for i in range(0, col_ct):
		var col = collision.get_collider()
		if col is CollisionObject2D:
			var norm := collision.get_collision_normal()
			if norm.is_zero_approx():
				norm = -velocity.normalized()
			try_hit_collider(
				collision.get_collider(), 
				collision.get_collision_point(), 
				norm)
		else:
			detonate_projectile(collision.get_collision_point())
	super.fixed_step(delta)

func orient_collision():
	collision.global_position = _last_calc_position
	collision.target_position = _next_calc_position * global_transform
	collision.force_raycast_update()

func release_from(weapon: Weapon):
	var stats: Stats.ShotStats = weapon.get_shot_stats()
	global_position = weapon.get_attack_target().global_position
	reset_positioning()
	orient_collision()
	var half_spread := stats.spread * 0.5
	var target_ang := weapon.global_rotation + randf_range(-half_spread, half_spread)
	var speed := stats.projectile_speed
	speed -= randf() * speed * stats.speed_variance
	velocity = Vector2.from_angle(target_ang) * speed
	_last_vel = velocity
	global_rotation = target_ang
	super.release_from(weapon)
	
	# _force_collider_update = true
	# var step_delta = (1 - Engine.get_physics_interpolation_fraction())
	# fixed_step(step_delta * get_physics_process_delta_time())

func collide_with(obj: CollisionObject2D, point: Vector2, normal: Vector2) -> bool:
	var ignore = super.collide_with(obj, point, normal)
	
	if !ignore:
		if obj is Actor:
			if obj.wound_type == Wound.Type.FLESH:
				for effect in blood_hit_effects:
					effect.burst(Transform2D(normal.angle(), point), obj.linear_velocity)
	return ignore
