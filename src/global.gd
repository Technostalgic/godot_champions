class_name GlobalClass
extends Node

class SessionParameters:
	var players: Array[PlayerConfig] = [PlayerConfig.new()]
	
	# Switch to a controller id that is not in use
	func switch_player_controller(player: PlayerConfig):
		var desired_id: int = player.controller_id
		player.controller_id = -2
		
		var occupied: bool = true
		while occupied:
			occupied = false
			desired_id += 1
			if desired_id >= players.size():
				desired_id = -1
			for plr in players:
				if plr.controller_id == desired_id:
					occupied = true
					break
		
		player.controller_id = desired_id
		Global.players_changed.emit()

class PlayerConfig:
	var player_id: int = 0
	var controller_id: int = -1
	var color: int = 0

const PLAYER_COLORS: Array[Color] = [
	Color.WHITE,
	Color.DODGER_BLUE,
	Color.LIME_GREEN,
	Color.GOLD,
	Color.BLUE_VIOLET,
	Color.AQUA,
	Color.SADDLE_BROWN,
	Color.BLACK,
]

@warning_ignore("int_as_enum_without_cast")
@warning_ignore("int_as_enum_without_match")
const COLLISION_MASK_GIB: CollisionLayer = (
	CollisionLayer.TERRAIN |
	CollisionLayer.PROPS |
	CollisionLayer.HERO_AOE |
	CollisionLayer.MOB_AOE 
)

enum CollisionLayer {
	TERRAIN = 1,
	HEROS = 2,
	MOBS = 4,
	PROPS = 8,
	GIBS = 16,
	HERO_AOE = 32,
	MOB_AOE = 64,
	ETHER_ITEMS = 128,
}

signal players_changed()

@export var all_arenas: Array[PackedScene] = []
@export var shop_arena: PackedScene = null
@export var hero_prefab: PackedScene = null
@export var camera_prefab: PackedScene = null
@export var game_ui_prefab: PackedScene = null
@export var game_setup_ui_container: Control = null
@export var default_explosion_prefab: PackedScene = null
@export var chest_prefab: PackedScene = null
@export var coin_prefab: PackedScene = null
@export var starting_level: PackedScene = null
@export var screenspace_effects: Array[PackedScene] = []

var root_2d_world: Node2D = null
var root_removables: Node2D = null
var session_params: SessionParameters = SessionParameters.new()
var session_started: bool = false
var heros: Array[Hero] = []

var damage_numbers: DamageNumbers = null
var gameplay_ui_container: GameplayUI = null
var camera: Camera2D = null
var current_level: Level = null
var spawn_points: Array[Node2D] = []
var ss_effects: Array[CanvasLayer] = []

var suspend_gameplay_input = false

func _ready():
	_search_tree_for_level()
	create_ss_shaders.call_deferred()

func _process(_delta: float):
	if session_started:
		if is_game_paused():
			if Input.is_action_just_pressed("start_global"):
				if Input.is_action_pressed("reset_global"):
					reset_game()
				else:
					Global.pause_game(false)
	else:
		if Input.is_action_just_pressed("add_player"):
			Global.create_new_player()
		if Input.is_action_just_pressed("remove_player"):
			Global.remove_last_player()
		if Input.is_action_just_pressed("start_global"):
			start_game_session()

func _search_tree_for_level() -> void:
	for child in get_tree().root.get_children():
		if child is Level:
			if !session_started:
				_start_game_in_level.call_deferred(child)

func _start_game_in_level(level: Level):
	start_game_session()
	level.get_parent().remove_child(level)
	set_level(level)
	respawn_heros()

func start_game_session():
	root_2d_world = Node2D.new()
	root_removables = Node2D.new()
	root_2d_world.add_child(root_removables)
	get_parent().add_child(root_2d_world)
	
	camera = camera_prefab.instantiate() as Camera2D
	root_2d_world.add_child(camera)
	
	spawn_heros()
	create_game_ui.call_deferred()
	damage_numbers = DamageNumbers.new()
	damage_numbers.initialize()
	
	session_started = true
	game_setup_ui_container.hide()
	
	set_level(starting_level.instantiate())
	respawn_heros()
	
	order_scene_tree()

func spawn_heros():
	for hero in heros:
		hero.queue_free()
	for profile in session_params.players:
		var hero = hero_prefab.instantiate() as Hero
		hero.configure(profile)
		heros.push_back(hero)
		root_2d_world.add_child(hero)
		hero.health.health = hero.health.max_health

func respawn_fallen_heros():
	for hero in heros:
		if !hero.is_inside_tree():
			root_2d_world.add_child(hero)
			hero.health.health = hero.health.max_health * 0.1
			hero.global_position = spawn_points.pick_random().global_position
			hero.reset_physics_interpolation()

func respawn_heros():
	for hero in heros:
		if !hero.is_inside_tree():
			root_2d_world.add_child(hero)
			hero.health.health = hero.health.max_health * 0.3
		hero.global_position = spawn_points.pick_random().global_position
		hero.reset_physics_interpolation()

func create_game_ui():
	if is_instance_valid(gameplay_ui_container):
		gameplay_ui_container.queue_free()
		
	gameplay_ui_container = game_ui_prefab.instantiate() as GameplayUI
	add_child(gameplay_ui_container)
	
	gameplay_ui_container.create_player_infos()
	gameplay_ui_container.create_reticles()

func create_ss_shaders():
	for layer in ss_effects:
		layer.queue_free()
	ss_effects.clear()
	for shader_fx in screenspace_effects:
		var layer = shader_fx.instantiate() as CanvasLayer
		get_tree().root.add_child(layer)
		ss_effects.push_back(layer)
	order_scene_tree()

func order_scene_tree():
	if root_2d_world != null:
		get_parent().move_child(root_2d_world, -1)
	for layer in ss_effects:
		layer.get_parent().move_child(layer, -1)
	get_parent().move_child(self, -1)

func set_level(level: Level):
	if is_instance_valid(current_level):
		current_level.queue_free()
		clear_removables()
		Coin.clear_active_coins()
	current_level = level
	root_2d_world.add_child(current_level)
	root_2d_world.move_child(current_level, 0)
	gather_spawn_points()

func is_in_level() -> bool:
	return is_instance_valid(current_level)

func gather_spawn_points():
	spawn_points.clear()
	for node in current_level.spawn_point_container.get_children():
		if node is Node2D:
			spawn_points.push_back(node)

func clear_removables():
	for child in root_removables.get_children():
		child.queue_free()

func set_input_ui_mode_all_players(ui_mode: bool = true):
	for hero in heros:
		hero.input_behavior.ui_mode = ui_mode

func create_new_player():
	if session_started:
		push_error("cannot add player to ongoing session")
		return
	
	var new_player = PlayerConfig.new()
	for plr in session_params.players:
		new_player.player_id = max(new_player.player_id, plr.player_id + 1)
		new_player.color = new_player.player_id
		new_player.controller_id = max(new_player.controller_id, plr.controller_id + 1)
	
	session_params.players.push_back(new_player)
	players_changed.emit()

func remove_last_player():
	if session_started:
		push_error("cannot remove player from ongoing session")
		return
		
	if session_params.players.size() > 1:
		session_params.players.pop_back()
	
	players_changed.emit()

func pause_game(pause: bool = true):
	if !is_instance_valid(Global.gameplay_ui_container):
		return
	gameplay_ui_container.open_pause_overlay(pause)
	if pause:
		root_2d_world.process_mode = Node.PROCESS_MODE_DISABLED
	else:
		Global.root_2d_world.process_mode = Node.PROCESS_MODE_INHERIT

func is_game_paused() -> bool:
	if !session_started:
		return false
	return root_2d_world.process_mode == Node.PROCESS_MODE_DISABLED

func reset_game():
	if !session_started:
		return
	
	Coin.clear_active_coins()
	ParticleSystem.remove_all()
	
	spawn_points.clear()
	WaveState.reset()
	
	gameplay_ui_container.queue_free()
	gameplay_ui_container = null
	
	root_2d_world.queue_free()
	heros = []
	root_2d_world = null
	camera = null
	
	WaveState.paused = true
	WaveState.reset()
	WaveState.wave_number = 1
	MobManager.clear_mobs()
	
	session_started = false
	game_setup_ui_container.show()

func damage_number_at(player_index: int, damage: float, position: Vector2):
	if !session_started:
		return
	damage_numbers.add_damage_at(player_index, damage, position)
