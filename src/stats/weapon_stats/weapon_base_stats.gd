class_name WeaponBaseStats
extends Node

@export var projectile: PackedScene = null
@export var fire_mode: Stats.FireMode = Stats.FireMode.AUTOMATIC
@export var power: float = 1
@export var damage: float = 5
@export var aoe_damage: float = 0
@export var aoe_size: float = 0
@export var fire_rate: float = 10
@export var cooldown: float = 0 
@export var crit_chance: float = 0.01 #TODO
@export var crit_mult: float = 2 #TODO
@export var spread: float = 0.1
@export var projectile_speed: float = 500
@export var speed_variance: float = 0.05
@export var fire_delay: float = 0
@export var recoil: float = 1
@export var projectile_damping: float = 0
@export var projectile_count: int = 1
@export var knockback: float = 20
@export var weapon_slots: float = 6 #TODO
@export var ammo_cost: float = 1
@export var magazine_size: float = 10
@export var reload_time: float = 2
@export var max_ammo: float = 100
@export var energy_cost_shot: float = 0
@export var energy_regeneration: float = 0.1
@export var max_energy: float = 10

var stats_owner: Weapon = null

func _enter_tree():
	if stats_owner == null:
		var parent = get_parent()
		if is_instance_valid(parent) && parent is Weapon:
			stats_owner = parent
			parent.attach_stats(self)

func apply_proj_stats_to(proj_stats: Stats.ProjectileStats) -> Stats.ProjectileStats:
	proj_stats.damage = damage
	proj_stats.knockback = knockback
	proj_stats.aoe_size = aoe_size
	proj_stats.aoe_damage = aoe_damage
	proj_stats.damping = projectile_damping
	return proj_stats
	
func apply_wep_stats_to(wep_stats: Stats.WeaponStats) -> Stats.WeaponStats:
	wep_stats.fire_rate = fire_rate
	wep_stats.crit_mult = crit_mult
	wep_stats.magazine_size = magazine_size
	wep_stats.reload_time = reload_time
	wep_stats.max_ammo = max_ammo
	wep_stats.mod_slots = weapon_slots
	wep_stats.energy_regeneration = energy_regeneration
	wep_stats.max_energy = max_energy
	return wep_stats

func apply_shot_stats_to(shot_stats: Stats.ShotStats) -> Stats.ShotStats:
	shot_stats.projectile = projectile
	shot_stats.projectile_stats = apply_proj_stats_to(Stats.ProjectileStats.new())
	shot_stats.fire_mode = fire_mode;
	shot_stats.power = power
	shot_stats.crit_chance = crit_chance
	shot_stats.cooldown = cooldown
	shot_stats.spread = spread
	shot_stats.projectile_speed = projectile_speed
	shot_stats.speed_variance = speed_variance
	shot_stats.projectile_count = projectile_count
	shot_stats.ammo_cost = ammo_cost
	shot_stats.energy_cost = energy_cost_shot
	shot_stats.fire_delay = fire_delay;
	shot_stats.recoil = recoil;
	return shot_stats

func get_weapon_stats() -> Stats.WeaponStats:
	return apply_wep_stats_to(Stats.WeaponStats.new())

func get_shot_stats() -> Stats.ShotStats:
	return apply_shot_stats_to(Stats.ShotStats.new())
