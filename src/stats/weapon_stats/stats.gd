class_name Stats
extends RefCounted

enum Stat {
	# Projectile
	DAMAGE,
	KNOCKBACK,
	AOE_SIZE,
	DAMPING,
	AOE_DAMAGE,
	# Shot
	PROJECTILE_TYPE,
	FIRE_MODE,
	POWER,
	CRIT_CHANCE,
	COOLDOWN,
	SPREAD,
	SPEED_VAR,
	PROJ_SPEED,
	PROJ_COUNT,
	AMMO_COST,
	ENERGY_COST,
	FIRE_DELAY,
	RECOIL,
	# Weapon
	FIRE_RATE,
	CRIT_MULT,
	MAG_SIZE,
	RELOAD_TIME,
	WEAPON_SLOTS,
	ENERGY_REGEN,
	MAX_AMMO,
	MAX_ENERGY,
}

enum StatType {
	PROJECTILE,
	SHOT,
	WEAPON,
}

enum FireMode {
	AUTOMATIC,
	SEMI_AUTO,
}

static func fire_mode_to_str(fire_mode: FireMode) -> String:
	match fire_mode:
		FireMode.AUTOMATIC:
			return "Full Auto"
		FireMode.SEMI_AUTO:
			return "Semi Auto"
	return "Unknown"

static func stat_to_str(stat: Stat) -> String:
	match stat:
		Stat.DAMAGE: 
			return "Damage"
		Stat.KNOCKBACK:
			return "Knockback"
		Stat.AOE_SIZE:
			return "AOE Size"
		Stat.DAMPING:
			return "Damping"
		Stat.AOE_DAMAGE:
			return "AOE Damage"
		Stat.PROJECTILE_TYPE:
			return "Projectile"
		Stat.FIRE_MODE:
			return "Fire Mode"
		Stat.POWER:
			return "Power"
		Stat.CRIT_CHANCE:
			return "Crit Chance"
		Stat.COOLDOWN:
			return "Cooldown"
		Stat.SPREAD:
			return "Spread"
		Stat.SPEED_VAR:
			return "Speed Var"
		Stat.PROJ_SPEED:
			return "Speed"
		Stat.PROJ_COUNT:
			return "Count"
		Stat.AMMO_COST:
			return "Ammo Cost"
		Stat.ENERGY_COST:
			return "Energy Cost"
		Stat.FIRE_DELAY:
			return "Fire Delay"
		Stat.RECOIL:
			return "Recoil"
		Stat.FIRE_RATE:
			return "Fire Rate"
		Stat.CRIT_MULT:
			return "Crit Mult"
		Stat.MAG_SIZE:
			return "Mag Size"
		Stat.RELOAD_TIME:
			return "Reload Time"
		Stat.ENERGY_REGEN:
			return "Energy Regen"
		Stat.MAX_AMMO:
			return "Max Ammo"
		Stat.MAX_ENERGY:
			return "Max Energy"
	return "Unknown"

class StatContainer:
	extends RefCounted
	
	func get_stat(_stat: Stat) -> float:
		return NAN

	func set_stat(_stat: Stat, _value: Variant):
		pass

	func modify_stat(modifier: StatModifier, multiplier: float):
		var stat_float_val := get_stat(modifier.stat) as float
		if modifier.modifier_variant == null && is_nan(stat_float_val):
			return
		var mod_val = modifier.get_modifier_value(multiplier)
		var stat_val = get_stat(modifier.stat)
		match modifier.mode:
			StatModifier.Mode.ADD:
				stat_val += mod_val
			StatModifier.Mode.MULTIPLY:
				stat_val *= mod_val
			StatModifier.Mode.SET:
				stat_val = mod_val
		set_stat(modifier.stat, stat_val)

	func modify_stat_add(modifier: StatModifier, multiplier: float):
		if modifier.mode != StatModifier.Mode.ADD:
			return
		var stat_float_val := get_stat(modifier.stat) as float
		if modifier.modifier_variant == null && is_nan(stat_float_val):
			return
		var mod_val = modifier.get_modifier_value(multiplier)
		set_stat(modifier.stat, get_stat(modifier.stat) + mod_val)

	func modify_stat_mult(modifier: StatModifier, multiplier: float):
		if modifier.mode != StatModifier.Mode.MULTIPLY:
			return
		var stat_float_val := get_stat(modifier.stat) as float
		if modifier.modifier_variant == null && is_nan(stat_float_val):
			return
		var mod_val = modifier.get_modifier_value(multiplier)
		set_stat(modifier.stat, get_stat(modifier.stat) * mod_val)

class ProjectileStats:
	extends StatContainer
	
	signal on_hit(obj: CollisionObject2D)
	
	var damage: float = 1
	var knockback: float = 20
	var aoe_size: float = 0
	var damping: float = 0
	var aoe_damage: float = 1
	
	static func filled_with(val: float) -> ProjectileStats:
		var stats := new()
		stats.damage = val
		stats.knockback = val
		stats.aoe_size = val
		stats.damping = val
		stats.aoe_damage = val
		return stats
	
	func get_stat(stat: Stat) -> float:
		match stat:
			Stat.DAMAGE: return damage
			Stat.KNOCKBACK: return knockback
			Stat.AOE_SIZE: return aoe_size
			Stat.AOE_DAMAGE: return aoe_damage
			Stat.DAMPING: return damping
		return NAN
	
	func set_stat(stat: Stat, value: Variant):
		match stat:
			Stat.DAMAGE: damage = value
			Stat.KNOCKBACK: knockback = value
			Stat.AOE_SIZE: aoe_size = value
			Stat.AOE_DAMAGE: aoe_damage = value
			Stat.DAMPING: damping = value

	func combine_add(other: ProjectileStats):
		damage += other.damage
		knockback += other.knockback
		aoe_size += other.aoe_size
		damping += other.damping
		aoe_damage += other.aoe_damage
	
	func combine_mult(other: ProjectileStats):
		damage *= other.damage
		knockback *= other.knockback
		aoe_size *= other.aoe_size
		damping *= other.damping
		aoe_damage *= other.aoe_damage

class ShotStats:
	extends StatContainer
	
	signal on_create_projectile(proj: Projectile)
	signal on_fire_projectile(proj: Projectile)
	
	var projectile: PackedScene = null
	var projectile_stats: ProjectileStats = null
	var fire_mode: FireMode = FireMode.AUTOMATIC
	var power: float = 1
	var crit_chance: float = 0.01
	var cooldown: float = 0.1
	var spread: float = 0.1
	var projectile_speed: float = 500
	var speed_variance: float = 0.05
	var projectile_count: float = 1
	var ammo_cost: float = 1
	var energy_cost: float = 0
	var fire_delay: float = 0
	var recoil: float = 0
	
	static func filled_with(val: float) -> ShotStats:
		var stats := new()
		stats.projectile = null
		stats.projectile_stats = ProjectileStats.filled_with(val)
		stats.power = val
		stats.crit_chance = val
		stats.cooldown = val
		stats.spread = val
		stats.projectile_speed = val
		stats.speed_variance = val
		stats.projectile_count = val
		stats.ammo_cost = val
		stats.energy_cost = val
		stats.fire_delay = val
		stats.recoil = val
		return stats
	
	func combine_add(other: ShotStats):
		projectile_stats.combine_add(other.projectile_stats)
		power += other.power
		crit_chance += other.crit_chance
		cooldown += other.cooldown
		spread += other.spread
		projectile_speed += other.projectile_speed
		speed_variance += other.speed_variance
		projectile_count += other.projectile_count
		ammo_cost += other.ammo_cost
		energy_cost += other.energy_cost
		fire_delay += other.fire_delay
		recoil += other.recoil
	
	func combine_mult(other: ShotStats):
		projectile_stats.combine_mult(other.projectile_stats)
		power *= other.power
		crit_chance *= other.crit_chance
		cooldown *= other.cooldown
		spread *= other.spread
		projectile_speed *= other.projectile_speed
		speed_variance *= other.speed_variance
		projectile_count *= other.projectile_count
		ammo_cost *= other.ammo_cost
		energy_cost *= other.energy_cost
		fire_delay *= other.fire_delay
		recoil *= other.recoil

	func get_stat(stat: Stat) -> float:
		match stat:
			Stat.POWER: return power
			Stat.CRIT_CHANCE: return crit_chance
			Stat.COOLDOWN: return cooldown
			Stat.SPREAD: return spread
			Stat.SPEED_VAR: return speed_variance
			Stat.PROJ_SPEED: return projectile_speed
			Stat.PROJ_COUNT: return projectile_count
			Stat.AMMO_COST: return ammo_cost
			Stat.ENERGY_COST: return energy_cost
			Stat.FIRE_DELAY: return fire_delay
			Stat.RECOIL: return recoil
		return NAN

	func set_stat(stat: Stat, value: Variant):
		match stat:
			Stat.PROJECTILE_TYPE: projectile = value
			Stat.FIRE_MODE: fire_mode = value
			Stat.POWER: power = value
			Stat.CRIT_CHANCE: crit_chance = value
			Stat.COOLDOWN: cooldown = value
			Stat.SPREAD: spread = value
			Stat.SPEED_VAR: speed_variance = value
			Stat.PROJ_SPEED: projectile_speed = value
			Stat.PROJ_COUNT: projectile_count = value
			Stat.AMMO_COST: ammo_cost = value
			Stat.ENERGY_COST: energy_cost = value
			Stat.FIRE_DELAY: fire_delay = value
			Stat.RECOIL: recoil = value

class WeaponStats:
	extends StatContainer
	
	signal on_create_projectile(proj: Projectile)
	signal on_fire_projectile(proj: Projectile)
	
	var fire_rate: float = 10
	var crit_mult: float = 2
	var magazine_size: float = 10
	var reload_time: float = 2
	var energy_regeneration: float = 0.1
	var mod_slots: float = 6
	var max_ammo: float = 100
	var max_energy: float = 10
	
	static func filled_with(val: float) -> WeaponStats:
		var stats := new()
		stats.fire_rate = val
		stats.crit_mult = val
		stats.magazine_size = val
		stats.reload_time = val
		stats.energy_regeneration = val
		stats.max_ammo = val
		stats.max_energy = val
		return stats
	
	func combine_add(other: WeaponStats):
		fire_rate += other.fire_rate
		crit_mult += other.crit_mult
		magazine_size += other.magazine_size
		reload_time += other.reload_time
		energy_regeneration += other.energy_regeneration
		max_ammo += other.max_ammo
		max_energy += other.max_energy
		
	func combine_mult(other: WeaponStats):
		fire_rate *= other.fire_rate
		crit_mult *= other.crit_mult
		magazine_size *= other.magazine_size
		reload_time *= other.reload_time
		energy_regeneration *= other.energy_regeneration
		max_ammo *= other.max_ammo
		max_energy *= other.max_energy
	
	func get_stat(stat: Stat) -> float:
		match stat:
			Stat.MAG_SIZE: return magazine_size
			Stat.RELOAD_TIME: return reload_time
			Stat.ENERGY_REGEN: return energy_regeneration
			Stat.WEAPON_SLOTS: return mod_slots
			Stat.MAX_AMMO: return max_ammo
			Stat.MAX_ENERGY: return max_energy
		return NAN
		
	func set_stat(stat: Stat, value: Variant):
		match stat:
			Stat.MAG_SIZE: magazine_size = value
			Stat.RELOAD_TIME: reload_time = value
			Stat.ENERGY_REGEN: energy_regeneration = value
			Stat.WEAPON_SLOTS: mod_slots = value
			Stat.MAX_AMMO: max_ammo = value
			Stat.MAX_ENERGY: max_energy = value
