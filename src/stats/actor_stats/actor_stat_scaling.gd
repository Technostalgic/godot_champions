class_name ActorStatScaling
extends Resource

## All stats are scaled by a factor of 1 per level by default, unless specified differently by the 
## pairs in this property
@export var scaling_value_multipliers: Array[ActorStatPair] = []

var _scaling_values: Array[float] = []

func _calc_scaling_values():
	_scaling_values.clear()
	var i := ActorStats.Stat.STAT_MAX_VALUE - 1
	while i >= 0:
		_scaling_values.push_back(1)
		i -= 1
	i = scaling_value_multipliers.size() - 1
	while i >= 0:
		var pair := scaling_value_multipliers[i]
		_scaling_values[pair.stat] = pair.value
		i -= 1

func apply_scaling(stat: ActorStats.Stat, base_value: float, scale: float) -> float:
	if _scaling_values.size() <= 0:
		_calc_scaling_values()
	return base_value + base_value * _scaling_values[stat] * scale
