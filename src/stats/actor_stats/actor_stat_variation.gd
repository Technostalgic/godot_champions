extends Node

@export var stats: ActorStats = null
@export var max_variance: float = 0.1
@export var anomaly_chance: float = 0.2
@export var max_anomaly_variance: float = 0.5

func _ready() -> void:
	stats.modifiers_cleared.connect(apply_modifiers)
	apply_modifiers()

func apply_modifiers():
	var i := ActorStats.Stat.STAT_MAX_VALUE - 1
	while i >= 0:
		var o_val := stats.get_stat(i)
		var t_max_variance = max_variance
		if randf() < anomaly_chance:
			t_max_variance = max_anomaly_variance
		var variance := o_val * randf_range(-t_max_variance, t_max_variance)
		stats.add_modifier(ActorStatMod.create(i as ActorStats.Stat, variance))
		i -= 1
