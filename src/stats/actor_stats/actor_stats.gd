class_name ActorStats
extends Node

enum Stat {
	HEALTH,
	KNOCKBACK_RESIST,
	POWER,
	SPEED,
	JUMP,
	DEXTERITY,
	LUCK,
	AI_SMARTNESS,
	STAT_MAX_VALUE,
}

static func get_default_stat_value(stat: Stat) -> float:
	match stat:
		Stat.HEALTH: return 100
		Stat.SPEED: return 50
		Stat.JUMP: return 0.5
	return 1

signal modifiers_cleared()

@export var base_stats: ActorBaseStats = null
@export var level_scaling: ActorStatScaling = null
@export var behavior_containers: Array[Node] = []
@export var level: float = 0 :
	set(value):
		if level != value:
			set_dirty()
		level = value

var _behaviors: Array[Behavior] = []
var _stat_mods: Array[ActorStatMod] = []
var _stat_values: Array[float] = []
var _stats_dirty: Array[bool] = []
var _nodes_dirty: bool = true

var actor: Actor :
	get:
		if actor == null:
			actor = get_parent() as Actor
		return actor

var health: float : 
	get: return get_stat(Stat.HEALTH)
var knockback_resist: float : 
	get: return get_stat(Stat.KNOCKBACK_RESIST)
var power: float : 
	get: return get_stat(Stat.POWER)
var speed: float : 
	get: return get_stat(Stat.SPEED)
var jump: float : 
	get: return get_stat(Stat.JUMP)
var dexterity: float : 
	get: return get_stat(Stat.DEXTERITY)
var luck: float : 
	get: return get_stat(Stat.LUCK)

func _enter_tree() -> void:
	if _behaviors.size() <= 0:
		_collect_behaviors()
		_stats_dirty.clear()
		_stat_values.clear()
		var i = ActorStats.Stat.STAT_MAX_VALUE - 1
		while i >= 0:
			_stats_dirty.push_back(true)
			_stat_values.push_back(base_stats.get_stat_value(i))
			i -= 1
	clear_modifiers()
	_apply_stats_to_nodes.call_deferred()

func _physics_process(_delta: float) -> void:
	if _nodes_dirty:
		_apply_stats_to_nodes()

func _collect_behaviors() -> void:
	_behaviors.clear()
	for node in behavior_containers:
		for child in node.get_children():
			if child is Behavior:
				_behaviors.push_back(child)

func _apply_stat_modifiers(stat: ActorStats.Stat, base_value: float) -> float:
	var additional: float = 0
	for mod in _stat_mods:
		if mod.stat_type != stat:
			continue
		match mod.mod_type:
			ActorStatMod.Type.SET: return mod.mod_value
			ActorStatMod.Type.ADD: additional += mod.mod_value
			ActorStatMod.Type.MULTIPLY: base_value *= mod.mod_value
	return base_value + additional

func _apply_stats_to_nodes():
	actor.knockback_multiplier = 1 / get_stat(Stat.KNOCKBACK_RESIST)
	for behavior in _behaviors:
		behavior.reset_stats()
	_nodes_dirty = false

func set_dirty():
	var i := _stats_dirty.size() - 1
	while i >= 0:
		_stats_dirty[i] = true
		i -= 1
	_nodes_dirty = true

func add_modifier(mod: ActorStatMod) -> void:
	_stat_mods.push_back(mod)
	_stats_dirty[mod.stat_type] = true
	_nodes_dirty = true

## Returns false if modifier was not found
func remove_modifier(mod: ActorStatMod) -> bool:
	var index = _stat_mods.find(mod)
	if index < 0:
		return false
	_stat_mods.remove_at(index)
	_stats_dirty[mod.stat_type] = true
	_nodes_dirty = true
	return true

func clear_modifiers() -> void:
	_stat_mods.clear()
	var i := _stat_values.size() - 1
	while i >= 0:
		_stat_values[i] = base_stats.get_stat_value(i as Stat)
		_stats_dirty[i] = false
		i -= 1
	_nodes_dirty = true
	modifiers_cleared.emit()

func get_base_stat(stat: ActorStats.Stat) -> float:
	var val := base_stats.get_stat_value(stat)
	if level_scaling != null:
		val = level_scaling.apply_scaling(stat, val, level)
	return val

func get_stat(stat: ActorStats.Stat) -> float:
	if _stats_dirty[stat]:
		_stat_values[stat] = _apply_stat_modifiers(stat, get_base_stat(stat))
		_stats_dirty[stat] = false
	return _stat_values[stat]
