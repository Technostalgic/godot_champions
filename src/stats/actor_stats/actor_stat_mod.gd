class_name ActorStatMod
extends RefCounted

enum Type {
	ADD,
	MULTIPLY,
	SET,
}

static func create(stat: ActorStats.Stat, value: float, mod_operation: Type = Type.ADD):
	var mod = ActorStatMod.new()
	mod.stat_type = stat
	mod.mod_value = value
	mod.mod_type = mod_operation
	return mod

@export var stat_type: ActorStats.Stat = ActorStats.Stat.STAT_MAX_VALUE
@export var mod_type: ActorStatMod.Type = ActorStatMod.Type.ADD
@export var mod_value: float = 1

func apply_mod_to(value: float) -> float:
	match stat_type:
		Type.ADD: return value + mod_value
		Type.MULTIPLY: return value * mod_value
		Type.SET: return mod_value
	return value
