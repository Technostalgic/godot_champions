class_name ActorBaseStats
extends Resource

@export var health: float = 100
@export var knockback_resist: float = 1
@export var power: float = 1
@export var speed: float = 50
@export var jump: float = 0.5
@export var dexterity: float = 1
@export var luck: float = 1
@export var ai_smartness: float = 1

var _stat_values: Array[float] = []

func _calc_stat_values() -> void:
	_stat_values.clear()
	var stat: int = 0
	while stat < ActorStats.Stat.STAT_MAX_VALUE:
		var val = 1
		match stat:
			ActorStats.Stat.HEALTH: val = health
			ActorStats.Stat.KNOCKBACK_RESIST: val = knockback_resist
			ActorStats.Stat.POWER: val = power
			ActorStats.Stat.SPEED: val = speed
			ActorStats.Stat.JUMP: val = jump
			ActorStats.Stat.DEXTERITY: val = dexterity
			ActorStats.Stat.LUCK: val = luck
			ActorStats.Stat.AI_SMARTNESS: val = ai_smartness
		_stat_values.push_back(val)
		stat += 1

func get_stat_value(stat: ActorStats.Stat) -> float:
	if _stat_values.size() <= 0:
		_calc_stat_values()
	return _stat_values[stat]
