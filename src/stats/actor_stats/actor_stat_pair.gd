class_name ActorStatPair
extends Resource

static func get_default_pairs() -> Array[ActorStatPair]:
	var arr := [] as Array[ActorStatPair]
	var t_stat: ActorStats.Stat = 0 as ActorStats.Stat
	while t_stat < ActorStats.Stat.STAT_MAX_VALUE:
		arr.push_back(ActorStatPair.create(t_stat, ActorStats.get_default_stat_value(t_stat)))
		t_stat = (t_stat + 1) as ActorStats.Stat
	return arr

static func create(stat_type: ActorStats.Stat, stat_value: float = 1) -> ActorStatPair:
	var pair = ActorStatPair.new()
	pair.stat = stat_type
	pair.value = stat_value
	return pair

@export var stat: ActorStats.Stat = ActorStats.Stat.HEALTH
@export var value: float = 1
