class_name DynamicGravityBody
extends RigidBody2D

signal gravity_changed(old_grav: Vector2, new_grav: Vector2)
signal integrate_forces(state: PhysicsDirectBodyState2D)

# Optional - the ground detection behavior for this actor if it has one
@export var grounded: GroundDetection = null

var _current_attractor: GravityField = null
var _current_attractor_dist_sq: float = INF
var _current_gravity: Vector2 = Vector2.ZERO
var _last_gravity: Vector2 = Vector2.ZERO

func _enter_tree() -> void:
	reset_physics_interpolation.call_deferred()

func set_gravity(attractor: GravityField, gravity: Vector2, dist_sq: float):
	var cur_prior := get_current_gravity_priority()
	if attractor.gravity_priority < cur_prior:
		return
	elif attractor.gravity_priority == cur_prior && dist_sq >= _current_attractor_dist_sq:
		return
	_current_gravity = gravity
	_current_attractor = attractor
	_current_attractor_dist_sq = dist_sq

func get_last_gravity() -> Vector2: 
	return _last_gravity

func get_current_gravity_priority() -> int:
	if is_instance_valid(_current_attractor):
		return _current_attractor.gravity_priority
	return 0

func _physics_process(delta):
	if _last_gravity != _current_gravity:
		gravity_changed.emit(_last_gravity, _current_gravity)
		if (grounded != null && grounded.is_on_ground() && 
		_last_gravity != Vector2.ZERO && _current_gravity != Vector2.ZERO):
			var last_up_axis = -_last_gravity.normalized()
			var last_fwd_axis = last_up_axis.rotated(PI * 0.5)
			var last_up_vel = last_up_axis.dot(linear_velocity)
			var last_fwd_vel = last_fwd_axis.dot(linear_velocity)
			var cur_up_axis = -_current_gravity.normalized()
			var cur_fwd_axis = cur_up_axis.rotated(PI * 0.5)
			linear_velocity = last_fwd_vel * cur_fwd_axis + last_up_vel * cur_up_axis
	linear_velocity += _current_gravity * delta * gravity_scale
	_last_gravity = _current_gravity
	_current_gravity = Vector2.ZERO
	_current_attractor = null
	_current_attractor_dist_sq = INF

func _integrate_forces(state):
	integrate_forces.emit(state)

static func get_contact_points(a: CollisionObject2D, b: CollisionObject2D) -> PackedVector2Array:
	for owner_a in a.get_shape_owners():
		var shape_trans_a = a.shape_owner_get_transform(owner_a)
		for s_i in range(0, a.shape_owner_get_shape_count(owner_a)):
			var shape_a = a.shape_owner_get_shape(owner_a, s_i)
			for owner_b in b.get_shape_owners():
				var shape_trans_b = b.shape_owner_get_transform(owner_b)
				for i in range(0, b.shape_owner_get_shape_count(owner_b)):
					var shape_b = b.shape_owner_get_shape(owner_b, i)
					var contacts = shape_a.collide_and_get_contacts(\
						shape_trans_a, shape_b, shape_trans_b)
					if contacts.size() > 0:
						return contacts
	return []
