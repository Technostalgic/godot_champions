class_name ProjectileTarget
extends DynamicGravityBody

signal on_hit(projectile: Node, damage: float, point: Vector2)
signal on_kill()

enum Allegiance { NUETRAL, HERO, ENEMY }

@export var health: Health = null
@export var allegiance: Allegiance = Allegiance.NUETRAL
@export var knockback_multiplier: float = 1.0
@export var wound_type: Wound.Type = Wound.Type.FLESH
@export var wound_area: CollisionObject2D = null

var _last_projectile_struck: Projectile = null
var _last_struck_pos: Vector2 = Vector2.ZERO
var _last_struck_impulse: Vector2 = Vector2.ZERO

@export var wound_container: Node2D = null : 
	get:
		if wound_container == null:
			wound_container = Node2D.new()
			add_child(wound_container)
		return wound_container

var dead: bool = false
var wounds: Array[Wound] = []
var wound_threshold: float = 5
var wound_level: float = 0

func _enter_tree() -> void:
	dead = false

func _ready() -> void:
	if wound_area == null: 
		wound_area = self

func add_wound(wound: Wound, proj: Projectile, point: Vector2, normal: Vector2):
	wounds.push_back(wound)
	wound_container.add_child(wound)
	wound.on_added(self, proj, point, normal)
	check_wound_positioning(wound)
	wound_level = 0

func steal_wound(wound_index: int, from: Actor):
	var wound := from.wounds[wound_index]
	wounds.push_back(wound)
	wound.reparent(wound_container)
	check_wound_positioning(wound)
	from.wounds.remove_at(wound_index)

func check_wound_positioning(wound: Wound):
	var trans = wound_container.global_transform
	var pt = GravityField.project_on_collider(wound_area, wound.global_position, trans)
	wound.global_position = pt

func is_alive() -> bool:
	return !dead

func hit(proj: Projectile, damage: float, point: Vector2):
	on_hit.emit(proj, damage, point)
	
	_last_projectile_struck = proj
	_last_struck_pos = point * global_transform
	
	var impulse := Vector2.ZERO
	if proj is DynamicProjectile:
		impulse = (proj._last_vel.normalized() * 
			proj.proj_stats.knockback * 
			knockback_multiplier * 
			mass)
		apply_impulse(impulse, point - global_position)
	elif proj is MeleeHit:
		impulse = proj.direction * proj.knockback * knockback_multiplier * mass
		apply_central_impulse(impulse)
	elif proj is Explosion:
		impulse = (global_position - proj.global_position).normalized()
		impulse *= proj.proj_stats.knockback * knockback_multiplier * mass
		apply_central_impulse(impulse)
	_last_struck_impulse = impulse
	
	if is_instance_valid(health):
		health.hit(proj, damage, point)
	
	wound_level += damage
	
	if is_instance_valid(proj):
		var proj_owner = proj.proj_owner
		if is_instance_valid(proj_owner):
			proj_owner = proj_owner.wep_owner
		if is_instance_valid(proj_owner) && proj_owner is Hero:
			Global.damage_number_at(proj_owner.player_id, damage, point)

func apply_damage(proj: Projectile, damage: float, point: Vector2):
	if is_instance_valid(health):
		health.damage(proj, damage, point)
	
	wound_level += damage
	
	if is_instance_valid(proj):
		var proj_owner = proj.proj_owner
		if is_instance_valid(proj_owner):
			proj_owner = proj_owner.wep_owner
		if is_instance_valid(proj_owner) && proj_owner is Hero:
			Global.damage_number_at(proj_owner.player_id, damage, point)

func wound_fraction() -> float:
	return wound_level / wound_threshold

func remove_wounds():
	for wound in wounds:
		Wounds.pool_wound(wound)
	wounds.clear()

func kill():
	if is_alive():
		on_kill.emit()
	dead = true

func remove():
	remove_wounds()
	queue_free()
