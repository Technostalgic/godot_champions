class_name DamageNumbers
extends RefCounted

const DISPLAY_TIME: float = 0.5
const MAX_DIST_SQ: float = 16 * 16

class DamageText:
	extends Node2D
	
	var label: Label = null
	var timer: float = 0
	var damage_total: float = 0
	
	static func create() -> DamageText:
		var txt = DamageText.new()
		txt.label = Label.new()
		txt.label.horizontal_alignment = HORIZONTAL_ALIGNMENT_CENTER
		txt.label.vertical_alignment = VERTICAL_ALIGNMENT_CENTER
		txt.label.set_anchors_preset(Control.PRESET_CENTER)
		txt.label.custom_minimum_size = Vector2.ONE
		var label_container = Node2D.new()
		label_container.add_child(txt.label)
		label_container.scale = Vector2.ONE * 0.35
		txt.add_child(label_container)
		return txt

	func _ready():
		label.force_update_transform()

	func _process(delta: float):
		if !is_visible_in_tree():
			return
		timer -= delta
		if timer <= 0:
			hide_display()
	
	func add_damage(damage: float):
		if !is_visible_in_tree():
			show_display()
		damage_total += damage
		timer = DamageNumbers.DISPLAY_TIME
		update_text()

	func update_text():
		label.text = str(roundf(damage_total))

	func hide_display():
		damage_total = 0
		hide()
		
	func show_display():
		show()

var _damage_texts: Array[DamageText] = []

func initialize():
	for plr in Global.heros:
		var txt = DamageText.create()
		Global.root_2d_world.add_child(txt)
		_damage_texts.push_back(txt)

func add_damage_at(player_index: int, damage: float, position: Vector2):
	var dmg_txt = _damage_texts[player_index]
	var dist_sq = dmg_txt.global_position.distance_squared_to(position)
	if dist_sq > DamageNumbers.MAX_DIST_SQ:
		dmg_txt.global_position = position
	dmg_txt.add_damage(damage)
