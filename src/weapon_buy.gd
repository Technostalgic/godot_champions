class_name WeaponBuy
extends Interactible

@export var weapon_prefab: PackedScene
@export var label: Label = null
@export var graphic: Sprite2D = null

func interact(interactor: Actor):
	if interactor is Hero:
		var wep = weapon_prefab.instantiate() as Weapon
		for child in interactor.weapon_slot.get_children():
			child.queue_free()
		interactor.weapon_slot.add_child(wep)
	super.interact(interactor)
