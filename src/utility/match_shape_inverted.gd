@tool
class_name MatchShapeInverted
extends Node

@export var source_shape: SS2D_Shape = null
@export var target_shape: SS2D_Shape = null
@export var target_collision: CollisionPolygon2D = null
@export var expansion_radius: float = 500

## Inspector button to update the shape
@export var force_update: bool = false :
	set(_value):
		if Engine.is_editor_hint():
			update_shape()

func update_shape():
	var inner_poly := source_shape.generate_collision_points()
	var outer_poly := Geometry2D.offset_polygon(inner_poly, expansion_radius, Geometry2D.JOIN_SQUARE)[0]
	var final_poly := [] as PackedVector2Array
	var shift_outer := 0
	var outer_poly_ct := outer_poly.size()
	
	while Geometry2D.triangulate_polygon(final_poly).size() <= 0:
		if shift_outer >= outer_poly_ct:
			push_error("cannot create polygon")
			return
		final_poly.clear()
		
		for inner_vert in inner_poly:
			final_poly.push_back(inner_vert)
		final_poly.push_back(inner_poly[0])
	
		for i in outer_poly_ct:
			var index := wrapi(outer_poly_ct - i - 1 + shift_outer, 0, outer_poly_ct)
			final_poly.push_back(outer_poly[index])
		final_poly.push_back(outer_poly[wrapi(shift_outer - 1, 0, outer_poly_ct)])
		
		shift_outer += 1
	
	target_shape._points.clear()
	for inner_vert in final_poly:
		target_shape.add_point(inner_vert)
	target_shape.close_shape()
	
	update_collision_poly.call_deferred(final_poly)

func update_collision_poly(points: PackedVector2Array):
	if target_collision == null:
		return
	target_collision.polygon = points
	target_collision.global_transform = target_shape.global_transform
