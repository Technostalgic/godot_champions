class_name Shop
extends Node

const ITEM_SPACING: float  = 24

@export var shop_slot_prefab: PackedScene
@export var reroll_interatible: Interactible
@export var max_items: int = 3
@export var reroll_cost: int = 25
@export var item_container: Node2D = null
@export var reroll_cost_label: Label = null
@export var loot_pool: LootPool = null

var cost_multiplier: float = 1

func _ready():
	cost_multiplier = max(1, 0.5 + WaveState.wave_number * 0.1)
	reroll_shop(null)
	reroll_interatible.on_interact.connect(reroll_shop)
	update_reroll_label()

func reroll_shop(interactor: Actor):
	if is_instance_valid(interactor) && interactor is Hero:
		var inv = Inventory.find_inventory_child(interactor)
		if inv.gold < reroll_cost:
			return
		inv.gold -= reroll_cost
		reroll_cost = (reroll_cost as float * 1.25) as int
		update_reroll_label()
	
	for slot in item_container.get_children():
		if slot is ShopSlot:
			slot.queue_free()
	create_items()

func update_reroll_label():
	reroll_cost_label.text = str("$", reroll_cost)

func create_items():
	for i in range(max_items):
		var off := Vector2.from_angle(item_container.global_rotation) * i * Shop.ITEM_SPACING
		var slot = shop_slot_prefab.instantiate()
		if slot is ShopSlot:
			slot.set_item(loot_pool.pick_random().instantiate(), cost_multiplier)
			item_container.add_child(slot)
			slot.global_position = item_container.global_position + off
