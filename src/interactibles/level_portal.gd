class_name LevelPortal
extends Interactible

static var heros_interacted: Array[Hero] = []
static var portals_selected: Array[LevelPortal] = []

static func pick_level_transition():
	
	var level: Level = null
	
	if WaveState.wave_number % 4 == 0:
		WaveState.wave_number += 1
		level = Global.shop_arena.instantiate() as Level
	else:
		var portal := LevelPortal.portals_selected.pick_random() as LevelPortal
		level = portal.picked_level
	
	LevelPortal.portals_selected.clear()
	LevelPortal.heros_interacted.clear()
	Global.set_level(level)
	Global.respawn_heros()
	
	if !level.is_safe_room:
		WaveState.paused = false
		WaveState.restart_wave()
	
	# give heroes a bit of health and ammo
	for hero in Global.heros:
		hero.health.health += hero.health.max_health * 0.2
		if hero.health.health > hero.health.max_health:
			hero.health.health = hero.health.max_health
		var wep = hero.get_equipped_weapon()
		if wep is ModableWeapon:
			var max_ammo = wep.get_weapon_stats().max_ammo
			wep.current_ammo += max_ammo * 0.25
			if wep.current_ammo > max_ammo:
				wep.current_ammo = max_ammo
			wep.current_ammo = roundf(wep.current_ammo)

@export var label_container: Node2D = null
# TODO fix not being able to use this property because of recursive dependencies
@export var level_pool: Array[PackedScene] = [] 

var picked_level: Level = null:
	get:
		if picked_level == null:
			var arr = level_pool
			if arr.size() <= 0:
				arr = Global.all_arenas
			picked_level = arr.pick_random().instantiate() as Level
		return picked_level

func _ready() -> void:
	label_container.global_rotation = 0
	super._ready()

func interact(interactor: Actor):
	if interactor is Hero:
		if !heros_interacted.has(interactor):
			LevelPortal.heros_interacted.push_back(interactor)
		if !LevelPortal.portals_selected.has(self):
			LevelPortal.portals_selected.push_back(self)
		if LevelPortal.heros_interacted.size() >= Global.heros.size():
			LevelPortal.pick_level_transition()
	super.interact(interactor)
