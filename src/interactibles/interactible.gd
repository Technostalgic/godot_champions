class_name Interactible
extends Node2D

signal on_interact(interactor: Actor)
signal on_interact_hold(interactor: Actor)
signal interactor_entered(interactor: Actor)
signal interactor_exited(interactor: Actor)

@export var collider: Area2D
@export var auto_interact: bool = false
@export var show_interaction_tooltip: bool = true
@export var tooltip_offset: Vector2 = Vector2.ZERO
@export var activate_triggers: Array[Trigger] = []

@export var enabled: bool = true :
	set(value):
		enabled = value
		if enabled: _enabled_check()
		else: _disabled_check()

var cached_actors: Array[Actor] = []

func _ready():
	collider.body_entered.connect(_on_body_entered)
	collider.body_exited.connect(_on_body_exited)

func _on_body_entered(body: Node2D):
	if body is Hero:
		if enabled:
			_interaction_available(body)
		if !cached_actors.has(body):
			cached_actors.push_back(body)

func _on_body_exited(body: Node2D):
	if body is Hero:
		if enabled:
			_interaction_unavailable(body)
		if cached_actors.has(body):
			cached_actors.erase(body)

func _interaction_available(body: Node2D):
	if body is Hero:
		body.interactor.add_interactible(self)
		interactor_entered.emit(body)
		if show_interaction_tooltip:
			var tip: GameTooltipManagerClass.TrackedTipInfo = \
				GameTooltipManager.show_tracked_tooltip(body.player_id, "'E'", 15)
			tip.offset_dir = Vector2(signf(tooltip_offset.x), signf(tooltip_offset.y))
			tip.offset_pos = tooltip_offset
			tip.follow_node = self

func _interaction_unavailable(body: Node2D):
	if body is Hero:
		body.interactor.remove_interactible(self)
		interactor_exited.emit(body)
		if show_interaction_tooltip:
			GameTooltipManager.hide_tracked_tooltip(body.player_id)

func _enabled_check():
	for body in cached_actors:
		if body is Hero:
			_interaction_available(body)
	cached_actors.clear()

func _disabled_check():
	for body in cached_actors:
		if body is Hero:
			_interaction_unavailable(body)
	cached_actors.clear()

func interact(interactor: Actor):
	on_interact.emit(interactor)
	var i: int = 0
	for trigger in activate_triggers:
		if !is_instance_valid(trigger):
			activate_triggers.remove_at(i)
			continue
		trigger.send_action_paramater(interactor)
		trigger.condition.meet_condition()
		i += 1

func interact_hold(interactor: Actor):
	interact(interactor)
	on_interact_hold.emit(interactor)
