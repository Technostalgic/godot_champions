class_name ShopSlot
extends Interactible

@export var icon: Sprite2D = null
@export var name_label: Label = null
@export var cost_label: Label = null
@export var permanent: bool = false

var _item: Item = null
var _item_cost: int = 25

func _ready() -> void:
	super._ready()
	for child in get_children():
		if child is Item:
			set_item(child)

func interact(interactor: Actor):
	super.interact(interactor)
	try_buy(Inventory.find_inventory_child(interactor))

func set_item(item: Item, cost_multiplier: float = 1):
	_item = item
	_item_cost = (item.cost as float * cost_multiplier) as int
	if !item.is_inside_tree():
		add_child(item)
	refresh_display()

func refresh_display():
	icon.texture = _item.icon
	name_label.text = _item.title
	cost_label.text = str("$", get_cost())

func get_cost() -> int:
	return _item_cost

func try_buy(target_inventory: Inventory) -> bool:
	if target_inventory.gold < get_cost():
		return false
	target_inventory.gold -= get_cost()
	if permanent:
		target_inventory.add_item(_item.duplicate())
	else:
		target_inventory.add_item(_item)
		queue_free()
	return true
