class_name SpawnChecker
extends Node2D

const CHECK_COUNT: int = 10
const CHECK_INTERVAL: float = 0.25

var target: CollisionObject2D = null
var check_point: Vector2 = Vector2.ZERO
var check_cooldown: float = SpawnChecker.CHECK_INTERVAL
var checks_left: int = SpawnChecker.CHECK_COUNT

# create and attach a spawn checker to the specified target
static func create_checker(target_checked: CollisionObject2D, point: Vector2) -> SpawnChecker:
	var checker: SpawnChecker = SpawnChecker.new()
	checker.target = target_checked
	checker.check_point = point
	target_checked.add_child(checker)
	return checker

func _physics_process(delta: float):
	handle_checking(delta)

func handle_checking(delta: float):
	check_cooldown -= delta
	if check_cooldown <= 0:
		check_cooldown = SpawnChecker.CHECK_INTERVAL
		checks_left -= 1
		if can_see_point():
			queue_free()
		elif checks_left <= 0:
			if target is Mob:
				target.remove()
			else:
				target.queue_free()
			push_warning("Target spawned outside of level")

func can_see_point() -> bool:
	var space_state = PhysicsServer2D.space_get_direct_state(target.get_world_2d().space)
	var ray = PhysicsRayQueryParameters2D.create(
		target.global_position,
		check_point,
		1 << 0)
	var result = space_state.intersect_ray(ray)
	return result.is_empty()
