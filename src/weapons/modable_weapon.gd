class_name ModableWeapon
extends BasicWeapon

signal mods_changed()
signal spawn_primary_projectile(proj: Projectile)
signal spawn_secondary_projectile(proj: Projectile)
signal switch_mod_mode(alt_mods: bool)

enum ModSwitchMode { SWAP, HOLD, PRESS, _TOTAL }

static func mod_switch_mode_to_str(mode: ModSwitchMode) -> String:
	match mode:
		ModSwitchMode.SWAP:
			return "Switch"
		ModSwitchMode.HOLD:
			return "Hold"
		ModSwitchMode.PRESS:
			return "Press"
	return "Unknown"

@export var title: String
@export var mod_switch_mode: ModSwitchMode = ModSwitchMode.SWAP
@export var power_stat_bonuses: PowerStatBonus = null
@export var mod_container_1: WeaponModContainer = null
@export var mod_container_2: WeaponModContainer = null

var wep_stats_cached: Stats.WeaponStats = null
var shot_stats_1_cached: Stats.ShotStats = null
var shot_stats_2_cached: Stats.ShotStats = null
var shot_effects_1_cached: Array[HitEffect] = []
var shot_effects_2_cached: Array[HitEffect] = []

var carry_primary_mods = true
var using_alt_mods: bool = false
var current_ammo: float = 0
var current_energy: float = 0

var was_alt_pressed: bool = false
var is_alt_pressed: bool = false
var alt_received: bool = false
var lock_mod_mode: bool = false

var live_projectiles: Array[Projectile] = []
var live_primary_projectiles: Array[Projectile] = []
var live_secondary_projectiles: Array[Projectile] = []
var pool1: Array[Projectile] = []
var pool2: Array[Projectile] = []
var pools_dirty: bool = false

static func get_weapon(node: Node) -> ModableWeapon:
	if node is ModableWeapon:
		return node
	var wep: ModableWeapon = null
	for child in node.get_children():
		wep = get_weapon(child)
		if wep != null:
			break
	return wep

func _receive_trigger():
	is_triggered = true
	trigger_received = true

func _receive_alt():
	is_alt_pressed = true
	alt_received = true

func _ready():
	mod_container_1.mods_changed.connect(_on_mods_changed)
	mod_container_2.mods_changed.connect(_on_mods_changed)
	prepool_projectiles(10)
	var stats: Stats.WeaponStats = get_weapon_stats()
	current_ammo = stats.max_ammo
	ammo_in_magazine = stats.magazine_size

func _on_mods_changed():
	wep_stats_cached = null
	shot_stats_1_cached = null
	shot_stats_2_cached = null
	shot_effects_1_cached.clear()
	shot_effects_2_cached.clear()
	pools_dirty = true
	power_stat_bonuses.level = 0
	mods_changed.emit()

func _physics_process(delta: float):
	if pools_dirty:
		for i in range(live_projectiles.size() - 1, -1, -1):
			if is_instance_valid(live_projectiles[i]):
				live_projectiles[i].queue_free()
		live_projectiles.clear()
		prepool_projectiles(10)
	if is_instance_valid(wep_owner):
		update_pos_from_aim()
	global_rotation = aim.angle()
	if fire_delay > 0:
		fire_delay -= delta
	elif !is_triggered && fire_delay < 0:
		fire_delay = 0
	if is_reloading:
		handle_reload(delta)
	if !lock_mod_mode:
		handle_mod_mode()
	if is_triggered:
		if !is_reloading:
			trigger(delta)
	elif !was_triggered:
		shots_triggered = 0
		trigger_charge = 0
	was_triggered = is_triggered
	was_alt_pressed = is_alt_pressed
	fire_cooldown -= delta
	handle_energy(delta)

func _process(_delta: float):
	if !trigger_received:
		is_triggered = false
	if !alt_received:
		is_alt_pressed = false
	trigger_received = false
	alt_received = false

func handle_energy(delta: float):
	var stats = get_weapon_stats()
	current_energy += stats.energy_regeneration * delta
	if current_energy > stats.max_energy:
		current_energy = stats.max_energy

func get_weapon_stats() -> Stats.WeaponStats:
	if wep_stats_cached == null:
		wep_stats_cached = wep_base_stats.get_weapon_stats()
		mod_container_1.mod_weapon_stats(wep_stats_cached)
		mod_container_2.mod_weapon_stats(wep_stats_cached)
	return wep_stats_cached

func get_shot_stats() -> Stats.ShotStats:
	if using_alt_mods: 
		if shot_stats_2_cached == null:
			shot_stats_2_cached = wep_base_stats.get_shot_stats()
			shot_effects_2_cached.clear()
			if carry_primary_mods:
				WeaponModContainer.multi_mod_shot_stats(
					[mod_container_1, mod_container_2], 
					shot_stats_2_cached)
				for mod in mod_container_1.mods:
					for fx in mod.hit_effects:
						shot_effects_2_cached.push_back(fx)
			else:
				mod_container_2.mod_shot_stats(shot_stats_2_cached)
			for mod in mod_container_2.mods:
				for fx in mod.hit_effects:
					shot_effects_2_cached.push_back(fx)
			power_stat_bonuses.level = shot_stats_2_cached.power
			power_stat_bonuses.modify_shot_stats(shot_stats_2_cached)
			power_stat_bonuses.level = 1
		return shot_stats_2_cached
	else:
		if shot_stats_1_cached == null:
			shot_stats_1_cached = wep_base_stats.get_shot_stats()
			mod_container_1.mod_shot_stats(shot_stats_1_cached)
			shot_effects_1_cached.clear()
			for mod in mod_container_1.mods:
				for fx in mod.hit_effects:
					shot_effects_1_cached.push_back(fx)
			power_stat_bonuses.level = shot_stats_1_cached.power
			power_stat_bonuses.modify_shot_stats(shot_stats_1_cached)
			power_stat_bonuses.level = 1
		return shot_stats_1_cached

func get_mod_container() -> WeaponModContainer:
	if using_alt_mods:
		return mod_container_2
	else:
		return mod_container_1

func handle_mod_mode():
	var was_using_alt_mods := using_alt_mods
	match mod_switch_mode:
		ModSwitchMode.SWAP:
			if is_alt_pressed && !was_alt_pressed:
				using_alt_mods = !using_alt_mods
		ModSwitchMode.PRESS:
			using_alt_mods = is_alt_pressed
			if is_alt_pressed:
				_receive_trigger()
		ModSwitchMode.HOLD:
			using_alt_mods = is_alt_pressed
	if was_using_alt_mods != using_alt_mods:
		is_triggered = false
		switch_mod_mode.emit(using_alt_mods)

func set_mod_mode(alt_mods: bool):
	var was_using_alt_mods := using_alt_mods
	using_alt_mods = alt_mods
	if was_using_alt_mods != using_alt_mods:
		switch_mod_mode.emit(using_alt_mods)

func prepool_projectiles(count: int):
	if pools_dirty:
		for proj in pool1:
			if is_instance_valid(proj):
				proj.queue_free()
		for proj in pool2:
			if is_instance_valid(proj):
				proj.queue_free()
		pool1.clear()
		pool2.clear()
	for i in range(0, count):
		pool1.push_back(instantiate_projectile())
	set_mod_mode(true)
	for i in range(0, count):
		pool2.push_back(instantiate_projectile())
	set_mod_mode(false)
	pools_dirty = false

func instantiate_projectile() -> Projectile:
	var wep_stats: Stats.WeaponStats = get_weapon_stats()
	var stats: Stats.ShotStats = get_shot_stats()
	var proj: Node = stats.projectile.instantiate()
	if proj is Projectile:
		proj.proj_stats = get_shot_stats().projectile_stats
		proj.is_alive = false
		stats.on_create_projectile.emit(proj)
		wep_stats.on_create_projectile.emit(proj)
		if using_alt_mods:
			spawn_secondary_projectile.emit(proj)
			for fx in shot_effects_2_cached:
				var cf = fx.duplicate()
				cf.set_level(fx.level)
				proj.add_child(cf)
		else:
			spawn_primary_projectile.emit(proj)
			for fx in shot_effects_1_cached:
				var cf = fx.duplicate()
				cf.set_level(fx.level)
				proj.add_child(cf)
	else:
		push_error("projectile scene reference is not a projectile node")
		proj.queue_free()
		return null
	return proj

# get projectiles from pool (or create them if pool is empty) and add them to the scene tree
func get_primary_projectiles(count: int) -> Array[Projectile]:
	var arr: Array[Projectile] = []
	var root_node = Global.root_2d_world
	while count > 0:
		var proj: Projectile = pool1.pop_back()
		if proj == null:
			proj = instantiate_projectile()
		root_node.add_child(proj)
		arr.push_back(proj)
		live_projectiles.push_back(proj)
		live_primary_projectiles.push_back(proj)
		count -= 1
	return arr
	
func get_secondary_projectiles(count: int) -> Array[Projectile]:
	var arr: Array[Projectile] = []
	var root_node = Global.root_2d_world
	while count > 0:
		var proj: Projectile = pool2.pop_back()
		if proj == null:
			proj = instantiate_projectile()
		root_node.add_child(proj)
		arr.push_back(proj)
		live_projectiles.push_back(proj)
		live_secondary_projectiles.push_back(proj)
		count -= 1
	return arr

func trigger(delta: float):
	if is_reloading:
		return
	var stats: Stats.ShotStats = get_shot_stats()
	if stats.ammo_cost > ammo_in_magazine:
		start_reload()
		return
	trigger_charge += delta
	if stats.fire_mode == Stats.FireMode.SEMI_AUTO:
		if shots_triggered >= 1:
			return
	if trigger_charge >= stats.fire_delay && fire_cooldown <= 0:
		while fire_delay <= 0:
			if current_energy >= stats.energy_cost:
				fire(delta)
				shots_triggered += 1
			else: 
				fire_delay = maxf(0, fire_delay)
				break
			fire_delay = maxf(0, fire_delay)

func handle_reload(delta: float):
	reload_cooldown -= delta
	if reload_cooldown <= 0:
		var stats: Stats.WeaponStats = get_weapon_stats()
		var ammo_needed = stats.magazine_size - ammo_in_magazine
		var ammo_take = minf(ammo_needed, current_ammo)
		current_ammo -= ammo_take
		ammo_in_magazine += ammo_take
		is_reloading = false

func fire(_delta: float):
	var shot_stats: Stats.ShotStats = get_shot_stats()
	var wep_stats: Stats.WeaponStats = get_weapon_stats()
	var delay = 1 / wep_stats.fire_rate
	ammo_in_magazine -= shot_stats.ammo_cost
	current_energy -= shot_stats.energy_cost
	fire_cooldown = shot_stats.cooldown
	fire_delay += delay
	var projs: Array[Projectile]
	if !using_alt_mods:
		projs = get_primary_projectiles(round(shot_stats.projectile_count))
	else:
		projs = get_secondary_projectiles(round(shot_stats.projectile_count))
	for proj in projs:
		proj.release_from(self)
		shot_stats.on_fire_projectile.emit(proj)
	emit_projectiles.emit(projs)
	on_fired.emit()

func on_projectile_removed(proj: Projectile):
	live_projectiles.erase(proj)
	var ind := live_primary_projectiles.find(proj)
	if ind >= 0:
		live_primary_projectiles.remove_at(ind)
		pool1.push_back(proj)
	else:
		ind = live_secondary_projectiles.find(proj)
		if ind >= 0:
			live_secondary_projectiles.remove_at(ind)
			pool2.push_back(proj)
