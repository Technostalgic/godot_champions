class_name BasicWeapon
extends Weapon

signal on_fired()

@export var max_aim_distance: float = 5

var is_triggered: bool = false
var trigger_received: bool = false
var aim: Vector2 = Vector2.ZERO
var fire_delay: float = 0
var fire_cooldown: float = 0
var reload_cooldown: float = 0
var trigger_charge: float = 0
var shots_triggered: int = 0
var is_reloading: bool = false
var was_triggered: bool = false
var ammo_in_magazine: float = 0

func _ready():
	ammo_in_magazine = get_weapon_stats().magazine_size

func _receive_trigger():
	is_triggered = true
	trigger_received = true

func _receive_reload():
	if !is_reloading:
		start_reload()

func _receive_aim(aim_vec: Vector2):
	if aim.length_squared() > 1:
		aim = aim_vec.normalized()
	else:
		aim = aim_vec

func _process(_delta: float):
	if !trigger_received:
		is_triggered = false
	trigger_received = false

func _physics_process(delta: float):
	if is_instance_valid(wep_owner):
		update_pos_from_aim()
	global_rotation = aim.angle()
	if fire_delay > 0:
		fire_delay -= delta
	elif !is_triggered && fire_delay < 0:
		fire_delay = 0
	if is_reloading:
		handle_reload(delta)
	if is_triggered:
		if !is_reloading:
			trigger(delta)
	elif !was_triggered:
		shots_triggered = 0
		trigger_charge = 0
	was_triggered = is_triggered
	fire_cooldown -= delta

func update_pos_from_aim():
	var parent = get_parent()
	if parent is Node2D:
		global_position = parent.global_position + aim * max_aim_distance

func start_reload():
	var stats: Stats.WeaponStats = get_weapon_stats()
	reload_cooldown = stats.reload_time
	is_reloading = true
	begin_reload.emit()

func handle_reload(delta: float):
	reload_cooldown -= delta
	if reload_cooldown <= 0:
		ammo_in_magazine += get_weapon_stats().magazine_size
		is_reloading = false

func trigger(delta: float):
	if is_reloading:
		return
	var stats: Stats.ShotStats = get_shot_stats()
	if stats.ammo_cost > ammo_in_magazine:
		start_reload()
		return
	trigger_charge += delta
	if stats.fire_mode == Stats.FireMode.SEMI_AUTO:
		if shots_triggered >= 1:
			return
	if trigger_charge >= stats.fire_delay && fire_cooldown <= 0:
		while fire_delay <= 0:
			fire(delta)
			shots_triggered += 1
			fire_delay = maxf(0, fire_delay)

func fire(_delta: float):
	var shot_stats: Stats.ShotStats = get_shot_stats()
	var wep_stats: Stats.WeaponStats = get_weapon_stats()
	var delay = 1 / wep_stats.fire_rate
	ammo_in_magazine -= shot_stats.ammo_cost
	fire_cooldown = shot_stats.cooldown
	fire_delay += delay
	var count = shot_stats.projectile_count
	while count > 0:
		count -= 1
		var proj = shot_stats.projectile.instantiate() as Projectile
		Global.root_2d_world.add_child(proj)
		proj.release_from(self)
		shot_stats.on_fire_projectile.emit(proj)
	on_fired.emit()
