class_name Weapon
extends Node2D

signal emit_projectiles(projectiles: Array[Projectile])
signal begin_reload()

@export var graphic: Node2D
@export var attack_target: Node2D
@export var wep_owner: ProjectileTarget = null

var wep_base_stats: WeaponBaseStats = null

func _receive_trigger():
	pass

func _receive_aim(_aim_vec: Vector2):
	pass

func _enter_tree():
	find_base_stats()
	var parent := get_parent()
	while parent != null:
		if parent is ProjectileTarget:
			wep_owner = parent
			break
		parent = parent.get_parent()

func get_attack_target() -> Node2D:
	if !is_instance_valid(attack_target):
		return self
	return attack_target
	
func find_base_stats():
	for child in get_children():
		if child is WeaponBaseStats:
			attach_stats(child)
			break

func attach_stats(stats: WeaponBaseStats):
	wep_base_stats = stats

func detach_stats():
	wep_base_stats = null

func on_projectile_removed(proj: Projectile):
	proj.queue_free()

func get_weapon_stats() -> Stats.WeaponStats:
	return wep_base_stats.get_weapon_stats()

func get_shot_stats() -> Stats.ShotStats:
	return wep_base_stats.get_shot_stats()
