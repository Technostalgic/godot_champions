extends WeaponEffect

@export var base_burst_count: int = 3
@export var burst_count_increment: int = 1
@export var fire_rate_penalty_mult: float = 2
@export var fire_rate_mult: float = 2

var total_burst_count: int = 0
var _burst_sequence: int = 0
var _prev_shots_triggered: int = 0

func _enter_weapon():
	var wep = get_weapon()
	if wep is BasicWeapon:
		if !wep.on_fired.is_connected(_on_fired):
			wep.on_fired.connect(_on_fired)

func _physics_process(delta: float):
	var wep = get_weapon()
	if !is_instance_valid(wep) || !is_active():
		return
	if _burst_sequence > 0:
		handle_burst(delta)

func _on_fired():
	if !is_active():
		return
	if _burst_sequence <= 0:
		start_burst()
	else:
		_burst_sequence -= 1
		var wep := get_weapon()
		if wep is BasicWeapon:
			var shot_stats = wep.get_shot_stats()
			if wep.ammo_in_magazine < shot_stats.ammo_cost:
				end_burst()
				wep.start_reload()
				return
			if wep is ModableWeapon:
				if wep.current_energy < shot_stats.energy_cost:
					end_burst()
					return
		if _burst_sequence <= 0:
			end_burst()

func start_burst():
	var wep = get_weapon()
	_burst_sequence = total_burst_count - 1
	if wep is BasicWeapon:
		_prev_shots_triggered = wep.shots_triggered - 1
		if wep is ModableWeapon:
			wep.lock_mod_mode = true

func end_burst():
	var wep = get_weapon()
	if wep is BasicWeapon:
		wep.trigger_charge = 0
		wep.fire_delay = 1 / wep.get_weapon_stats().fire_rate * fire_rate_penalty_mult
		wep.shots_triggered = _prev_shots_triggered + 1
		if wep is ModableWeapon:
			wep.lock_mod_mode = false
	_burst_sequence = 0
	_prev_shots_triggered = 0

func handle_burst(delta: float):
	var wep = get_weapon()
	if wep is BasicWeapon:
		if wep.is_reloading:
			end_burst()
			return
		wep.is_triggered = true
		wep.was_triggered = true
		wep.fire_cooldown = 0
		wep.fire_delay -= (delta * (fire_rate_mult - 1))
		wep.shots_triggered = _prev_shots_triggered
		wep.trigger(delta)

func set_level(lvl: int):
	total_burst_count = base_burst_count + burst_count_increment * (lvl - 1)

func get_stat_string() -> Array[HitEffect.StatString]:
	return [
		HitEffect.StatString.create("Shots", str(total_burst_count))
	]
