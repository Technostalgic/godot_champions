class_name LaserSight
extends WeaponEffect

@export var laser_color: Color = Color.RED
@export var use_player_color: bool = true
@export var laser_width: float = 1

var is_enabled: bool = true
var laser_sprite: Sprite2D = null
var laser_ray: RayCast2D = null

func _exit_tree():
	if laser_sprite != null:
		LineFx.pool_line(laser_sprite)
		laser_sprite = null

func _enter_weapon():
	if laser_sprite == null:
		create_sprite()
		create_ray()

func _physics_process(_delta: float):
	if !is_active():
		if is_instance_valid(laser_sprite):
			laser_sprite.hide()
		return
	var wep = get_weapon()
	if is_enabled:
		set_aim_gfx(wep)
	else:
		laser_sprite.hide()

func update_laser_ray(wep: Weapon):
	var target_pos := Vector2.from_angle(wep.global_rotation) * 5000
	laser_ray.global_position = wep.get_attack_target().global_position
	laser_ray.target_position = target_pos
	laser_ray.force_raycast_update()

func set_aim_gfx(wep: Weapon):
	if !is_instance_valid(wep):
		laser_sprite.hide()
		return
	
	update_laser_ray(wep)
	var target_pos := Vector2.from_angle(wep.global_rotation) * 5000
	if laser_ray.is_colliding():
		target_pos = laser_ray.get_collision_point()
	
	var wep_pos := wep.get_attack_target().global_position
	LineFx.stretch_line(laser_sprite, wep_pos, target_pos)
	laser_sprite.show()
	
func create_ray():
	laser_ray = RayCast2D.new()
	laser_ray.collision_mask = 1 << 0 | 1 << 2 | 1 << 3
	add_child(laser_ray)
	laser_ray.hide()

func create_sprite():
	if use_player_color:
		var wep := get_weapon()
		if is_instance_valid(wep):
			if is_instance_valid(wep.wep_owner):
				var wep_owner := wep.wep_owner
				if wep_owner is Hero:
					laser_color = wep_owner.color
	if laser_sprite != null:
		LineFx.pool_line(laser_sprite)
		laser_sprite = null
	laser_sprite = LineFx.pick_line()
	laser_sprite.modulate = laser_color
	laser_sprite.scale.y = laser_width
