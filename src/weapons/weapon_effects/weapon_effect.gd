class_name WeaponEffect
extends Node

@export var parent_wep: Weapon = null
@export var parent_wep_mod: WeaponMod = null
@export var use_alt: bool = false

var _wep_alt_checked: bool = false
var _is_alt: bool = false

func _init():
	set_level(1)

func _exit_tree():
	_wep_alt_checked = false

func _enter_tree():
	_wep_alt_checked = false
	_check_enter_weapon.call_deferred()

func _check_enter_weapon():
	if is_instance_valid(get_weapon()):
		_enter_weapon()

func _enter_weapon():
	pass

func get_weapon() -> Weapon:
	if is_instance_valid(parent_wep):
		return parent_wep
	elif is_instance_valid(parent_wep_mod):
		return parent_wep_mod.get_weapon()
	return null

func is_active() -> bool:
	var wep = get_weapon()
	if is_instance_valid(wep):
		if !_wep_alt_checked:
			if wep_alt_check():
				_wep_alt_checked = true
			else:
				return false
		if wep is ModableWeapon:
			if !_is_alt && wep.carry_primary_mods:
				return true
			return _is_alt == wep.using_alt_mods
		return true
	return false

func wep_alt_check() -> bool:
	if parent_wep_mod == null && is_instance_valid(parent_wep):
		_is_alt = use_alt
		return true
	var wep = get_weapon()
	if is_instance_valid(wep):
		if wep is ModableWeapon:
			for mod in wep.mod_container_1.mods:
				if mod == parent_wep_mod:
					_is_alt = false
					return true
			for mod in wep.mod_container_2.mods:
				if mod == parent_wep_mod:
					_is_alt = true
					return true
		else:
			_is_alt = false
			return true
	return false

func set_level(_lvl: int):
	pass

func get_stat_string() -> Array[HitEffect.StatString]:
	return []
