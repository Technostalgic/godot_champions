extends WeaponEffect

@export var laser_color: Color = Color.BLUE
@export var target_texture: Texture2D = null
@export var max_projectiles_tracked: int = 2
@export var max_proj_track_increment: int = 1

var total_proj_tracked: int = 1
var target: Node2D = null
var laser_sight: LaserSight = null
var tracked_projectiles: Array[DynamicProjectile]
var _no_target: bool = true
var _last_target_pos: Vector2 = Vector2.ZERO
var _last_wep: Weapon = null
var _anim_delta: float = 0

func _enter_weapon():
	_on_added.call_deferred()

func _exit_tree():
	if is_instance_valid(_last_wep):
		if _last_wep.emit_projectiles.is_connected(on_wep_emit_proj):
			_last_wep.emit_projectiles.disconnect(on_wep_emit_proj)
		if _last_wep.begin_reload.is_connected(remove_target):
			_last_wep.begin_reload.disconnect(remove_target)
	_last_wep = null
	remove_target()

func _physics_process(delta: float):
	if !is_instance_valid(laser_sight):
		return
	var wep = get_weapon()
	if is_instance_valid(wep):
		if is_active():
			set_weapon_target()
			laser_sight.is_enabled = true
		else:
			laser_sight.is_enabled = false
		handle_projectile_tracking(delta)
		animate_target(delta)

func _on_added():
	if !is_instance_valid(laser_sight):
		create_laser()
	_last_wep = get_weapon()
	laser_sight.parent_wep_mod = parent_wep_mod
	laser_sight.parent_wep = parent_wep
	if is_instance_valid(parent_wep_mod):
		set_level(parent_wep_mod.get_stack_size())
	if is_instance_valid(_last_wep):
		if !_last_wep.emit_projectiles.is_connected(on_wep_emit_proj):
			_last_wep.emit_projectiles.connect(on_wep_emit_proj)
		if !_last_wep.begin_reload.is_connected(remove_target):
			_last_wep.begin_reload.connect(remove_target)

func prune_tracked_projectiles():
	var i = tracked_projectiles.size() - 1
	while i >= 0:
		if !is_instance_valid(tracked_projectiles[i]) || !tracked_projectiles[i].is_alive:
			tracked_projectiles.remove_at(i)
		i -= 1

func on_wep_emit_proj(projs: Array[Projectile]):
	prune_tracked_projectiles()
	for proj in projs:
		if tracked_projectiles.size() >= max_projectiles_tracked:
			break
		tracked_projectiles.push_back(proj)

func create_laser():
	laser_sight = LaserSight.new()
	laser_sight.parent_wep_mod = parent_wep_mod
	laser_sight.parent_wep = parent_wep
	laser_sight.laser_color = laser_color
	laser_sight.is_enabled = false
	add_child(laser_sight)

func create_target():
	target = Node2D.new()
	add_child(target)
	var sprite = Sprite2D.new()
	sprite.texture = target_texture
	sprite.modulate = laser_color
	sprite.modulate.a = 1
	target.add_child(sprite)
	target.hide()

func remove_target():
	if is_instance_valid(target):
		target.queue_free()
	target = null
	_no_target = true

func set_weapon_target():
	if !is_instance_valid(laser_sight):
		return
	if !is_instance_valid(target):
		create_target()
	_no_target = false
	laser_sight.update_laser_ray(get_weapon())
	var target_point = laser_sight.laser_ray.get_collision_point()
	var target_col = laser_sight.laser_ray.get_collider()
	if target_col is Node2D:
		if target.get_parent() != target_col:
			target.reparent(target_col)
	target.global_position = target_point

func handle_projectile_tracking(delta: float):
	if _no_target:
		return
	if !is_instance_valid(target) || !target.is_inside_tree():
		if is_instance_valid(target):
			remove_target()
		create_target()
		target.global_position = _last_target_pos
	_last_target_pos = target.global_position
	prune_tracked_projectiles()
	var damp_factor: float = 0.85
	var targ_pos: Vector2 = target.global_position
	for proj in tracked_projectiles:
		var lost_speed = proj.velocity.length()
		proj.velocity *= damp_factor
		lost_speed = lost_speed - proj.velocity.length()
		var dif = targ_pos - proj.global_position
		var rot_spd = PI * 4
		proj.velocity += (dif).normalized() * lost_speed
		proj.global_rotation = rotate_toward(proj.global_rotation, dif.angle(), rot_spd * delta)

func animate_target(delta: float):
	if _no_target:
		if is_instance_valid(target):
			target.hide()
		return
	target.show()
	_anim_delta += delta
	target.global_rotation = 0
	var min_scl = 0.35
	var dif_scl = 0.05
	var cur_delta = (sin(_anim_delta * TAU * 2) + 1) * 0.5
	target.scale = Vector2.ONE * (cur_delta * dif_scl + min_scl)
	target.modulate.a = cur_delta * 0.5 + 0.5

func set_level(lvl: int):
	total_proj_tracked = max_projectiles_tracked + (max_proj_track_increment * (lvl - 1))

func get_stat_string() -> Array[HitEffect.StatString]:
	return [
		HitEffect.StatString.create("Trackers", str(total_proj_tracked))
	]
