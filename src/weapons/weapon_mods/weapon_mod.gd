class_name WeaponMod
extends Item

@export var listen_for_update: bool
@export var listen_for_reload: bool

var level: float = 1
var mod_container: WeaponModContainer = null
var stat_mods: Array[StatModifier] = []
var hit_effects: Array[HitEffect] = []

func _enter_tree():
	# clear hit effects as they are regathered on enter_tree
	hit_effects.clear()

func _exit_tree():
	mod_container = null

func use(actor: Actor):
	if actor is Hero:
		var wep := actor.get_equipped_weapon() as Weapon
		if is_instance_valid(wep):
			if wep is ModableWeapon:
				var mods := wep.mod_container_1.mods.duplicate() as Array[WeaponMod]
				mods.push_back(self)
				wep.mod_container_1.set_mods(mods)

func get_weapon() -> Weapon:
	if is_instance_valid(mod_container):
		return mod_container.weapon
	return null

func modify_weapon_stats(stats: Stats.WeaponStats):
	for stat_mod in stat_mods:
		stats.modify_stat(stat_mod, level)

func modify_weapon_stats_set(stats: Stats.WeaponStats):
	for stat_mod in stat_mods:
		if stat_mod.mode != StatModifier.Mode.SET:
			continue
		stats.set_stat(stat_mod.stat, stat_mod.get_modifier_value(1))

func modify_weapon_stats_add(stats: Stats.WeaponStats):
	for stat_mod in stat_mods:
		if stat_mod.mode != StatModifier.Mode.ADD:
			continue
		stats.modify_stat_add(stat_mod, level)

func modify_weapon_stats_mult(stats: Stats.WeaponStats):
	for stat_mod in stat_mods:
		if stat_mod.mode != StatModifier.Mode.MULTIPLY:
			continue
		stats.modify_stat_mult(stat_mod, level)

func modify_shot_stats(stats: Stats.ShotStats):
	for stat_mod in stat_mods:
		match stat_mod.stat_type:
			Stats.StatType.PROJECTILE:
				stats.projectile_stats.modify_stat(stat_mod, level)
			Stats.StatType.SHOT:
				stats.modify_stat(stat_mod, level)

func modify_shot_stats_set(stats: Stats.ShotStats):
	for stat_mod in stat_mods:
		if stat_mod.mode != StatModifier.Mode.SET:
			continue
		match stat_mod.stat_type:
			Stats.StatType.PROJECTILE:
				stats.projectile_stats.set_stat(stat_mod.stat, stat_mod.get_modifier_value(1))
			Stats.StatType.SHOT:
				stats.set_stat(stat_mod.stat, stat_mod.get_modifier_value(1))

func modify_shot_stats_add(stats: Stats.ShotStats):
	for stat_mod in stat_mods:
		match stat_mod.stat_type:
			Stats.StatType.PROJECTILE:
				stats.projectile_stats.modify_stat_add(stat_mod, level)
			Stats.StatType.SHOT:
				stats.modify_stat_add(stat_mod, level)

func modify_shot_stats_mult(stats: Stats.ShotStats):
	for stat_mod in stat_mods:
		match stat_mod.stat_type:
			Stats.StatType.PROJECTILE:
				stats.projectile_stats.modify_stat_mult(stat_mod, level)
			Stats.StatType.SHOT:
				stats.modify_stat_mult(stat_mod, level)

func validate_compatibility(_wep_stats: Stats.WeaponStats, _shot_stats: Stats.ShotStats):
	pass

func get_is_compatible() -> bool:
	return true

func on_weapon_update(_delta: float):
	pass
	
func on_weapon_reload():
	pass

func try_combine_with(other: WeaponMod) -> bool:
	if other.title != title:
		return false
	level += other.get_stack_size()
	return true

func get_stack_size() -> int:
	return roundi(level)
