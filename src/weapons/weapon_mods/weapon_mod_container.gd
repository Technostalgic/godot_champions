class_name WeaponModContainer
extends Node

signal mods_changed()
signal on_weapon_update(delta: float)
signal on_weapon_reload()

var weapon: Weapon = null

var mods: Array[WeaponMod] = []

func _enter_tree():
	child_order_changed.connect(_refresh_mods)
	_refresh_mods()
	var parent = get_parent()
	if parent is Weapon:
		weapon = parent

func _exit_tree():
	child_order_changed.disconnect(_refresh_mods)

func _refresh_mods():
	mods.clear()
	for connection in on_weapon_update.get_connections():
		on_weapon_update.disconnect(connection.callable)
	for connection in on_weapon_reload.get_connections():
		on_weapon_reload.disconnect(connection.callable)
	for child in get_children():
		if child is WeaponMod:
			child.mod_container = self
			mods.push_back(child)
			if child.listen_for_update:
				on_weapon_update.connect(child.listen_for_update)
			if child.listen_for_reload:
				on_weapon_reload.connect(child.listen_for_reload)
	mods_changed.emit()

func mod_weapon_stats(stats: Stats.WeaponStats):
	var stats_add = Stats.WeaponStats.filled_with(0)
	var stats_mul = Stats.WeaponStats.filled_with(1)
	for mod in mods:
		mod.modify_weapon_stats_add(stats_add)
		mod.modify_weapon_stats_mult(stats_mul)
	stats.combine_mult(stats_mul)
	stats.combine_add(stats_add)
	for mod in mods: 
		mod.modify_weapon_stats_set(stats)

func mod_shot_stats(stats: Stats.ShotStats):
	var stats_add = Stats.ShotStats.filled_with(0)
	var stats_mul = Stats.ShotStats.filled_with(1)
	for mod in mods: 
		mod.modify_shot_stats_add(stats_add)
		mod.modify_shot_stats_mult(stats_mul)
	stats.combine_mult(stats_mul)
	stats.combine_add(stats_add)
	for mod in mods: 
		mod.modify_shot_stats_set(stats)

static func multi_mod_shot_stats(containers: Array[WeaponModContainer], stats: Stats.ShotStats):
	var stats_add = Stats.ShotStats.filled_with(0)
	var stats_mul = Stats.ShotStats.filled_with(1)
	for cont in containers:
		for mod in cont.mods:
			mod.modify_shot_stats_add(stats_add)
			mod.modify_shot_stats_mult(stats_mul)
	stats.combine_mult(stats_mul)
	stats.combine_add(stats_add)
	for cont in containers:
		for mod in cont.mods:
			mod.modify_shot_stats_set(stats)

func validate_mod_compatibility(wep_stats: Stats.WeaponStats, shot_stats: Stats.ShotStats):
	for mod in mods:
		mod.validate_compatibility(wep_stats, shot_stats)

func set_mods(new_mods: Array[WeaponMod]):
	var old_mods = mods.duplicate(false)
	mods.clear()
	for child in get_children():
		remove_child(child)
	for mod in new_mods:
		if mod.get_parent() == null:
			add_child(mod)
		else:
			mod.reparent(self, false)
		mods.push_back(mod)
	for mod in old_mods:
		if mod.get_parent() == null:
			mod.queue_free()
	_refresh_mods()
