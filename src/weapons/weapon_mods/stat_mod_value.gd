class_name StatModValue
extends Node

var require_set: bool = true

func _enter_tree():
	var parent := get_parent()
	if parent is StatModifier:
		if require_set:
			parent.mode = StatModifier.Mode.SET
