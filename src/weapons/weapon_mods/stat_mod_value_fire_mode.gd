class_name StatModValueProjectile
extends StatModValue

@export var fire_mode: Stats.FireMode

func _enter_tree():
	super._enter_tree()
	var parent := get_parent()
	if parent is StatModifier:
		parent.modifier_variant = fire_mode
