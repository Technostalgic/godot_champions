class_name StatModValueFireMode
extends StatModValue

@export var projectile_prefab: PackedScene

func _enter_tree():
	super._enter_tree()
	var parent := get_parent()
	if parent is StatModifier:
		parent.modifier_variant = projectile_prefab
