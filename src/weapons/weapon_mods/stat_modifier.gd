class_name StatModifier
extends Node

enum Mode { ADD, MULTIPLY, SET }

@export var stat_type: Stats.StatType = Stats.StatType.SHOT
@export var stat: Stats.Stat = Stats.Stat.POWER
@export var mode: Mode = Mode.ADD
@export var modifier: float = 0

var modifier_variant: Variant = null

func _enter_tree():
	var parent := get_parent()
	if parent is WeaponMod:
		parent.stat_mods.push_back(self)

func _exit_tree():
	var parent := get_parent()
	if parent is WeaponMod:
		parent.stat_mods.erase(self)

func get_modifier_value_float(multiplier: float) -> float:
	match mode:
		StatModifier.Mode.ADD:
			return modifier * multiplier
		StatModifier.Mode.MULTIPLY:
			if modifier - 1 < 0:
				if multiplier == 0:
					return 1
				return pow(modifier, multiplier)
			return (modifier - 1) * multiplier + 1
		StatModifier.Mode.SET:
			if modifier_variant != null:
				return 0
			return modifier
	return 0

func get_modifier_value(mutliplier: float) -> Variant:
	if modifier_variant != null:
		return modifier_variant
	return get_modifier_value_float(mutliplier)

func get_modifier_value_str(mult: float) -> String:
	match mode:
		Mode.ADD:
			var sign_str := "+"
			if modifier < 0:
				sign_str = "-"
			return str(sign_str, String.num(absf(get_modifier_value_float(mult)), 2))
		Mode.MULTIPLY:
			var sign_str := "+"
			if modifier < 1:
				sign_str = "-"
			return str(sign_str, String.num(absf((get_modifier_value_float(mult) - 1) * 100), 2), "%")
		Mode.SET:
			var modifier_str = ""
			if modifier_variant != null:
				modifier_str = "?" #TODO get proper modifier_variant string
			else:
				modifier_str = String.num(get_modifier_value_float(mult), 2)
			return str("SET=", modifier_str)
	return "??"
