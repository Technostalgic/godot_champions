extends Node

@export var target:RigidBody2D = null
@export var graphic: Node2D = null
@export var invert: bool = false

func _process(_delta: float) -> void:
	var right := target.global_transform.basis_xform(Vector2.RIGHT)
	var dot := target.linear_velocity.dot(right)
	var inv := -1.0 if invert else 1.0
	if dot < 0:
		graphic.scale.x = -absf(target.scale.x) * inv
	elif dot > 0:
		graphic.scale.x = absf(target.scale.x) * inv
