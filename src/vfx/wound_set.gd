class_name WoundSet
extends Resource

@export var type: Wound.Type
@export var wounds: Array[PackedScene]
