class_name ParticleEffectTimer
extends Node

@export var effect: ParticleEffect = null
@export var start_time: float = 0
@export var stop_time: float = 1
@export var reset_on_enter: bool = true

var elapsed_time: float = 0
var running: float = true

func _enter_tree() -> void:
	if reset_on_enter:
		elapsed_time = 0
		running = true

func _process(delta: float) -> void:
	if !running:
		return
	
	elapsed_time += delta
	if elapsed_time >= start_time:
		effect.is_emitting = true
		if elapsed_time >= stop_time:
			effect.is_emitting = false
	else:
		effect.is_emitting = false
