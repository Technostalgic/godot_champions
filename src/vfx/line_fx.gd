extends Node

const PRE_POOL_COUNT: int = 10

var line_tex: Texture2D :
	get:
		if _line_tex == null:
			_line_tex = _create_line_tex()
		return _line_tex

var _pool: Array[Sprite2D] = []
var _to_pool: Array[Sprite2D] = []
var _to_pool_cursor: int = -1
var _line_tex: Texture2D = null

func _ready():
	var i = PRE_POOL_COUNT
	while i > 0:
		_pool.push_back(_create_new_line())
		i -= 1

func _process(_delta: float):
	while _to_pool_cursor >= 0:
		pool_line(_to_pool[_to_pool_cursor])
		_to_pool.remove_at(_to_pool_cursor)
		_to_pool_cursor -= 1
	_to_pool_cursor = _to_pool.size() - 1

func _create_new_line() -> Sprite2D:
	var line = Sprite2D.new()
	line.physics_interpolation_mode = Node.PHYSICS_INTERPOLATION_MODE_OFF
	line.texture = line_tex
	line.centered = false
	line.offset.y = -0.5
	return line

func _create_line_tex() -> Texture2D:
	var tex = GradientTexture1D.new()
	tex.width = 1
	tex.gradient = Gradient.new()
	while tex.gradient.get_point_count() > 1:
		tex.gradient.remove_point(1)
	tex.gradient.set_color(0, Color.WHITE)
	return tex

func _pool_line(line: Sprite2D):
	var parent = line.get_parent()
	if is_instance_valid(parent):
		parent.remove_child(line)
	_pool.push_back(line)

func pick_line() -> Sprite2D:
	var line := (_create_new_line() if _pool.size() <= 0 else _pool.pop_back()) as Sprite2D
	Global.root_2d_world.add_child(line)
	line.scale = Vector2.ONE
	line.modulate = Color.WHITE
	line.show()
	return line
	
func pool_line(line: Sprite2D):
	_pool_line.call_deferred(line)

func pool_line_next_frame(line: Sprite2D):
	_to_pool.push_back(line)

func stretch_line(line: Sprite2D, from: Vector2, to: Vector2):
	var dif = to - from
	line.global_position = from
	line.global_rotation = dif.angle()
	line.global_scale.x = dif.length()
