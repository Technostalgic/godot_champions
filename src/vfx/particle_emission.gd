@tool
class_name ParticleEmission
extends ParticleVfxBase

@export var emit_rate: float = 10
@export var speed_min: float = 10
@export var speed_max: float = 25
@export_range(0, TAU) var spread: float = 0

func handle_emission_from(sys: GPUParticles2D, trans: Transform2D, delta: float):
	var chance := emit_rate * delta
	while chance >= randf():
		chance -= 1
		emit_at(sys, trans)

func handle_emission(trans: Transform2D, delta: float):
	handle_emission_from(get_particle_sys(), trans, delta)

func emit_at(sys: GPUParticles2D, trans: Transform2D, velocity: Vector2 = Vector2.ZERO):
	trans = Transform2D(trans)
	#trans = trans * sys.global_transform
	var base_pos = trans.get_origin()
	var base_angle = trans.get_rotation() - sys.global_rotation
	var vel: Vector2 = Vector2.from_angle(base_angle + randf_range(-spread, spread))
	vel *= randf_range(speed_min, speed_max)
	trans.origin = Vector2.ZERO
	trans.origin = base_pos + trans * emission_shape.get_random_offset()
	sys.emit_particle(trans, vel + velocity, color, color, get_emit_flags())
	trans.origin = base_pos
