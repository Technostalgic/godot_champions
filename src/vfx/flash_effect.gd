class_name FlashEffect
extends Node

@export var color: Color = Color.WHITE
@export var color_multiplier: float = 1000
@export var target_nodes: Array[Node2D] = []

var _flash_timer: float = 0
var _module_orig: Color = Color.WHITE

func _process(delta: float) -> void:
	if _flash_timer > 0:
		_flash_timer -= delta
		if _flash_timer <= 0:
			for node in target_nodes:
				node.modulate = _module_orig

func flash(time: float) -> void:
	if _flash_timer <= 0 && target_nodes.size() > 0:
		_module_orig = target_nodes[0].modulate
	for node in target_nodes:
		node.modulate = color * color_multiplier
	_flash_timer = time

func is_flashing() -> bool:
	return _flash_timer > 0
