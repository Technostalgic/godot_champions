@tool
class_name ParticleBurst
extends ParticleVfxBase

@export var count_min: int = 5
@export var count_max: int = 10
@export var speed_min: float = 10
@export var speed_max: float = 25
@export_range(0, TAU) var spread: float = TAU

func get_count() -> int:
	return randi_range(count_min, count_max)

func burst_from(sys: GPUParticles2D, trans: Transform2D, velocity: Vector2 = Vector2.ZERO):
	var flags = get_emit_flags()
	var base_pos = trans.get_origin()
	var base_angle = trans.get_rotation() - sys.global_rotation
	var count = get_count()
	
	while count > 0:
		var vel: Vector2 = Vector2.from_angle(base_angle + randf_range(-spread, spread))
		vel *= randf_range(speed_min, speed_max)
		trans.origin = Vector2.ZERO
		trans.origin = base_pos + trans * emission_shape.get_random_offset()
		sys.emit_particle(trans, vel + velocity, color, color, flags)
		count -= 1
	trans.origin = base_pos

func burst(trans: Transform2D, velocity: Vector2 = Vector2.ZERO):
	burst_from(get_particle_sys(), trans, velocity)
