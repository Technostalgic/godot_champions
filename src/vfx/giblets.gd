extends Node

@export var flesh_giblet_scenes: Array[PackedScene] = []
@export var stone_giblet_scenes: Array[PackedScene] = []
@export var metal_giblet_scenes: Array[PackedScene] = []
@export var prepool_count: int = 10

## Dictionary<Wound.Type, Array[Giblet]>
var _pool_gibs_map: Dictionary = {}

func _ready() -> void:
	pass

func _prepool_gibs() -> void:
	for i in prepool_count:
		var flesh_gib = create_gib(Wound.Type.FLESH)
		if flesh_gib != null:
			pool_gib(flesh_gib)
		else:
			push_error("no flesh gib scenes available")
		var stone_gib = create_gib(Wound.Type.STONE)
		if stone_gib != null:
			pool_gib(stone_gib)
		else:
			push_error("no stone gib scenes available")
		var metal_gib = create_gib(Wound.Type.METAL)
		if metal_gib != null:
			pool_gib(metal_gib)
		else:
			push_error("no metal gib scenes available")

func pick_gib(type: Wound.Type) -> Giblet:
	if !_pool_gibs_map.has(type):
		_pool_gibs_map[type] = []
	var pool = _pool_gibs_map[type] as Array[Giblet]
	var gib: Giblet = null
	if pool.is_empty():
		gib = create_gib(type)
	else:
		gib = pool.pop_back()
	return gib

func pool_gib(gib: Giblet) -> void:
	if !_pool_gibs_map.has(gib.gib_type):
		_pool_gibs_map[gib.gib_type] = []
	var pool = _pool_gibs_map[gib.gib_type] as Array[Giblet]
	pool.push_back(gib)
	var parent = gib.get_parent()
	if parent != null:
		parent.remove_child(gib)

func create_gib(type: Wound.Type) -> Giblet:
	var gib: Giblet = null
	match type:
		Wound.Type.FLESH:
			if !flesh_giblet_scenes.is_empty():
				gib = flesh_giblet_scenes.pick_random().instantiate()
		Wound.Type.STONE:
			if !stone_giblet_scenes.is_empty():
				gib = stone_giblet_scenes.pick_random().instantiate()
		Wound.Type.METAL:
			if !metal_giblet_scenes.is_empty():
				gib = metal_giblet_scenes.pick_random().instantiate()
	return gib
