class_name ContainerWound
extends Wound

@export var container: Node2D = null

func _ready() -> void:
	if container == null:
		container = self

func _on_exit():
	print(self)

func on_removed():
	if wound_owner != null:
		release_contained_objects()
	super.on_removed()

func release_contained_objects():
	var wound_owner_parent := wound_owner.get_parent()
	if is_instance_valid(wound_owner_parent):
		for contained in container.get_children():
			if contained is Node2D:
				var targ_pos = contained.global_position
				var targ_rot_vect = (
					Vector2.from_angle(contained.global_rotation) * 
					contained.global_scale 
				)
				contained.reparent(wound_owner_parent, false)
				contained.global_position = targ_pos
				contained.global_rotation = targ_rot_vect.angle()
			else:
				contained.reparent(wound_owner_parent, true)

func add_object_to_container(node: Node2D):
	if node is DynamicProjectile:
		node.velocity.x = 0
		node.velocity.y = 0
	if is_instance_valid(node.get_parent()):
		node.reparent(container, false)
	else:
		container.add_child(node)
	node.rotation = 0
	node.position.x = 0
	node.position.y = 0
	node.reset_physics_interpolation()
	
