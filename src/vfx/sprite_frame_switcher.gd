extends Sprite2D

var total_frames: int = -1 :
	get:
		if total_frames < 0:
			total_frames = hframes * vframes
		return total_frames

func _increment_frame_any(_unused: Variant) -> void:
	increment_frame()

func increment_frame() -> void:
	var next_frame := frame + 1
	if next_frame >= total_frames:
		next_frame = 0
	frame = next_frame
