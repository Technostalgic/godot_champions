@tool
class_name ParticleVfxBase
extends Resource

## Must be of type PersistentParticleSystem
@export var particle_system: PackedScene

@export var emission_shape: ParticleEmitShape = null
@export var use_color: bool = false
@export var color: Color = Color.WHITE

var _part_sys: PersistentParticleSystem = null
var _emit_flags: int = 0

func get_particle_sys() -> PersistentParticleSystem:
	if !is_instance_valid(_part_sys):
		_part_sys = ParticleSystem.get_particle_system(particle_system)
	return _part_sys

func get_emit_flags() -> int:
	if _emit_flags == 0:
		_emit_flags = (
			GPUParticles2D.EMIT_FLAG_POSITION | 
			GPUParticles2D.EMIT_FLAG_ROTATION_SCALE |
			GPUParticles2D.EMIT_FLAG_VELOCITY
		)
		if use_color:
			_emit_flags |= (
				GPUParticles2D.EMIT_FLAG_COLOR |
				GPUParticles2D.EMIT_FLAG_CUSTOM
			)
	return _emit_flags
