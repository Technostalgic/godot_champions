@tool
class_name PersistentParticleSystem
extends GPUParticles2D

@export var test_effect: ParticleVfxBase = null
@export var test_effect_run: bool : 
	set(val): _run_test_effect(val)
	get: return _test_effect_run

var _test_effect_cached: ParticleVfxBase = null
var _test_effect_run: bool = false

func _process(delta):
	if test_effect_run:
		if Engine.is_editor_hint():
			_handle_test_effect(delta)

func _run_test_effect(val: bool):
	_test_effect_run = val
	if val && is_instance_valid(test_effect):
		if test_effect is ParticleBurst:
			test_effect._part_sys = self
			test_effect.burst(global_transform, Vector2.ZERO)

func _handle_test_effect(delta: float):
	if is_instance_valid(test_effect):
		if test_effect is ParticleEmission:
			test_effect._part_sys = self
			test_effect.handle_emission(global_transform, delta)

func _notification(what):
	if what == NOTIFICATION_EDITOR_PRE_SAVE:
		_test_effect_cached = test_effect
		test_effect = null
	elif what == NOTIFICATION_EDITOR_POST_SAVE:
		test_effect = _test_effect_cached
