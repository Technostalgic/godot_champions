extends Node

@export var position_target: Node2D = null
@export var forced_interpolation: bool = true
@export var thickness: float = 1
@export var color: Color = Color.YELLOW

var _line_gfx: Sprite2D = null
var _last_pos: Vector2 = Vector2.ZERO
var _cur_pos: Vector2 = Vector2.ZERO
var _last_render_pos: Vector2 = Vector2.ZERO

func _enter_tree() -> void:
	reset.call_deferred()

func _exit_tree() -> void:
	LineFx.pool_line(_line_gfx)

func _physics_process(_delta: float) -> void:
	_last_pos = _cur_pos
	_cur_pos = position_target.global_position

func _process(_delta: float) -> void:
	var target_pos: Vector2
	if forced_interpolation:
		var dif = _cur_pos - _last_pos
		target_pos = _last_pos + dif * Engine.get_physics_interpolation_fraction()
	else:
		target_pos = position_target.position
	orient_line(_last_render_pos, target_pos)
	_last_render_pos = target_pos

func reset() -> void:
	_last_pos = position_target.global_position
	_cur_pos = _last_pos
	_last_render_pos = _last_pos
	_line_gfx = LineFx.pick_line()
	_line_gfx.self_modulate = color
	orient_line(_last_pos, _cur_pos)

func orient_line(start: Vector2, end: Vector2):
	var dif = end - start
	_line_gfx.global_position = start
	_line_gfx.rotation = dif.angle()
	_line_gfx.scale.x = dif.length()
	_line_gfx.scale.y = thickness
