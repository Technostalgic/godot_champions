class_name GroundActorGfxController
extends Node

@export var graphic: AnimatedSprite2D = null
@export var graphic_container: Node2D = null
@export var actor: Actor = null
@export var weapon_slot: WeaponSlot = null
@export var ground_detection: GroundDetection = null
@export var ground_movement: GroundMovement = null
@export var ground_jumping: GroundJumping = null

@export var jump_stretch_delta_max: float = 0.25
@export var jump_stretch_factor: float = 1.3
@export var jump_squash_delta_max: float = 0.2
@export var jump_squash_factor: float = 0.75

@export var run_stretch_factor: float = 1.1
@export var run_cycle_length: float = 0.333333
@export var run_cycle_offset: float = 0.5

var _jump_stretch_delta: float = 0
var _jump_squash_delta: float = 0
var _run_cycle_delta: float = 0
var _was_on_ground: bool = false

static func arc_slerp(from: float, to: float, delta: float) -> float:
	var dif := to - from
	delta = delta - 1
	delta = clampf(-delta * delta + 1, 0, 2)
	dif *= delta
	return from + dif

func _ready():
	if actor is Hero:
		graphic.self_modulate = actor.color
		graphic.self_modulate.a = 1
	ground_jumping.on_jump_begin.connect(_on_jump_begin)

func _process(delta: float):
	if is_instance_valid(get_weapon()):
		handle_flip()
	reset_container_transform()
	detect_landing()
	handle_jump_land_trans_anim(delta)
	handle_run_trans_anim(delta)
	handle_animation()

func _on_jump_begin():
	_jump_stretch_delta = jump_stretch_delta_max
	_jump_squash_delta = 0

func _on_landing():
	_jump_squash_delta = jump_squash_delta_max
	_jump_stretch_delta = 0

func reset_container_transform():
	graphic_container.position = Vector2.ZERO
	graphic_container.scale = Vector2.ONE
	graphic_container.rotation = 0

func detect_landing():
	if !actor.is_alive():
		return
	var is_on_ground := ground_detection.is_on_ground()
	if !_was_on_ground && is_on_ground:
		_on_landing()
	_was_on_ground = is_on_ground

func handle_jump_land_trans_anim(delta: float):
	if !actor.is_alive():
		return
	if _jump_squash_delta > 0:
		_jump_squash_delta -= delta
		var factor = arc_slerp(
			1, jump_squash_factor, 
			_jump_squash_delta / jump_squash_delta_max * 2
		)
		graphic_container.scale.y *= factor
		graphic_container.scale.x *= 1 / factor
		var off_y: float = (1 - factor) * 10
		graphic_container.position.y += off_y
	elif _jump_stretch_delta > 0:
		_jump_stretch_delta -= delta
		var factor = arc_slerp(
			1, jump_stretch_factor, 
			_jump_stretch_delta / jump_stretch_delta_max * 2
		)
		graphic_container.scale.y *= factor
		graphic_container.scale.x *= 1 / factor

func handle_run_trans_anim(delta: float):
	if !actor.is_alive():
		return
	if !ground_detection.is_on_ground() || absf(ground_movement.move_dir) < 0.5 :
		_run_cycle_delta = run_cycle_offset * run_cycle_length
		return
	_run_cycle_delta = wrapf(_run_cycle_delta + delta, 0, run_cycle_length)
	var half_cycle := run_cycle_length * 0.5
	var cycle_delta := _run_cycle_delta
	var cycle_factor := run_stretch_factor
	if _run_cycle_delta >= half_cycle:
		cycle_delta = _run_cycle_delta - half_cycle
		cycle_factor = 1 / run_stretch_factor
	var factor: float = arc_slerp(1, cycle_factor, cycle_delta / half_cycle * 2)
	graphic_container.scale.y *= factor
	graphic_container.scale.x *= 1 / factor

func get_weapon() -> Weapon:
	if !is_instance_valid(weapon_slot):
		return null
	var eq_wep = weapon_slot.get_equipped_weapons()
	if eq_wep.size() > 0:
		return eq_wep[0]
	return null

func handle_animation():
	if !actor.is_alive():
		return
	if !ground_detection.is_on_ground():
		graphic.play("jump")
		return
	if absf(ground_movement.move_dir) > 0.5 && ground_detection.is_on_ground():
		graphic.play("run")
	else:
		graphic.play("default")

func handle_flip():
	if !actor.is_alive():
		return
	var wep = get_weapon()
	if is_instance_valid(wep.graphic):
		if wep.graphic.scale.y < 0:
			graphic.flip_h = true
		elif wep.graphic.scale.y > 0:
			graphic.flip_h = false
	else:
		if absf(angle_difference(graphic.global_rotation, wep.global_rotation)) > PI * 0.5:
			graphic.flip_h = true
		else:
			graphic.flip_h = false
