extends ColorRect

const WORLD_OFFSET_PROP: StringName = &"world_offset"
const VISIBLE_RANGE_PROP: StringName = &"visible_range"

@export var base_visible_range: float = 32
var blah: float = 0

func _ready():
	material = material.duplicate()

func _process(delta: float) -> void:
	var off = Vector2.ZERO
	var vis_range_mult := 1.0
	
	var cam := Global.camera as Camera2D
	if cam != null:
		var res_factor: float = 1
		off.x = fmod(cam.global_position.x, res_factor)
		if off.x < 0:
			off.x += res_factor
		off.y = fmod(cam.global_position.y, res_factor)
		if off.y < 0:
			off.y += res_factor
		off *= cam.zoom
		vis_range_mult = maxf(cam.zoom.x, cam.zoom.y)
	
	var mat := material as ShaderMaterial
	mat.set_shader_parameter(VISIBLE_RANGE_PROP, base_visible_range * vis_range_mult)
	
	# (material as ShaderMaterial).set_shader_parameter(WORLD_OFFSET_PROP, off)
