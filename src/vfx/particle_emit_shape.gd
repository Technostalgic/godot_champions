@tool
class_name ParticleEmitShape
extends Resource

@export var offset: Vector2 = Vector2.ZERO
@export var radius: float = 0

func get_random_offset() -> Vector2:
	return offset + Vector2.from_angle(randf_range(0, TAU)) * radius * (1 - randf() * randf())
