extends Node

@export var target: Node2D = null
@export var factor: float = 0.25
@export var cycle_time: float = 0.25

var _time: float = 0

func _process(delta: float) -> void:
	_time += delta
	var t_delta: float = absf(fmod(_time, cycle_time * 2) - cycle_time) / cycle_time
	t_delta = smoothstep(0, 1, t_delta)
	var t_factor = lerp(1 - factor, 1 + factor, t_delta)
	target.scale.x = t_factor
	target.scale.y = 1 / t_factor
