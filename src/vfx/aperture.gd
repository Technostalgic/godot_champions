extends Node

@export var segment_container: Node2D
@export var mask: Node2D
@export var anim_length: float = 0.5
@export var max_rotation: float = PI * 0.5
@export_range(0,1) var open: float = 0 :
	set(value):
		open = value
		apply_segment_transform()

var _segments: Array[Node2D] = []
var _orig_rots: Array[float] = []
var _anim_dir: float = 0

var segments: Array[Node2D] :
	get:
		if _segments.size() <= 0:
			gather_segments()
		return _segments

func gather_segments():
	_segments.clear()
	_orig_rots.clear()
	for child in segment_container.get_children():
		if child is Node2D:
			_segments.push_back(child)
			_orig_rots.push_back(child.rotation)

func apply_segment_transform():
	var x := open - 1
	var delta := -(x * x) + 1
	var i := 0
	for seg in segments:
		seg.rotation = _orig_rots[i] + delta * max_rotation
		i += 1
	if delta > 0.15:
		mask.clip_children = CanvasItem.CLIP_CHILDREN_ONLY
		mask.self_modulate.a = 1
	else:
		mask.clip_children = CanvasItem.CLIP_CHILDREN_DISABLED
		mask.self_modulate.a = 0

func _process(delta: float) -> void:
	if _anim_dir > 0 && open < 1:
		open += 1 / anim_length * delta
		if open > 1:
			open = 1
			_anim_dir = 0
	if _anim_dir < 0 && open > 0:
		open -= 1 / anim_length * delta
		if open < 0:
			open = 0
			_anim_dir = 0

func open_door():
	_anim_dir = 1

func open_door_any(_unused: Actor):
	open_door()

func close_door():
	_anim_dir = -1

func close_door_any(_unused: Actor):
	close_door()
