class_name DeathEffect
extends Node

@export var actor: Actor = null
@export var particle_burst: ParticleBurst = null
@export var gib_count: int = 0
@export var gib_burst_radius: float = 15
@export var gib_burst_speed: float = 100
@export var corpse_scene: PackedScene = null
@export var remove_on_death: bool = true

func _ready() -> void:
	actor.on_kill.connect(do_death_effect)

func burst_gibs(count: int) -> void:
	var random_spread := 1.0
	var ang_speed := 10
	for i: int in count:
		var gib = Giblets.pick_gib(actor.wound_type)
		var ang = (i + randf() * random_spread) / count * TAU
		var off = Vector2.from_angle(ang)
		var speed = randf() * gib_burst_speed
		Global.root_2d_world.add_child(gib)
		gib.global_position = actor.global_position + off * gib_burst_radius * randf()
		gib.reset_physics_interpolation()
		gib.linear_velocity = actor.linear_velocity + off * speed
		gib.angular_velocity = randf_range(-ang_speed, ang_speed)

func do_death_effect() -> void:
	burst_gibs(gib_count)
	if particle_burst != null:
		particle_burst.burst(actor.global_transform, actor.linear_velocity)
	if corpse_scene != null:
		var corpse := CorpseManager.pick_corpse(corpse_scene) as Corpse
		corpse.spawn_from(actor)
	if remove_on_death:
		actor.remove()
