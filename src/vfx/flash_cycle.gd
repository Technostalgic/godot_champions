extends Node

@export var flash_effects: Array[FlashEffect] = []
@export var cycle_time: float = 0.2

var _timer: float = 0

func _process(delta: float) -> void:
	_timer += delta
	_timer = wrapf(_timer, 0, cycle_time)
	var index := floorf(_timer / cycle_time * flash_effects.size()) as int
	flash_effects[index].flash(0.05)
