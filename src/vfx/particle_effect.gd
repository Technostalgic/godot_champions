class_name ParticleEffect
extends Node2D

@export var local_particle_system: GPUParticles2D = null
@export var particle_emission: ParticleEmission = null
@export var particle_burst: ParticleBurst = null
@export var is_emitting: bool = true

func _process(delta: float):
	if is_emitting && particle_emission != null:
		if !is_instance_valid(local_particle_system):
			particle_emission.handle_emission(global_transform, delta)
		else:
			particle_emission.handle_emission_from(local_particle_system, global_transform, delta)

func burst():
	if particle_burst != null:
		if !is_instance_valid(local_particle_system):
			particle_burst.burst(global_transform)
		else:
			particle_burst.burst_from(local_particle_system, global_transform)

# Just to help with attaching signals that have a parameter, even if param is unused
func burst_any(_unused):
	burst()
