@tool
class_name ParticleEmitRect
extends ParticleEmitShape

@export var extents: Vector2 = Vector2.ZERO

func get_random_offset() -> Vector2:
	var off := Vector2(randf_range(-extents.x, extents.x), randf_range(-extents.y, extents.y))
	return off + super.get_random_offset()
