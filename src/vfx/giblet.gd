class_name Giblet
extends DynamicGravityBody

@export var gib_type: Wound.Type = Wound.Type.FLESH

var max_lifetime: float = 4.5
var lifetime: float = 0

func _enter_tree() -> void:
	rotation = randf() * TAU
	_reset()

func _process(delta: float) -> void:
	lifetime += delta
	var fade_time := 0.5
	var fade_start := max_lifetime - fade_time
	if lifetime > max_lifetime - fade_time:
		if lifetime > max_lifetime:
			remove()
			return
		modulate.a = 1 - (lifetime - fade_start) / fade_time

func _reset() -> void:
	modulate.a = 1
	lifetime = 0

func remove() -> void:
	Giblets.pool_gib(self)
