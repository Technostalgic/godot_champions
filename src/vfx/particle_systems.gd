class_name ParticleSystems
extends Node

var particle_sys_map: Dictionary = {}

func get_particle_system(particle_sys: PackedScene) -> PersistentParticleSystem:
	var id := particle_sys.get_instance_id()
	var sys = particle_sys_map.get(id)
	if sys == null:
		sys = particle_sys.instantiate()
		if sys is PersistentParticleSystem:
			particle_sys_map[id] = sys
			sys.emitting = false
			Global.root_2d_world.add_child(sys)
			sys.global_position = Vector2.ZERO
		else:
			push_error("specified scene is NOT a persistent particle system node")
			sys.queue_free()
	return sys

func remove_all():
	for key in particle_sys_map.keys():
		var sys = particle_sys_map.get(key)
		if sys is Node:
			sys.queue_free()
	particle_sys_map.clear()
