extends Node

@export var default_wounds: Array[WoundSet] = []
@export var basic_container_prefab: PackedScene = null

## Dictionary<Wound.Type, Array[PackedScene]>
var default_wound_map: Dictionary = {} :
	get:
		if default_wound_map.is_empty():
			default_wound_map = create_map_from_sets(default_wounds)
		return default_wound_map

## Dictionary<InstanceID, Array[Wound]>
var wound_map: Dictionary = {}

## Map structure must be Dictionary<Wound.Type, Array[PackedScene(Wound)]>
func pick_wound_from_map(map: Dictionary, type: Wound.Type) -> Wound:
	if !map.has(type):
		return null
	var arr := map[type] as Array[PackedScene]
	if arr.size() <= 0:
		return null
	return pick_wound(arr.pick_random())

func create_map_from_sets(sets: Array[WoundSet]) -> Dictionary:
	var map: Dictionary = {}
	for wound_set in sets:
		if !map.has(wound_set.type):
			map[wound_set.type] = [] as Array[PackedScene]
		var arr := map[wound_set.type] as Array[PackedScene]
		for wound_scene in wound_set.wounds:
			if !(wound_scene in arr):
				arr.push_back(wound_scene)
	return map

func pick_basic_container() -> ContainerWound:
	return pick_wound(basic_container_prefab) as ContainerWound

func pick_wound(prefab: PackedScene) -> Wound:
	var id := prefab.get_instance_id()
	var wound: Wound = null
	if wound_map.has(id):
		wound = (wound_map[id] as Array[Wound]).pop_back()
	if wound == null:
		wound = prefab.instantiate() as Wound
		wound.instance_id = prefab.get_instance_id()
	return wound

func pool_wound(wound: Wound):
	if wound.instance_id == 0:
		push_error("Wound does not appear to be from a pool")
		wound.queue_free()
		return
	var wound_parent := wound.get_parent()
	if wound_parent != null:
		wound.on_removed()
		wound_parent.remove_child(wound)
	if !wound_map.has(wound.instance_id):
		wound_map[wound.instance_id] = [] as Array[Wound]
	(wound_map[wound.instance_id] as Array[Wound]).push_back(wound)
