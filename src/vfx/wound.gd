class_name Wound
extends Node2D

enum Type {
	FLESH,
	STONE,
	METAL
}

signal added(actor: ProjectileTarget, proj: Projectile, point: Vector2, normal: Vector2)
signal removed()

@export var type: Wound.Type = Wound.Type.FLESH
@export var orient_to_normal: bool = true
@export var min_penetration: float = 0
@export var max_penetration: float = 8
@export var random_flip: bool = true
@export var random_angle: float = PI * 0.125

var instance_id: int = 0
var wound_owner: ProjectileTarget = null
var proj_owner: Projectile = null

func on_added(actor: ProjectileTarget, proj: Projectile, point: Vector2, normal: Vector2) -> void:
	wound_owner = actor
	proj_owner = proj
	orient_wound(point, normal)
	reset_physics_interpolation()
	added.emit(actor, proj, point, normal)

func on_removed():
	removed.emit()
	wound_owner = null

func orient_wound(point: Vector2, normal: Vector2) -> void:
	
	scale = Vector2.ONE
	if random_flip && randf() >= 0.5:
		scale.y = 1 if randf() >= 0.5 else -1
	
	var penetration := randf_range(max_penetration, min_penetration)
	global_position = point + normal * -penetration
	if orient_to_normal:
		global_rotation = normal.angle() + randf_range(-random_angle, random_angle)
