class_name SquadNode
extends Node2D

static func create_single_mob_squad(mob_info: MobInfo, spawn_delay: float = 1) -> SquadNode:
	var squad = SquadNode.new()
	var spawn = MobSpawner.new()
	spawn.picked_mob_info = mob_info
	spawn.spawn_time = spawn_delay
	squad.add_child(spawn)
	return squad

@export var orient_to_gravity: bool = true
@export var random_flip_h: bool = true

var squad_instance: WaveSquad.Instance = null

var flags_incl: MobFlags :
	get: return squad_instance.flags_incl
var flags_excl: MobFlags :
	get: return squad_instance.flags_excl

var spawners: Array[MobSpawner] = [] :
	get:
		if spawners.size() <= 0:
			spawners = _find_mob_spawners()
		return spawners

func _enter_tree() -> void:
	_orient_to_gravity.call_deferred()
	if random_flip_h:
		if randf() >= 0.5:
			scale.x *= -1

func _ready() -> void:
	child_exiting_tree.connect(_remove_check_deferred)

func _remove_check_deferred(_unused: Node) -> void:
	_remove_check.call_deferred()

func _remove_check() -> void:
	for child in get_children():
		if child is MobSpawner:
			return
	queue_free()

func _orient_to_gravity() -> void:
	if !orient_to_gravity:
		rotation = randf_range(-PI, PI)
		return
	var grav = GravityField.get_gravity_at_pos(global_position)
	if grav.length_squared() <= 0:
		rotation = randf_range(-PI, PI)
		return
	global_rotation = grav.angle() - PI * 0.5

func _find_mob_spawners() -> Array[MobSpawner]:
	var arr: Array[MobSpawner] = []
	for child in get_children():
		if child is MobSpawner:
			arr.push_back(child)
	return arr

func calculate_squad_cost() -> float:
	var total: float = 0
	for spawner in spawners:
		if spawner.picked_mob_info != null:
			total += spawner.get_total_cost()
	return total
