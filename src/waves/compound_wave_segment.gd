class_name CompoundWaveSegment
extends Resource

enum Order {
	SEQUENTIAL,
	SIMULTANEOUS
}

static func create(from_wave: Wave, order_type: Order) -> CompoundWaveSegment:
	var segment = CompoundWaveSegment.new()
	segment.wave = from_wave
	segment.order = order_type
	return segment

@export var wave: Wave = null
@export var order: Order = Order.SEQUENTIAL
