class_name MobSpawner
extends Node2D

## Additional mobs of the same type will spawn at the sub spawn positions
@export var sub_spawn_positions: Array[Node2D] = []
@export var spawn_cost_max: float = 1
@export var mob_pool_forced: Array[MobInfo] = []
@export var flags_include: MobFlags = null
@export var flags_exclude: MobFlags = null
@export var spawn_time: float = 2

var spawn_fx: Array[Node2D] = []
var timer: float = 0
var parent_squad: SquadNode :
	get:
		if parent_squad == null:
			parent_squad = get_parent() as SquadNode
		return parent_squad

var picked_mob_info: MobInfo = null :
	get:
		if picked_mob_info == null: 
			picked_mob_info = pick_mob_info(WaveState.wave_number)
		return picked_mob_info
	set(val):
		picked_mob_info = val

func _enter_tree() -> void:
	if is_instance_valid(parent_squad):
		if parent_squad.squad_instance.force_mob_pool:
			mob_pool_forced = parent_squad.mob_pool_forced
		if parent_squad.flags_incl != null:
			if flags_include != null:
				flags_include = MobFlags.combine_add(flags_include, parent_squad.flags_incl)
			else: flags_include = parent_squad.flags_incl
		if parent_squad.flags_excl != null:
			if flags_exclude != null:
				flags_exclude = MobFlags.combine_add(flags_exclude, parent_squad.flags_excl)
			else: flags_exclude = parent_squad.flags_excl

func _ready():
	if picked_mob_info == null:
		queue_free()
		return
	_create_fx()

func _process(delta: float) -> void:
	timer += delta
	handle_fx()
	if timer >= spawn_time:
		complete_spawn()

func _create_fx() -> void:
	var i := sub_spawn_positions.size() - 1
	while i >= -1:
		var spawn_node: Node2D = sub_spawn_positions[i] if i >= 0 else self
		var spawn_effect := MobManager.mob_spawn_fx.instantiate() as Node2D
		spawn_node.add_child(spawn_effect)
		spawn_effect.position = Vector2.ZERO
		spawn_effect.scale = Vector2.ZERO
		spawn_effect.reset_physics_interpolation()
		spawn_fx.push_back(spawn_effect)
		i -= 1

func handle_fx() -> void:
	var delta := timer / spawn_time
	delta -= 1
	delta = 1 - delta * delta
	for spawn_effect in spawn_fx:
		spawn_effect.modulate.a = delta
		spawn_effect.scale.x = delta
		spawn_effect.scale.y = delta

func complete_spawn() -> void:
	spawn_mob(global_position, global_rotation)
	for spawn_pos in sub_spawn_positions:
		spawn_mob(spawn_pos.global_position, spawn_pos.global_rotation)
	picked_mob_info = null
	queue_free()

func spawn_mob(spawn_pos: Vector2, spawn_rot: float):
	var mob := picked_mob_info.create_instance()
	Global.root_2d_world.add_child(mob)
	mob.global_position = spawn_pos
	mob.global_rotation = spawn_rot
	mob.reset_physics_interpolation()
	mob.stats.level = WaveState.mob_level
	WaveState.add_mob(mob)

func pick_mob_info(wave_num: int) -> MobInfo:
	var arr := (
		MobManager.get_filter_flag_mobs(flags_include.flags, flags_exclude.flags, mob_pool_forced)
		if mob_pool_forced.size() > 0 else
		MobManager.get_filter_flag_mobs(flags_include.flags, flags_exclude.flags)
	)
	if arr.size() <= 0:
		return null
	var index := randi_range(0, arr.size() - 1)
	var mob_inf = arr[index]
	var off := arr.size()
	while mob_inf.mob_cost > spawn_cost_max || mob_inf.min_wave_num > wave_num:
		off -= 1
		if off < 0:
			return null
		mob_inf = arr[(index + off) % arr.size()]
	return mob_inf

func get_total_cost() -> float:
	return picked_mob_info.mob_cost * (sub_spawn_positions.size() + 1)
