class_name ProceduralWave
extends CompoundWave

@export var total_credits: float = 10
@export_range(0, 1) var wave_pacing: float = 0.5
@export_range(0, 1) var segment_length: float = 0.5
@export_range(0, 1) var diversity: float = 0.5

func generate_all_segments() -> Array[CompoundWaveSegment]:
	var arr: Array[CompoundWaveSegment] = []
	var credits_left := total_credits
	while credits_left > 0:
		var seg_credits = minf(segment_length * (1 - randf() * diversity) * total_credits, credits_left)
		var seg_pace_var = wave_pacing * randf() * diversity
		var seg_pacing = wave_pacing + randf_range(-seg_pace_var, seg_pace_var)
		var seg := generate_segment(seg_credits, seg_pacing)
		credits_left -= seg.wave.wave_credits
		arr.push_back(seg)
	return arr

func generate_segment(credits: float, pacing: float) -> CompoundWaveSegment:
	var wave := Wave.new()
	wave.wave_credits = credits
	wave.spawn_interval = (1 - pacing) * 2 + 0.5
	wave.max_mobs = roundi(credits * pacing * 0.9) + 1
	wave.mob_flags_include = mob_flags_include
	wave.mob_flags_exclude = mob_flags_exclude
	wave.force_mob_pool = force_mob_pool
	wave.mob_pool_forced = mob_pool_forced
	return CompoundWaveSegment.create(wave, CompoundWaveSegment.Order.SEQUENTIAL)

func begin():
	segments = generate_all_segments()
	super.begin()

func tick_difficulty_upward(amount: float):
	super.tick_difficulty_upward(amount)
	total_credits += 4 * amount
	var param_normalizer := Vector3(randf(), randf(), randf()).normalized()
	wave_pacing = 0.85 # sqrt(param_normalizer.x)
	segment_length = 0.45 # maxf(param_normalizer.y, 1 / wave_credits + 0.05)
	diversity = param_normalizer.z
