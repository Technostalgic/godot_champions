class_name CompoundWave
extends Wave

@export var segments: Array[CompoundWaveSegment] = []

var segment_cursor: int = 0

func begin() -> void:
	segment_cursor = 0
	current_progress = 0
	_is_complete = false
	_squad_cursor = 0
	for segment in segments:
		segment.wave.begin()

func pick_all_squads() -> Array[SquadSpawn]:
	push_error("Invalid call for compound wave, use one of its segments instead")
	return []

func pick_squad() -> SquadSpawn:
	push_error("Invalid call for compound wave, use one of its segments instead")
	return null

func handle_wave_state(delta: float) -> void:
	if segment_cursor >= segments.size():
		if !_is_complete:
			_is_complete = true
		return
	var cursor_add = 0
	while (
		cursor_add <= 0 || 
		segments[segment_cursor + cursor_add].order == CompoundWaveSegment.Order.SIMULTANEOUS
	):
		if !segments[segment_cursor + cursor_add].wave._is_complete:
			segments[segment_cursor + cursor_add].wave.handle_wave_state(delta)
		cursor_add += 1
		if segment_cursor + cursor_add >= segments.size():
			break
	while segments[segment_cursor].wave._is_complete:
		segment_cursor += 1
		if segment_cursor >= segments.size():
			_is_complete = true
			break
