class_name Wave
extends Resource

class SquadSpawn:
	static func create(wave_squad: WaveSquad.Instance, time: float) -> SquadSpawn:
		var spawn = SquadSpawn.new()
		spawn.squad = wave_squad
		spawn.spawn_time = time
		return spawn
	
	var squad: WaveSquad.Instance = null
	var spawn_time: float = 0

## Combined with the mob spawner's flags to build up a pool of mobs to spawn from 
## (if no mob pool specified specified)
@export var mob_flags_include: MobFlags = null
## Combined with the mob spawner's flags to narrow down a pool of mobs to spawn from 
## (if no mob pool specified specified)
@export var mob_flags_exclude: MobFlags = null
@export var force_mob_pool: bool = false
@export var mob_pool_forced: Array[MobInfo] = []
@export var squad_pool: Array[WaveSquad] = []
@export var wave_credits: float = 10
@export var max_mobs: int = 6
@export var spawn_interval: float = 1

var current_progress: float = 0
var picked_squads: Array[Wave.SquadSpawn] = []

var _squad_cursor: int = 0
var _is_complete: bool = false

var all_squads_spawned: bool :
	get: return _squad_cursor >= picked_squads.size()

func pick_all_squads() -> Array[SquadSpawn]:
	var arr := [] as Array[SquadSpawn]
	var cur_interval: float = 0
	var total_squad_cost: float = 0
	while total_squad_cost < wave_credits:
		var squad := pick_squad()
		var increment := squad.spawn_time
		squad.spawn_time += cur_interval
		cur_interval += increment
		arr.push_back(squad)
		var squad_cost := squad.squad.spawned_squad.calculate_squad_cost()
		total_squad_cost += squad_cost
	return arr

func pick_squad() -> SquadSpawn:
	var picked_squad := (
		squad_pool 
		if squad_pool.size() > 0 else 
		WaveState.base_squads
	).pick_random() as WaveSquad
	var squad_inst = (
		picked_squad.create_instance_forced_mob_pool(mob_pool_forced) 
		if force_mob_pool else
		picked_squad.create_instance()
	) 
	var spawn := SquadSpawn.create(squad_inst, spawn_interval)
	return spawn

func tick_difficulty_upward(amount: float) -> void:
	wave_credits += 4 * amount

func begin() -> void:
	_squad_cursor = 0
	_is_complete = false
	picked_squads = pick_all_squads()
	current_progress = 0

func complete() -> void:
	_is_complete = true
	WaveState.on_wave_completed()

func update(delta: float) -> void:
	if _is_complete: return
	handle_wave_state(delta)
	if _is_complete:
		complete()

func handle_wave_state(delta: float) -> void:
	if _is_complete || WaveState.paused || WaveState.active_mobs.size() >= max_mobs:
		return
	current_progress += delta
	if !all_squads_spawned:
		handle_spawning()
	elif WaveState.active_spawners_exhausted() && WaveState.active_mobs.size() <= 0:
		_is_complete = true

func handle_spawning() -> void:
	if _squad_cursor >= picked_squads.size():
		return
	var picked_squad := picked_squads[_squad_cursor] as SquadSpawn
	while picked_squad.spawn_time <= current_progress:
		WaveState.spawn_squad(picked_squad.squad.create_squad())
		_squad_cursor += 1
		if all_squads_spawned: break
		picked_squad = picked_squads[_squad_cursor]
