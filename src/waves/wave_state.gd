extends Node

signal mob_added(mob: Mob)
signal mob_killed(mob: Mob)
signal wave_completed()

@export var all_waves: Array[Wave] = []
@export var base_squads: Array[WaveSquad] = []
@export var base_items: Array[PackedScene] = []

var paused: bool = false
var wave_number: int = 1
var mob_level: float = 0
var difficulty_multiplier: float = 1

var _items_pool: Array[ItemContainer] = [] :
	get:
		if _items_pool.size() <= 0:
			for itm_scn in base_items:
				var inst = itm_scn.instantiate()
				_items_pool.push_back(inst)
		return _items_pool
var _items_spawned: Array[ItemContainer] = []
var _active_mobs: Array[Mob] = []
var _active_mobs_dirty: bool = true
var _active_spawners: Array[SquadNode] = []

var completed: bool :
	get:
		if !is_instance_valid(wave_resource):
			return false
		return wave_resource._is_complete

var active_mobs: Array[Mob] :
	get:
		if _active_mobs_dirty:
			var i: int = _active_mobs.size() - 1
			while i >= 0:
				if !_active_mobs[i].is_alive():
					_active_mobs.remove_at(i)
				i -= 1
			_active_mobs_dirty = false
		return _active_mobs

var wave_resource: Wave = null

func _process(delta: float) -> void:
	if wave_resource != null:
		wave_resource.update(delta)
		if !paused && !wave_resource._is_complete:
			_handle_items(delta)

func _mob_killed(mob: Mob):
	_active_mobs_dirty = true
	mob.on_kill.disconnect(_mob_killed)
	mob_killed.emit(mob)

func on_wave_completed():
	Global.respawn_fallen_heros()
	wave_number += 1
	mob_level += 0.25 * difficulty_multiplier
	tick_progression(1)
	wave_completed.emit()

func _handle_items(delta: float) -> void:
	if _items_spawned.size() <= 0:
		var base_item := _items_pool.pick_random() as ItemContainer
		var item := base_item.duplicate() as ItemContainer
		Global.root_2d_world.add_child(item) 
		item.global_position = Global.spawn_points.pick_random().global_position
		item.reset_physics_interpolation()
		var cont_itm := item.contained_item as Item
		if cont_itm is Consumable:
			cont_itm.used.connect(
				func(_user: Actor):
					_items_spawned.erase(item)
			)
			_items_spawned.push_back(item)
		else:
			item.queue_free()

func active_spawners_exhausted() -> bool:
	if _active_spawners.is_empty():
		return true
	while !is_instance_valid(_active_spawners[0]):
		_active_spawners.pop_front()
		if _active_spawners.is_empty():
			return true
	return _active_spawners.is_empty()

func spawn_squad(squad: SquadNode):
	var point := Global.spawn_points.pick_random().global_position as Vector2
	Global.root_2d_world.add_child(squad)
	squad.global_position = point
	_active_spawners.push_back(squad)

func add_mob(mob: Mob):
	mob.on_kill.connect(_mob_killed.bind(mob))
	active_mobs.push_back(mob)
	mob_added.emit(mob)

func remove_active_mobs():
	for mob in active_mobs:
		mob.remove()
	for spawn in _active_spawners:
		if is_instance_valid(spawn):
			spawn.queue_free()
	active_mobs.clear()
	_active_spawners.clear()

func reset():
	remove_active_mobs()
	for item in _items_spawned:
		if is_instance_valid(item):
			item.queue_free()
	_items_spawned.clear()

func restart_wave() -> void:
	if wave_resource == null:
		set_wave(pick_wave(Global.current_level))
	paused = false
	reset()
	wave_resource.begin()

func set_wave(wave: Wave) -> void:
	if wave_resource == wave:
		return
	wave_resource = wave.duplicate()
	tick_progression(wave_number)

func pick_wave(_level: Level) -> Wave:
	# TODO pick a wave based on level properties/flags
	if wave_resource != null:
		return wave_resource
	return all_waves.pick_random()

## Ticks the wave difficulty forward the specified number of times, increasing the wave difficulty
func tick_progression(amount: int = 1) -> void:
	while amount >= 0:
		wave_resource.tick_difficulty_upward(difficulty_multiplier)
		amount -= 1
