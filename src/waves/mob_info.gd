class_name MobInfo
extends Resource

enum Flag {
	NONE = 0,
	MELEE = 1 << 0,
	RANGED = 1 << 1,
	FLYING = 1 << 2,
	GROUND = 1 << 3,
	GRUNT = 1 << 4,
	ELITE = 1 << 5,
	FLESH = 1 << 6,
	STONE = 1 << 7,
	METAL = 1 << 8,
}

@export var mob_flags: MobFlags = null
@export var mob_scene: PackedScene = null
@export var mob_cost: float = 1
@export var min_wave_num: int = 0

func create_instance() -> Mob:
	var mob: Mob = MobManager.pick_mob(mob_scene)
	mob.wave_credit_cost = mob_cost
	return mob
