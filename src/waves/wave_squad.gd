class_name WaveSquad
extends Resource

class Instance:
	var squad_resource: WaveSquad = null
	var squad_scene: PackedScene = null
	var force_mob_pool: bool = false
	var mob_pool_forced: Array[MobInfo]
	
	var flags_incl: MobFlags :
		get: return squad_resource.flags_incl
	var flags_excl: MobFlags :
		get: return squad_resource.flags_excl
	
	var _spawned_squad: SquadNode = null
	var spawned_squad: SquadNode = null :
		get:
			if _spawned_squad == null:
				var squad := squad_scene.instantiate()
				if squad is SquadNode:
					squad.squad_instance = self
					_spawned_squad = squad
				else: push_error("Invalid squad scene")
			return _spawned_squad

	func create_squad() -> SquadNode:
		var _squad = _spawned_squad
		_spawned_squad = null
		return _squad

@export var mob_pool_forced: Array[MobInfo] = []
@export var flags_incl: MobFlags = null
@export var flags_excl: MobFlags = null
@export var squad_scene: PackedScene = null

func create_instance() -> Instance:
	var inst := Instance.new()
	inst.squad_resource = self
	inst.squad_scene = squad_scene
	return inst

func create_instance_forced_mob_pool(mob_pool_forced: Array[MobInfo]) -> Instance:
	var inst = create_instance()
	inst.force_mob_pool = true
	inst.mob_pool_forced = mob_pool_forced
	return inst
