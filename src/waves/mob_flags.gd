class_name MobFlags
extends Resource

enum Flag {
	NONE = 0,
	MELEE = 1 << 0,
	RANGED = 1 << 1,
	FLYING = 1 << 2,
	GROUND = 1 << 3,
	GRUNT = 1 << 4,
	ELITE = 1 << 5,
	FLESH = 1 << 6,
	STONE = 1 << 7,
	METAL = 1 << 8,
}

const FLAGS_NONE: int = 0
const FLAGS_ALL: int = 9223372036854775807

static func create(from_flags: Flag):
	var mob_flags := MobFlags.new()
	mob_flags.flags = from_flags
	return mob_flags

static func create_none() -> MobFlags:
	var m := MobFlags.new()
	m._exported_flags = MobFlags.FLAGS_ALL
	return m

static func create_all() -> MobFlags:
	var m := MobFlags.new()
	m._exported_flags = MobFlags.FLAGS_ALL
	return m

static func combine_add(a: MobFlags, b: MobFlags) -> MobFlags:
	if a == null:
		return b
	if b == null:
		return a
	return MobFlags.create(a.flags | b.flags)

static func combine_intersect(a: MobFlags, b: MobFlags) -> MobFlags:
	if a == null:
		return null
	if b == null:
		return null
	return MobFlags.create(a.flags & b.flags)

@export_flags(
	"Melee:1",
	"Ranged:2",
	"Flying:4",
	"Ground:8",
	"Grunt:16",
	"Elite:32",
	"Flesh:64",
	"Stone:128",
	"Metal:256",
) 
var _exported_flags: int = (
	Flag.MELEE |
	Flag.GRUNT | 
	Flag.FLESH 
) as int

var flags: Flag :
	get: return _exported_flags as Flag
	set(val): _exported_flags = val as int

func add_flags(t_flags: Flag) -> void:
	_exported_flags = _exported_flags | t_flags

func remove_flags(t_flags: Flag) -> void:
	_exported_flags = _exported_flags & t_flags

func contains_any_flags(t_flags: Flag) -> bool:
	return _exported_flags & t_flags > 0
