extends Area2D

@export var col_shape: CollisionShape2D

var picked_coins: Array[Coin] = []

func _ready():
	body_entered.connect(_on_body_enter)
	body_exited.connect(_on_body_exit)

func _physics_process(delta: float):
	var i: int = picked_coins.size()
	while i > 0:
		i -= 1
		if !is_instance_valid(picked_coins[i]):
			picked_coins.remove_at(i)
			continue
		var coin = picked_coins[i]
		coin.linear_velocity *= 0.9
		coin.angular_velocity = 0
		var dif = global_position - coin.global_position
		coin.linear_velocity += dif.normalized() * 1000 * delta

func _on_body_enter(body: Node2D):
	if body is Coin:
		picked_coins.push_back(body)

func _on_body_exit(body: Node2D):
	if body is Coin:
		picked_coins.erase(body)
