extends Behavior

@export var target_inventory: Inventory = null

func _ready():
	actor.body_entered.connect(_on_body_entered)
	
func _on_body_entered(body: Node):
	if !actor.is_alive():
		return
	if body is ItemContainer:
		body.pick_up(target_inventory)
	if body is Coin:
		body.pick_up(target_inventory)
