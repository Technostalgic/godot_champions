class_name GroundJumping
extends Behavior

signal on_jump_begin()

@export var grounded: GroundDetection = null
@export var jump_power: float = 100
@export var jump_sustain: float = 0.5

var _jump_queued: bool = false
var _is_jumping: bool = false
var _was_jumping: bool = false

func reset_stats():
	jump_sustain = actor.stats.jump
	super.reset_stats()

func recieve_jump_state(jumping: bool):
	_is_jumping = jumping
	if jumping:
		if !_was_jumping:
			_jump_queued = jumping
	else:
		_jump_queued = false

func _physics_process(delta: float):
	if grounded.is_on_ground():
		if (_is_jumping && !_was_jumping) || _jump_queued:
			do_jump()
	elif _is_jumping:
		do_jump_sustain(delta)
	_was_jumping = _is_jumping

func do_jump():
	if !actor.is_alive():
		return
	var up_axis = -actor.get_last_gravity().normalized()
	var fwd_axis = up_axis.rotated(PI * 0.5)
	var up_vel = jump_power #up_axis * actor.linear_velocity
	var fwd_vel = fwd_axis * actor.linear_velocity
	actor.linear_velocity = fwd_axis * fwd_vel + up_axis * up_vel
	grounded.set_ungrounded()
	_jump_queued = false
	on_jump_begin.emit()

func do_jump_sustain(delta: float):
	if !actor.is_alive():
		return
	var grav = actor.get_last_gravity()
	actor.linear_velocity += -grav * actor.gravity_scale * jump_sustain * delta
