class_name MovementBehavior
extends Behavior

@export var max_speed: float = 75
@export var acceleration_factor: float = 1

var _dex_mult: float = 1

func reset_stats():
	max_speed = actor.stats.speed
	_dex_mult = actor.stats.dexterity
	super.reset_stats()
