extends Behavior

@export var max_angular_speed: float = 100

func _physics_process(_delta: float):
	if absf(actor.angular_velocity) > max_angular_speed:
		actor.angular_velocity = max_angular_speed * signf(actor.angular_velocity)
