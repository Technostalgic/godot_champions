class_name GroundMovement
extends MovementBehavior

@export var grounded: GroundDetection = null
@export var ground_friction: float = 0.35

var movement: Vector2 = Vector2.ZERO
var move_dir: float = 0

func _physics_process(delta):
	if !actor.is_alive():
		return
	var horizontal_movement: float = 0;
	if movement.length_squared() >= 0.5:
		var fwd_axis := -actor.get_last_gravity().normalized().rotated(PI * 0.5)
		var mov_normalized := movement.normalized()
		if fwd_axis == Vector2.ZERO:
			var acceleration = max_speed * acceleration_factor * 0.25
			actor.linear_velocity += mov_normalized * acceleration * delta
			return
		var dot := fwd_axis.dot(mov_normalized)
		if absf(dot) > 0.1:
			horizontal_movement = signf(dot)
	move_horizontal(delta, horizontal_movement)

func _receive_movement(recieved_mov: Vector2):
	movement = recieved_mov

func move_horizontal(delta: float, horizontal_movement: float):
	
	# if no movement input, apply friction and return
	if absf(horizontal_movement) < 0.1:
		apply_ground_friction(delta)
		move_dir = 0
		return
	
	# calculate the relative velocity
	var up_axis := -actor.get_last_gravity().normalized()
	var fwd_axis := up_axis.rotated(PI * 0.5)
	var up_vel = up_axis.dot(actor.linear_velocity)
	var fwd_vel := fwd_axis.dot(actor.linear_velocity)
	
	# calculate desired motion parameters
	var targ_vel := max_speed * clampf(horizontal_movement, -1, 1)
	var acceleration = max_speed * acceleration_factor * _dex_mult
	
	# if speed is higher than wanted
	if absf(fwd_vel) > absf(targ_vel):
		var dif_vel_x := targ_vel - fwd_vel
		var acc_x: float = minf(acceleration, absf(dif_vel_x)) * signf(dif_vel_x) * delta
		if grounded.is_on_ground():
			if signf(fwd_vel) != signf(fwd_vel + acc_x):
				fwd_vel = 0
			else:
				fwd_vel += acc_x
		elif signf(acc_x) != signf(fwd_vel):
			fwd_vel += acc_x
		apply_ground_friction(delta)
	
	# if speed needs to be increased
	else: 
		
		# if ground friction helps accelerate toward target velocity, apply friction
		if absf(fwd_vel) > 0.1 && targ_vel != 0 && signf(fwd_vel) != signf(targ_vel):
			apply_ground_friction(delta)
		
		# calculate total horizontal acceleration for this step
		var acc_x: float = acceleration * horizontal_movement * delta
		
		# if acceleration causes actor to exceed speed limit, just set the velocity to target vel
		if absf(fwd_vel + acc_x) >= absf(targ_vel):
			fwd_vel = targ_vel
		
		# accelerate actor if the acceleration won't cause it to go over the speed limit
		else:
			fwd_vel += acc_x
	
	# apply the relative velocity changes to the actor
	actor.linear_velocity = fwd_axis * fwd_vel + up_axis * up_vel
	move_dir = signf(horizontal_movement)

func apply_ground_friction(delta: float):
	if grounded.is_on_ground():
		actor.linear_velocity *= pow(1 - ground_friction, delta * 50)
