extends Behavior

@export var target: Node2D = null
@export var max_rotate_speed: float = 2 * PI
@export var ang_offset: float = 0
@export var orient_after_death: bool = true

func _physics_process(delta):
	if !orient_after_death && !actor.is_alive():
		return
	if actor.linear_velocity != Vector2.ZERO:
		var max_delta = max_rotate_speed * delta;
		var fwd_dir := actor.linear_velocity.angle()
		var ang_dif := angle_difference(target.global_rotation + ang_offset, fwd_dir)
		if absf(ang_dif) >= PI - PI * 0.25:
			target.global_rotation += PI
			return
		var ang_delta := clampf(ang_dif, -max_delta, max_delta)
		if target is RigidBody2D:
			target.angular_velocity *= 0.9
		target.global_rotation += ang_delta;
