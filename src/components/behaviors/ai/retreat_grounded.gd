extends SeekTargetGrounded

@export var max_distance: float = 100

func _physics_process(delta: float):
	if !actor.is_alive():
		return
	_last_vel = actor.linear_velocity
	if is_instance_valid(targeting) && targeting.has_target():
		if (targeting.target.global_position - actor.global_position).length() <= max_distance:
			super._physics_process(delta)

func calc_move_dir(target_dif: Vector2) -> Vector2:
	var dir := -target_dif.normalized()
	if grounded.is_on_ground():
		if grounded._last_ground_norm.dot(dir) < 0:
			dir = (dir + grounded._last_ground_norm).normalized()
	return dir

func calc_should_jump(target_dif: Vector2) -> bool:
	return super.calc_should_jump(-target_dif)
