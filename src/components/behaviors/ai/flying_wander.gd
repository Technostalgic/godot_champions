class_name FlyingWander
extends Behavior

signal send_movement(mov: Vector2)

@export var targeting: TargetingBehavior = null
@export var wander_speed_factor: float = 0.5
@export var redirect_interval: float = 1

var wander_dir: Vector2 = Vector2.ZERO
var redirect_countdown: float = 0.0

func _ready():
	actor.body_entered.connect(_on_body_entered)

func _physics_process(delta: float):
	if !actor.is_alive():
		return
	if targeting == null || !targeting.has_target():
		handle_wander(delta)

func _on_body_entered(body: Node):
	if !actor.is_alive():
		return
	if body is CollisionObject2D:
		if actor.collision_mask & body.collision_layer == 0:
			return
	redirect_countdown = redirect_interval
	var normal := Vector2.ZERO
	var set_normal := false
	if body is CollisionObject2D:
		var contacts = DynamicGravityBody.get_contact_points(actor, body)
		if contacts.size() > 0:
			set_normal = true
			normal = (contacts[0] - contacts[1]).normalized()
	if set_normal:
		wander_dir = normal
	else:
		redirect()

func handle_wander(delta: float):
	if redirect_countdown <= 0:
		redirect()
	redirect_countdown -= delta
	send_movement.emit(wander_dir * wander_speed_factor)

func redirect():
	redirect_countdown = redirect_interval
	wander_dir = Vector2.from_angle(randf() * PI * 2)
