extends Node

signal send_movement(movement: Vector2)
signal send_jump_state(jumping: bool)

@export var actor: Actor = null
@export var grounded: GroundDetection = null
@export var targeting: TargetingBehavior = null
@export var wander_switch_interval_max: float = 8
@export var wander_switch_interval_min: float = 3

var wander_dir: Vector2 = Vector2.ZERO
var wander_dir_time_left: float = 0
var _last_vel: Vector2 = Vector2.ZERO
var _jump_timer: float = 0

func _physics_process(delta: float):
	if !actor.is_alive():
		return
	
	_last_vel = actor.linear_velocity
	if is_instance_valid(targeting) && targeting.has_target():
		return
	
	# choose random wnader direction whenever wander timer ends
	if wander_dir_time_left <= 0:
		wander_dir = Vector2.from_angle(randf_range(-PI, PI))
		wander_dir_time_left = randf_range(wander_switch_interval_min, wander_switch_interval_max)
	
	# if it's reached an area where the direction converges on the ground surface, reset timer
	# to choose a new wander direction
	var up_dir = -actor.get_last_gravity().normalized()
	var dot = wander_dir.dot(up_dir)
	if absf(dot) >= 0.9:
		_jump_timer = 0.75
		wander_dir_time_left = 0
	
	send_movement.emit(wander_dir)
	send_jump_state.emit(_jump_timer > 0)
	_jump_timer -= delta
	wander_dir_time_left -= delta

func _actor_integrate_forces(body: PhysicsDirectBodyState2D):
	if !is_instance_valid(targeting) || !targeting.has_target():
		if SeekTargetGrounded.check_collided_obstacles(actor, body, grounded, _last_vel):
			_jump_timer = 0.5
