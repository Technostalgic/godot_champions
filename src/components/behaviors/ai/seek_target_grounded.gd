class_name SeekTargetGrounded
extends Behavior

signal send_movement(movement: Vector2)
signal send_jump_state(jumping: bool)

@export var grounded: GroundDetection = null
@export var targeting: TargetingBehavior = null
@export var min_distance: float = 0
@export var jump_follow_dist: float = 50

var _last_vel: Vector2 = Vector2.ZERO
var _obstacle_jump_timer: float = 0

static func check_collided_obstacles(
	self_actor: Actor, body: PhysicsDirectBodyState2D, grounding: GroundDetection, last_vel: Vector2
) -> bool:
	
	# if on ground, check to see if obstacle was hit
	if grounding.is_on_ground() && grounding.was_on_ground():
		
		# iterate through each contact to see if it was an obstacle
		var i := body.get_contact_count() - 1
		while i >= 0:
			
			var other = body.get_contact_collider_object(i)
			if other is CollisionObject2D:
				var col_intersect = self_actor.collision_mask & other.collision_layer
				if col_intersect == 0:
					i -= 1
					continue
			
			# detect obstancles by checking to see if the normal was in the opposite direction 
			# as the actor was travelling
			var norm = body.get_contact_local_normal(i)
			if norm.dot(last_vel.normalized()) <= -0.5:
				return true
			i -= 1
	return false

func _physics_process(delta: float):
	if !actor.is_alive():
		return
	_last_vel = actor.linear_velocity
	if is_instance_valid(targeting) && targeting.has_target():
		var dist := (targeting.target.position - actor.global_position).length()
		if dist >= min_distance:
			seek_target(targeting.target.position)
		_obstacle_jump_timer -= delta

func _actor_integrate_forces(body: PhysicsDirectBodyState2D):
	if is_instance_valid(targeting) && targeting.has_target():
		if SeekTargetGrounded.check_collided_obstacles(actor, body, grounded, _last_vel):
			_obstacle_jump_timer = 0.5

func seek_target(target_pos: Vector2):
		var dif := target_pos - actor.global_position
		send_movement.emit(calc_move_dir(dif))
		var should_jump: bool = calc_should_jump(dif)
		send_jump_state.emit((should_jump || _obstacle_jump_timer > 0) && randf() < 0.95)

func calc_move_dir(dif_target: Vector2) -> Vector2:
	var dir := dif_target.normalized()
	if grounded.is_on_ground():
		if grounded._last_ground_norm.dot(dir) < 0:
			dir = (dir + grounded._last_ground_norm).normalized()
	return dir

func calc_should_jump(dif_target: Vector2) -> bool:
	var dir := dif_target.normalized()
	var up_dir := -actor.get_last_gravity().normalized()
	var jump_thresh := 0.9
	if dir.length_squared() <= jump_follow_dist:
		jump_thresh = 0.5
	return dir.dot(up_dir) >= jump_thresh
