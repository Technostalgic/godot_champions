class_name TargetingBehavior
extends Behavior

@export var target: Node2D = null

func is_target(targ: Node2D) -> bool:
	return targ != null && is_instance_valid(targ)

func has_target() -> bool:
	return target != null && is_instance_valid(target)
