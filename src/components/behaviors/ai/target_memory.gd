extends TargetingBehavior

@export var base_targeting: TargetingBehavior = null
@export var memory_time_max: float = 5

var memory_left: float = 0

func _physics_process(delta: float):
	if !base_targeting.actor.is_alive():
		target = null
		return
	
	if memory_left <= 0:
		target = null
	else:
		if target is Actor:
			if !target.is_alive():
				target = null
		memory_left -= delta
	
	if is_instance_valid(base_targeting):
		if base_targeting.has_target():
			target = base_targeting.target
			memory_left = memory_time_max
