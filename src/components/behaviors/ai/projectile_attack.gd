extends BasicWeapon

@export var parent_equipper: Node2D = null
@export var targeting: TargetingBehavior = null
@export var max_attack_range: float = 150
@export var min_attack_range: float = 50

func _receive_trigger():
	global_position = parent_equipper.global_position
	super._receive_trigger()

func _physics_process(delta: float):
	if parent_equipper is Actor && !parent_equipper.is_alive():
		return
	if targeting.has_target():
		var dif: Vector2 = targeting.target.global_position - parent_equipper.global_position
		var dist = dif.length()
		_receive_aim(dif.normalized())
		if dist >= min_attack_range && dist <= max_attack_range:
			_receive_trigger()
	super._physics_process(delta)
