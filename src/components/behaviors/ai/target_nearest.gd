extends TargetingBehavior

signal found_target(targ: ProjectileTarget)
signal lost_target()

@export var check_los: bool = true

const SEARCH_INTERVAL = 0.35
var search_cooldown = 0

func _physics_process(delta: float):
	if !actor.is_alive():
		target = null
		return
	if !is_instance_valid(target):
		target = null
	if search_cooldown <= 0:
		var had_target := has_target()
		if !can_see_target(target):
			if has_target():
				target = null
			search_for_target()
			if had_target && !has_target():
				lost_target.emit()
			elif !had_target && has_target():
				found_target.emit(target)
	search_cooldown -= delta

func search_for_target():
	if actor is Mob:
		var closest_targ: ProjectileTarget = null
		var closest_dist: float = INF
		for hero in Global.heros:
			if !is_instance_valid(hero) || !hero.is_alive():
				continue
			if !can_see_target(hero): 
				continue
			var cur_dist = hero.global_position.distance_squared_to(actor.global_position);
			if closest_targ == null || cur_dist < closest_dist:
				closest_targ = hero
				closest_dist = cur_dist
		if closest_targ != null:
			target = closest_targ
	search_cooldown = SEARCH_INTERVAL

func can_see_target(targ: Node2D) -> bool:
	if !is_target(targ):
		return false
	if targ is Actor:
		if !targ.is_alive():
			return false
	if !check_los:
		return true
	var space_state = PhysicsServer2D.space_get_direct_state(actor.get_world_2d().space)
	var ray = PhysicsRayQueryParameters2D.create(
		actor.global_position, 
		targ.global_position, 
		1 << 0)
	var result = space_state.intersect_ray(ray)
	return result.is_empty()
