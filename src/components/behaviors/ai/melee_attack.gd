extends Behavior

signal attack_target(targ: ProjectileTarget)

@export var attack_scn: PackedScene = null
@export var attack_delay: float = 1
@export var damage: float = 10
@export var knockback: float = 50

var _power_mult: float = 1
var _proj_stats: Stats.ProjectileStats
var _attack_cooldown: float = 0

func reset_stats():
	_power_mult = (actor.stats.power - 1) * 0.25 + 1
	if _proj_stats == null:
		_proj_stats = Stats.ProjectileStats.new()
	_proj_stats.damage = damage * _power_mult
	_proj_stats.knockback = knockback * _power_mult
	super.reset_stats()

func _physics_process(delta):
	_attack_cooldown -= delta

func _on_collide(node: Node):
	if _attack_cooldown > 0:
		return
	if node is ProjectileTarget:
		if node.allegiance != actor.allegiance:
			attack(node)

func attack(targ: ProjectileTarget):
	if !actor.is_alive():
		return
	attack_target.emit(targ)
	_attack_cooldown = attack_delay
	var atk_pos = GravityField.project_on_collider(actor, targ.global_position)
	var atk_node = attack_scn.instantiate()
	if atk_node is MeleeHit:
		atk_node.proj_stats = _proj_stats
		atk_node.target = targ
		add_child_at_pos.call_deferred(actor.get_parent(), atk_node, atk_pos)

func add_child_at_pos(parent: Node, child: Node, global_pos: Vector2):
		parent.add_child(child)
		child.global_position = global_pos
