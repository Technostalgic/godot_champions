extends Behavior

signal send_movement(mov: Vector2)

@export var targeting: TargetingBehavior = null
@export var orbit_distance: float = 70
@export var lateral_motion_factor: float = 50

var _orbit_direction: float = 1

func _ready():
	_orbit_direction = signf(randf() - 0.5)
	actor.body_entered.connect(_on_body_entered)

func _physics_process(_delta: float): 
	if !actor.is_alive():
		return
	if targeting.has_target():
		var dif := targeting.target.global_position - actor.global_position
		var dist = dif.length()
		
		# calculate orbit factor from bell curve
		var exp_top: float = dist - orbit_distance
		exp_top *= exp_top
		var orbit_factor: float = pow(2, -exp_top / lateral_motion_factor)
		
		# get the direction for orbiting and the direction that the target is in
		var target_dir := dif.normalized()
		var orbit_dir := target_dir
		if dist < orbit_distance:
			target_dir *= -1
		if _orbit_direction > 0:
			orbit_dir = orbit_dir.rotated(PI * 0.5)
		else:
			orbit_dir = orbit_dir.rotated(-PI * 0.5)
			
		# calculate and send movement based on orbit factor
		var movement := Vector2.ZERO
		movement += orbit_dir * orbit_factor
		movement += target_dir * (1 - orbit_factor)
		send_movement.emit(movement)

func _on_body_entered(_body: Node):
	_orbit_direction *= -1
