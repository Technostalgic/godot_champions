extends Behavior

signal send_movement(mov: Vector2)

@export var targeting: TargetingBehavior = null
@export var min_distance: float = 0

func _physics_process(_delta: float): 
	if !actor.is_alive():
		return
	if targeting.has_target():
		var dif := targeting.target.global_position - actor.global_position
		if dif.length_squared() > min_distance * min_distance:
			send_movement.emit(dif.normalized())
