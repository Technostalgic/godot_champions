class_name Interactor
extends Behavior

var interactibles: Array[Interactible]

func _ready() -> void:
	actor.on_kill.connect(on_killed)

func interact():
	var i: int = 0
	for intr in interactibles:
		if !is_instance_valid(intr):
			interactibles.remove_at(i)
			continue
		intr.interact(actor)
		i += 1

func interact_hold():
	var i: int = 0
	for intr in interactibles:
		if !is_instance_valid(intr):
			interactibles.remove_at(i)
			continue
		intr.interact_hold(actor)
		i += 1

func can_interact():
	for intr in interactibles:
		if intr.enabled:
			return true
	return false

func on_killed():
	interactibles.clear()

func add_interactible(interactible: Interactible):
	interactibles.push_back(interactible)

func remove_interactible(interactible: Interactible):
	interactibles.erase(interactible)
