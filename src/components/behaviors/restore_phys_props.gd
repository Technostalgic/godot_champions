extends Behavior

var gravity_scale: float = 0
var collision_layer: int = 0
var collision_mask: int = 0
var lock_rotation: bool = false
var _initialized: bool = false

func _ready() -> void:
	gravity_scale = actor.gravity_scale
	collision_layer = actor.collision_layer
	collision_mask = actor.collision_mask
	lock_rotation = actor.lock_rotation
	_initialized = true

func _enter_tree() -> void:
	restore_properties()

func restore_properties():
	if !_initialized:
		return
	actor.gravity_scale = gravity_scale
	actor.collision_layer = collision_layer
	actor.collision_mask = collision_mask
	actor.lock_rotation = lock_rotation
