class_name InputBehavior
extends Behavior

signal send_movement(movement: Vector2)
signal send_jumping(jumping: bool)
signal send_attacking_primary(attacking: bool)
signal send_attacking_secondary(attacking: bool)
signal send_reloading(reloading: bool)
signal send_aim(aim: Vector2)
signal send_aim_reset()

const IN_MOVE_RIGHT: StringName = &"move_right"
const IN_MOVE_LEFT : StringName = &"move_left"
const IN_MOVE_UP : StringName = &"move_up"
const IN_MOVE_DOWN : StringName = &"move_down"
const IN_AIM_X_POS : StringName = &"aim_x+"
const IN_AIM_X_NEG : StringName = &"aim_x-"
const IN_AIM_Y_POS : StringName = &"aim_y+"
const IN_AIM_Y_NEG : StringName = &"aim_y-"
const IN_JUMP : StringName = &"jump"
const IN_ATTACK1 : StringName = &"attack1"
const IN_ATTACK2 : StringName = &"attack2"
const IN_RELOAD : StringName = &"reload"
const IN_INTERACT : StringName = &"interact"
const IN_INVENTORY : StringName = &"inventory"
const IN_START : StringName = &"start"
const INTERACT_HOLD_THRESH : float = 0.5

@export var ui_player_id: int = 0
@export var controller_id: int = -1
@export var ui_cursor_prefab: PackedScene = null
@export var aim_sensitivity: float = 0.25
@export var aim_assistance: float = 1
@export var interactor: Interactor = null

var aim_assist_col: Area2D = null
var aim_assist_dist: float = 300
var aim_assist_detection_width: float = 48

var _aim_assist_raycast: RayCast2D = null
var _aim_assist_targets: Array[RigidBody2D] = []
var _aim_assist_last_targ: RigidBody2D = null
var _aim_assist_valid: bool = false
var _aim_assist_target_dif: Vector2 = Vector2.ZERO
var _aim_assist_target_ang: float = 0
var _use_mouse_keyboard: bool = true

## Dictionary<StringName, StringName> NOTE - only handles single input collisions
var _input_collision_map: Dictionary = {}

var ui_mode: bool = false
var ui_control: Control = null
var marked_control: Control = null
var ui_cursor: Control = null
var ui_marker: Control = null
var _wep_ui: WeaponUI = null

var in_move_right: StringName = IN_MOVE_RIGHT
var in_move_left: StringName = IN_MOVE_LEFT
var in_move_up: StringName = IN_MOVE_UP
var in_move_down: StringName = IN_MOVE_DOWN
var in_aim_x_pos: StringName = IN_AIM_X_POS
var in_aim_x_neg: StringName = IN_AIM_X_NEG
var in_aim_y_pos: StringName = IN_AIM_Y_POS
var in_aim_y_neg: StringName = IN_AIM_Y_NEG
var in_jump: StringName = IN_JUMP
var in_attack1: StringName = IN_ATTACK1
var in_attack2: StringName = IN_ATTACK2
var in_reload: StringName = IN_RELOAD
var in_interact: StringName = IN_INTERACT
var in_inventory: StringName = IN_INVENTORY
var in_start: StringName = IN_START

var _interact_hold_time: float = 0

func _ready():
	create_aim_assist_cone()
	set_controller_id(controller_id)
	actor.on_kill.connect(_on_kill)

func _process(_delta):
	
	if !is_instance_valid(aim_assist_col):
		create_aim_assist_cone()
	
	if is_instance_valid(ui_control):
		if !ui_control.is_visible_in_tree():
			ui_control = null
	
	if Input.is_action_just_pressed(in_inventory):
		toggle_inventory()
	
	if Input.is_action_just_pressed(in_start):
		if !inv_is_open():
			Global.pause_game.call_deferred(true)
		else:
			toggle_inventory()
	
	if !ui_mode:
		handle_gameplay_input(_delta)
	else:
		send_idle_gameplay_input()
		if !_use_mouse_keyboard:
			handle_ui_input(_delta)
		
	handle_ui_cursor_and_marker(_delta)

func _get_inputs_array() -> Array[StringName]:
	return [
		in_move_right,
		in_move_left,
		in_move_up,
		in_move_down,
		in_aim_x_pos,
		in_aim_x_neg,
		in_aim_y_pos,
		in_aim_y_neg,
		in_jump,
		in_attack1,
		in_attack2,
		in_reload,
		in_interact,
		in_inventory,
		in_start,
	]

func _find_input_collisions() -> void:
	_input_collision_map.clear()
	var arr = _get_inputs_array()
	for in0 in arr:
		var evts0 := InputMap.action_get_events(in0)
		for evt0 in evts0:
			if evt0 is InputEventJoypadButton || evt0 is InputEventJoypadMotion:
				for in1 in arr:
					if in0 == in1:
						continue
					var evts1 := InputMap.action_get_events(in1)
					for evt1 in evts1:
						if evt1 is InputEventJoypadButton:
							if evt0 is InputEventJoypadButton:
								if (
									evt1.device == evt0.device &&
									evt1.button_index == evt0.button_index
								):
									_input_collision_map[in0] = in1
									_input_collision_map[in1] = in0
						elif evt1 is InputEventJoypadMotion:
							if evt0 is InputEventJoypadMotion:
								if (
									evt1.device == evt0.device && 
									evt1.axis == evt0.axis && 
									evt1.axis_value == evt0.axis_value
								):
									_input_collision_map[in0] = in1
									_input_collision_map[in1] = in0

func _on_kill():
	if _wep_ui != null:
		toggle_inventory()

func _on_enter_aim_assist(col: Node2D):
	if col is ProjectileTarget:
		_aim_assist_targets.push_back(col)

func _on_exit_aim_assist(col: Node2D):
	if col is ProjectileTarget:
		_aim_assist_targets.erase(col)

func inv_is_open() -> bool:
	return _wep_ui != null

func create_aim_assist_cone():
	aim_assist_detection_width = 48 * aim_assistance
	if is_instance_valid(aim_assist_col):
		aim_assist_col.queue_free()
	if !is_instance_valid(_aim_assist_raycast):
		_aim_assist_raycast = RayCast2D.new()
		_aim_assist_raycast.collision_mask = (
			GlobalClass.CollisionLayer.TERRAIN | 
			GlobalClass.CollisionLayer.PROPS
		)
		_aim_assist_raycast.hide()
		actor.add_child.call_deferred(_aim_assist_raycast)
	aim_assist_col = Area2D.new()
	aim_assist_col.hide()
	aim_assist_col.collision_layer = 0
	aim_assist_col.collision_mask = GlobalClass.CollisionLayer.MOBS
	var circle := CircleShape2D.new()
	circle.radius = aim_assist_detection_width
	var box := RectangleShape2D.new()
	box.size = Vector2(aim_assist_dist, aim_assist_detection_width * 2)
	var box_shape := CollisionShape2D.new()
	box_shape.shape = box
	box_shape.position.x = aim_assist_detection_width + box.size.x * 0.5
	var circ_shape := CollisionShape2D.new()
	circ_shape.shape = circle
	aim_assist_col.add_child(circ_shape)
	aim_assist_col.add_child(box_shape)
	actor.add_child.call_deferred(aim_assist_col)
	circ_shape.position.x = aim_assist_detection_width
	aim_assist_col.body_entered.connect(_on_enter_aim_assist)
	aim_assist_col.body_exited.connect(_on_exit_aim_assist)

func find_aim_assist_target_pos(aim_vec: Vector2):
	var wep: Weapon = null
	if actor is Hero:
		wep = actor.weapon_slot.get_equipped_weapons()[0] as Weapon
	if wep == null || aim_assistance <= 0 || _aim_assist_targets.size() <= 0:
		_aim_assist_last_targ = null
		_aim_assist_valid = false
		return
	
	var wep_pos := actor.global_position
	var stats := wep.get_shot_stats()
	var avg_speed := stats.projectile_speed + (stats.projectile_speed * stats.speed_variance) * 0.5
	var final_target_pos: Vector2 = Vector2.ZERO
	var final_distance_sq: float = INF
	var aim_assist_cone: float = PI * 0.2
	_aim_assist_valid = false
	for targ in _aim_assist_targets:
		var targ_pos := targ.global_position
		var ang_dist := aim_vec.angle_to(targ_pos - wep_pos)
		if absf(ang_dist) > aim_assist_cone:
			continue
		var targ_dist := wep_pos.distance_to(targ_pos)
		var travel_time := targ_dist / avg_speed
		var targ_displ := travel_time * targ.linear_velocity
		var predicted_targ_pos := targ_pos + targ_displ
		var predicted_targ_off := predicted_targ_pos - wep_pos
		var projected_pos := predicted_targ_off.project(aim_vec.normalized()) + wep_pos
		var projected_distance_sq := projected_pos.distance_squared_to(predicted_targ_pos)
		if projected_distance_sq < final_distance_sq:
			_aim_assist_raycast.global_position = actor.global_position
			_aim_assist_raycast.target_position = predicted_targ_pos * actor.global_transform
			_aim_assist_raycast.force_raycast_update()
			if !_aim_assist_raycast.is_colliding():
				final_target_pos = predicted_targ_pos
				final_distance_sq = projected_distance_sq
				_aim_assist_valid = true
				
	_aim_assist_target_dif = final_target_pos - wep_pos
	_aim_assist_target_ang = wep_pos.angle_to_point(final_target_pos)

func position_aim_assist_cone(aim_vec: Vector2):
	if actor is Hero:
		aim_assist_col.global_position = actor.weapon_slot.global_position
	else:
		aim_assist_col.global_position = actor.global_position
	aim_assist_col.global_rotation = aim_vec.angle()

func get_adjusted_aim_assist(aim_vec: Vector2) -> Vector2:
	var cur_ang := aim_vec.angle()
	var cur_mag = aim_vec.length()
	var dist = _aim_assist_target_dif.project(aim_vec).distance_to(_aim_assist_target_dif)
	if dist <= aim_assist_detection_width:
		var factor = dist / (aim_assist_detection_width * 1)
		factor = clampf(factor, 0, 1)
		factor = 3 * factor * factor - 2 * factor * factor * factor
		var final_ang = lerp_angle(cur_ang, _aim_assist_target_ang, 1 - factor)
		aim_vec = Vector2.from_angle(final_ang) * cur_mag
	return aim_vec

func toggle_inventory():
	if _wep_ui != null:
		_wep_ui.hide()
		_wep_ui = null
		Global.gameplay_ui_container.organize_wep_ui_container()
		ui_mode = Global.gameplay_ui_container.is_complete_wave_popup_open()
	else:
		_wep_ui = Global.gameplay_ui_container.open_unused_weapon_ui(ui_player_id)
		if _wep_ui != null:
			PlayerFocusedControl.set_control_buttons_to_id(_wep_ui, ui_player_id)
			ui_control = PlayerFocusedControl.find_compatible_button(_wep_ui, ui_player_id)
			ui_mode = true

func get_ui_cursor() -> Control:
	if ui_cursor == null:
		ui_cursor = ui_cursor_prefab.instantiate() as Control
		ui_cursor.modulate = GlobalClass.PLAYER_COLORS[ui_player_id]
		ui_cursor.hide()
		Global.gameplay_ui_container.add_child(ui_cursor)
	return ui_cursor

func get_ui_marker():
	if ui_marker == null:
		ui_marker = ui_cursor_prefab.instantiate() as Control
		ui_marker.modulate = GlobalClass.PLAYER_COLORS[ui_player_id]
		ui_marker.modulate.a = 0.25
		ui_marker.hide()
		Global.gameplay_ui_container.add_child(ui_marker)
	return ui_marker

func handle_ui_input(_delta: float):
	
	var mov := Vector2.ZERO
	if Input.is_action_just_pressed(in_move_right):
		mov.x += 1
	if Input.is_action_just_pressed(in_move_left):
		mov.x -= 1
	if Input.is_action_just_pressed(in_move_up):
		mov.y -= 1
	if Input.is_action_just_pressed(in_move_down):
		mov.y += 1
	
	if mov.length_squared() > 0:
		if !is_instance_valid(ui_control) || !ui_control.is_visible_in_tree():
			ui_control = PlayerFocusedControl.find_compatible_button(Global, ui_player_id)
		else:
			var side := SIDE_RIGHT
			if mov.x < 0: side = SIDE_LEFT
			if mov.y < 0: side = SIDE_TOP
			if mov.y > 0: side = SIDE_BOTTOM
			ui_control = PlayerFocusedControl.find_valid_focus_neighbor(
				ui_control, ui_player_id, side
			)
	
	if Input.is_action_just_pressed(in_attack2):
		if is_instance_valid(_wep_ui):
			_wep_ui.swap_alt_mod_tab()
			
	if !is_instance_valid(ui_control):
		return
	
	if Input.is_action_just_pressed(in_reload):
		InfoPopup.try_info_popup(ui_control)
	
	if Input.is_action_just_pressed(in_jump):
		select_control(ui_control)

func select_control(control: Control):
	if control is BaseButton:
		if control.disabled:
			return
		if control is ItemSlot:
			select_item_slot(control)
			return
		if control.toggle_mode:
			control.set_pressed_no_signal(!control.button_pressed)
		#if control is MenuButton:
		#	if !control.get_popup().visible:
		#		control.show_popup()
		#		var pid = PlayerFocusedControl.get_focus_id(control)
		#		if pid >= 0:
		#			PlayerFocusedControl.set_all_controls_to_id(control.get_popup(), pid)
		control.pressed.emit()
	
	elif control is TabBar:
		var des_tab = control.current_tab + 1
		if des_tab >= control.tab_count:
			control.current_tab = 0
		control.current_tab = des_tab

func select_item_slot(slot: ItemSlot):
	var slot_parent = slot.get_parent()
	if is_instance_valid(slot_parent):
		slot_parent = slot_parent.get_parent()
		if is_instance_valid(slot_parent):
			slot_parent = slot_parent.get_parent()
			if is_instance_valid(slot_parent):
				if slot_parent is PlayerReward:
					slot.activate()
					return
	if marked_control == slot:
		marked_control = null
	elif is_instance_valid(marked_control):
		swap_slots(marked_control, slot)
		marked_control = null
	else:
		marked_control = slot

func swap_slots(from_slot: ItemSlot, to_slot: ItemSlot):
	
	# swap direction if from is empty, do nothing if both are empty
	if from_slot.is_empty():
		var islot = from_slot
		from_slot = to_slot
		to_slot = islot
	if from_slot.is_empty():
		return

	from_slot.activate()
	to_slot.activate()
	from_slot.activate()

func handle_ui_cursor_and_marker(_delta: float):
	if !ui_mode || _use_mouse_keyboard:
		get_ui_cursor().hide()
		get_ui_marker().hide()
		return
	if !is_instance_valid(ui_control):
		get_ui_cursor().hide()
		ui_control = null
		return
	elif !get_ui_cursor().is_visible_in_tree():
		ui_cursor.show()
	
	# if PlayerFocusedControl.get_focus_id(ui_control) == ui_player_id:
	# 	ui_cursor.modulate.a = 1.0
	# else:
	# 	ui_cursor.modulate.a = 0.25
	
	ui_cursor.global_position = ui_control.global_position - Vector2.ONE * 10
	ui_cursor.custom_minimum_size = ui_control.size + Vector2.ONE * 20
	ui_cursor.size.x = 0
	ui_cursor.size.y = 0
	ui_cursor.update_minimum_size()
	
	if is_instance_valid(marked_control) && marked_control.is_visible_in_tree():
		ui_marker.global_position = marked_control.global_position - Vector2.ONE * 5
		ui_marker.custom_minimum_size = marked_control.size + Vector2.ONE * 10
		ui_marker.size.x = 0
		ui_marker.size.y = 0
		ui_marker.update_minimum_size()
		ui_marker.show()
		pass
	else:
		ui_marker.hide()
		marked_control = null

func handle_gameplay_input(delta: float):
	if !actor.is_alive():
		return
	if Global.suspend_gameplay_input:
		return
	_use_mouse_keyboard = controller_id < 0
	
	var mov := Vector2.ZERO
	var jumping := false
	var atk_1 := false
	var atk_2 := false
	var reloading := false
	var aim := Vector2.ZERO
	
	if Input.is_action_pressed(in_move_right):
		mov.x += 1
	if Input.is_action_pressed(in_move_left):
		mov.x -= 1
	if Input.is_action_pressed(in_move_up):
		mov.y -= 1
	if Input.is_action_pressed(in_move_down):
		mov.y += 1
	
	jumping = Input.is_action_pressed(in_jump)
	atk_1 = Input.is_action_pressed(in_attack1)
	atk_2 = Input.is_action_pressed(in_attack2)
	reloading = Input.is_action_pressed(in_reload)
	
	if _use_mouse_keyboard:
		var dif = actor.get_global_mouse_position() - actor.global_position
		var dist = dif.length()
		aim = dif.normalized() * minf(1, dist * 0.015)
		send_aim_reset.emit()
	else:
		aim = (
			Vector2.RIGHT * Input.get_axis(in_aim_x_neg, in_aim_x_pos) + 
			Vector2.DOWN * Input.get_axis(in_aim_y_neg, in_aim_y_pos) )
		if aim.length() >= 0.1:
			aim = aim.normalized() * aim_sensitivity
		else:
			aim = Vector2.ZERO
	
	send_movement.emit(mov)
	send_jumping.emit(jumping)
	send_attacking_primary.emit(atk_1)
	send_attacking_secondary.emit(atk_2)
	
	if interactor.can_interact():
		if _input_collision_map.has(in_interact):
			reloading = false
		if Input.is_action_pressed(in_interact):
			_interact_hold_time += delta
			if _interact_hold_time >= INTERACT_HOLD_THRESH:
				interactor.interact_hold()
		else:
			_interact_hold_time = 0
			if Input.is_action_just_released(in_interact):
				interactor.interact()
	else:
		_interact_hold_time = 0
	
	send_reloading.emit(reloading)
	
	if aim.length_squared() > 0:
		position_aim_assist_cone(aim)
		find_aim_assist_target_pos(aim)
		if _aim_assist_valid:
			aim = get_adjusted_aim_assist(aim)
		send_aim.emit(aim)

func send_idle_gameplay_input():
	send_movement.emit(Vector2.ZERO)
	send_jumping.emit(false)
	send_attacking_primary.emit(false)
	send_attacking_secondary.emit(false)
	send_reloading.emit(false)

func set_controller_id(plr_num: int = -1):
	controller_id = plr_num
	in_move_right = create_input_action(IN_MOVE_RIGHT, controller_id)
	in_move_left = create_input_action(IN_MOVE_LEFT, controller_id)
	in_move_up = create_input_action(IN_MOVE_UP, controller_id)
	in_move_down = create_input_action(IN_MOVE_DOWN, controller_id)
	in_aim_x_pos = create_input_action(IN_AIM_X_POS, controller_id, 0)
	in_aim_x_neg = create_input_action(IN_AIM_X_NEG, controller_id, 0)
	in_aim_y_pos = create_input_action(IN_AIM_Y_POS, controller_id, 0)
	in_aim_y_neg = create_input_action(IN_AIM_Y_NEG, controller_id, 0)
	in_jump = create_input_action(IN_JUMP, controller_id)
	in_attack1 = create_input_action(IN_ATTACK1, controller_id)
	in_attack2 = create_input_action(IN_ATTACK2, controller_id)
	in_reload = create_input_action(IN_RELOAD, controller_id)
	in_interact = create_input_action(IN_INTERACT, controller_id)
	in_inventory = create_input_action(IN_INVENTORY, controller_id)
	in_start = create_input_action(IN_START, controller_id)
	_find_input_collisions()

func set_ui_player_id(id: int = -1):
	ui_player_id = id
	if ui_cursor != null:
		ui_cursor.modulate = GlobalClass.PLAYER_COLORS[id]
	if ui_marker != null:
		ui_marker.modulate = GlobalClass.PLAYER_COLORS[id]
		ui_marker.modulate.a = 0.25

func create_input_action(action_name: String, player_num: int, dead_zone: float = 0.5) -> String:
	
	if player_num < 0:
		return action_name
	
	var new_action = action_name
	if player_num >= 0:
		new_action += str(player_num)
	
	if !InputMap.has_action(new_action):
		InputMap.add_action(new_action)
		InputMap.action_set_deadzone(new_action, dead_zone)
	
	for evt in InputMap.action_get_events(action_name):
		var new_evt: InputEvent = null
		if evt is InputEventJoypadButton:
			new_evt = evt.duplicate()
			(new_evt as InputEventJoypadButton).device = player_num
		elif evt is InputEventJoypadMotion:
			new_evt = evt.duplicate()
			(new_evt as InputEventJoypadMotion).device = player_num
		if new_evt != null:
			InputMap.action_add_event(new_action, new_evt)
	
	return new_action
