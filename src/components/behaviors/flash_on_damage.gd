extends Behavior

@export var target: ProjectileTarget = null
@export var flash_fx: FlashEffect = null

func _ready() -> void:
	target.on_hit.connect(_on_hit)

func _on_hit(_proj: Projectile, _damage: float, _point: Vector2) -> void:
	if !target.is_alive():
		return
	flash_fx.flash(0.05)
