extends Behavior

@export var weapon: BasicWeapon = null
@export var targeting: TargetingBehavior = null
@export var max_rotate_speed: float = 2 * PI

var _dex_mult: float = 1

func reset_stats():
	_dex_mult = actor.stats.dexterity
	super.reset_stats()

func _physics_process(delta):
	if !actor.is_alive():
		return
	if is_instance_valid(targeting) && !targeting.has_target():
		return
	var max_delta = max_rotate_speed * delta * _dex_mult;
	var fwd_dir := weapon.global_rotation
	var ang_dif := angle_difference(actor.global_rotation, fwd_dir)
	if absf(ang_dif) >= PI - PI * 0.25:
		actor.global_rotation += PI
		return
	var ang_delta := clampf(ang_dif, -max_delta, max_delta)
	actor.angular_velocity *= 0.9
	actor.global_rotation += ang_delta;
