class_name Behavior
extends Node

@export var actor: Actor = null

func _enter_tree() -> void:
	reset_state.call_deferred()

func reset_stats():
	pass

func reset_state():
	pass
