class_name Health
extends Behavior

@export var max_health = 100
@export var fill_health_on_reset: bool = true

var health: float = 0

func reset_stats():
	max_health = actor.stats.get_stat(ActorStats.Stat.HEALTH)
	health = minf(health, max_health)
	super.reset_stats()

func reset_state():
	if fill_health_on_reset:
		health = max_health
	super.reset_state()

func hit(projectile: Node, dmg: float, point: Vector2):
	damage(projectile, dmg, point)

func damage(projectile: Node, dmg: float, _point: Vector2):
	health -= dmg
	if health <= 0:
		kill()

func kill():
	if actor.is_alive():
		actor.kill()

static func get_health(node: Node) -> Health:
	if node is Health:
		return node
	var health_node: Health = null
	for child in node.get_children():
		health_node = get_health(child)
		if health_node != null:
			break
	return health_node
