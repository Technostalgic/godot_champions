class_name Shooting
extends Behavior

signal send_use_weapon_primary()
signal send_use_weapon_secondary()
signal send_start_reload()
signal send_aim_weapon(aim_vec: Vector2)

@export var weapon: Weapon

var is_attacking_primary: bool = false
var is_attacking_secondary: bool = false
var is_reloading: bool = false
var aim: Vector2 = Vector2.ZERO

func _receive_attack_primary_state(attacking: bool):
	is_attacking_primary = attacking
	
func _receive_attack_secondary_state(attacking: bool):
	is_attacking_secondary = attacking
	
func _receive_aim(aim_dir: Vector2):
	aim += aim_dir
	if aim.length_squared() > 1:
		aim = aim.normalized()

func _receive_aim_reset():
	aim.x = 0
	aim.y = 0

func _receive_reload(reloading: bool):
	is_reloading = reloading

func _process(_delta: float):
	if !actor.is_alive():
		return
	send_aim_weapon.emit(aim)

func _physics_process(_delta: float):
	if !actor.is_alive():
		return
	if is_attacking_primary:
		send_use_weapon_primary.emit()
	if is_attacking_secondary:
		send_use_weapon_secondary.emit()
	if is_reloading:
		send_start_reload.emit()
