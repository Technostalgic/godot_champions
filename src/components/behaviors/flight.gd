extends MovementBehavior

@export var air_friction: float = 0.05

var movement: Vector2 = Vector2.ZERO

func _receive_movement(mov: Vector2):
	movement = mov

func _physics_process(delta: float):
	if !actor.is_alive():
		return
	if movement.length_squared() >= 0.1:
		var acc = acceleration_factor * max_speed * _dex_mult
		actor.linear_velocity += movement.normalized() * acc * delta
	#enforce speed limit
	if actor.linear_velocity.length() > max_speed: 
		apply_air_friction(delta)
	
func apply_air_friction(delta: float):
	actor.linear_velocity *= pow(1 - air_friction, delta * 50)
