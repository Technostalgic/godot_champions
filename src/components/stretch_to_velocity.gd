extends Node

@export var actor: RigidBody2D = null
@export var target: Node2D = null
@export var speed_threshold: float = 50
@export var factor: float = 0.02
@export var min_stretch: float = 0.5
@export var max_stretch: float = 2
@export var hide_if_under_thresh: bool = false

func _process(_delta: float):
	var speed := actor.linear_velocity.length()
	if speed <= speed_threshold:
		target.scale.x = min_stretch
		if hide_if_under_thresh:
			target.hide()
		return
	if !target.visible:
		if hide_if_under_thresh:
			target.show()
	var thresh_off := speed - speed_threshold
	var speed_adjusted := thresh_off * factor
	var stretch_dif := max_stretch - min_stretch
	if speed_adjusted > stretch_dif:
		speed_adjusted = stretch_dif
	var total_stretch := min_stretch + speed_adjusted
	target.scale.x = total_stretch
