extends Camera2D

@export var max_zoom: Vector2 = Vector2.ONE * 4
@export var padding: float = 100
@export var move_weight: float = 0.25

var last_pos: Vector2
var last_zoom: Vector2
var destination_position: Vector2
var destination_zoom: Vector2

func _ready():
	zoom = max_zoom

func _physics_process(delta: float):
	calculate_destination()
	go_to_destination(delta)

func go_to_destination(_delta: float):
	
	last_pos = global_position
	last_zoom = zoom
	
	var inv_move_weight = 1 - move_weight
	
	var target_pos = last_pos * inv_move_weight + destination_position * move_weight
	var target_zoom = last_zoom * inv_move_weight + destination_zoom * move_weight
	
	global_position = target_pos
	zoom = target_zoom

func calculate_destination():
	var pmin := Vector2.ZERO
	var pmax := Vector2.ZERO
	var first_iter := true
	for hero in Global.heros:
		if !hero.is_alive():
			continue
		var pos = hero.global_position
		var aim = hero.shooting.aim * 25
		pos += aim
		if first_iter:
			pmin = pos - Vector2.ONE * padding
			pmax = pos + Vector2.ONE * padding
			first_iter = false
			continue
		pmin.x = minf(pmin.x, pos.x - padding)
		pmin.y = minf(pmin.y, pos.y - padding)
		pmax.x = maxf(pmax.x, pos.x + padding)
		pmax.y = maxf(pmax.y, pos.y + padding)
		
	destination_position = (pmin + pmax) * 0.5
	
	var pdif = pmax - pmin
	var view_size = get_viewport_rect().size
	
	var targ_zoom = view_size / pdif
	targ_zoom.x = minf(targ_zoom.x, max_zoom.x)
	targ_zoom.y = minf(targ_zoom.y, max_zoom.y)
	var final_zoom = Vector2.ZERO
	final_zoom.x = minf(targ_zoom.x, targ_zoom.y)
	final_zoom.y = minf(targ_zoom.x, targ_zoom.y)
	
	destination_zoom = final_zoom
