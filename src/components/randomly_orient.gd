extends Node

@export var target: Node2D = null
@export var flip_h: bool = true
@export var flip_v: bool = true
@export var rotate: bool = true

func _enter_tree():
	var scale = target.scale
	if flip_h:
		scale.x = scale.x if randf() >= 0.5 else -scale.x
	if flip_v:
		scale.y = scale.y if randf() >= 0.5 else -scale.y
	target.scale = scale
	if rotate:
		target.rotation = randf() * TAU
	
