extends Node

@export var grounded: GroundDetection = null
@export var laying_gfx: Node2D = null
@export var falling_gfx: Node2D = null

func _process(delta: float) -> void:
	
	var is_laying := grounded.is_on_ground()
	if is_laying:
		var actor := grounded.actor
		var up_dot := (
			actor.global_transform.basis_xform(Vector2.UP).dot(
				-actor.get_last_gravity().normalized()
			)
		)
		if absf(up_dot) > 0.65:
			is_laying = false
	
	if is_laying:
		laying_gfx.show()
		falling_gfx.hide()
	else:
		laying_gfx.hide()
		falling_gfx.show()
