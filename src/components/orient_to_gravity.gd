extends Node

@export var actor: RigidBody2D
@export var max_rotate_speed: float = 2 * PI
@export var orient_after_death: bool = false

func _physics_process(delta):
	if !orient_after_death && actor is Actor:
		if !actor.is_alive():
			return
	var up_axis = -actor.get_last_gravity().normalized()
	if up_axis != Vector2.ZERO:
		var max_delta = max_rotate_speed * delta;
		var fwd_dir := atan2(up_axis.y, up_axis.x) + PI * 0.5
		var ang_dif := angle_difference(actor.global_rotation, fwd_dir)
		if absf(ang_dif) >= PI - PI * 0.25:
			actor.global_rotation += PI
			return
		var ang_delta := clampf(ang_dif, -max_delta, max_delta)
		actor.angular_velocity *= 0.9
		actor.global_rotation += ang_delta;
