class_name GroundDetection
extends Node

signal on_ground_enter()
signal on_ground_exit()

# the amount of time it takes for the behavior to realize its no longer on the ground
@export var actor: DynamicGravityBody = null
@export var max_coyote_time: float = 0.2

# the threshold at which a slope is considered climbable
@export_range(-1, 1) var slope_threshold: float = 0.75

# how long the actor has been in the air for
var _hang_time: float = INF
var _manual_unground: bool = false
var _last_ground_norm: Vector2 = Vector2.ZERO
var _was_on_ground: bool = false

func _ready():
	actor.integrate_forces.connect(_actor_integrate_forces)
	actor.contact_monitor = true
	actor.max_contacts_reported = maxi(actor.max_contacts_reported, 4)

func is_on_ground() -> bool:
	return _hang_time <= max_coyote_time

func was_on_ground() -> bool:
	return _was_on_ground

func set_ungrounded():
	_manual_unground = true
	if is_on_ground():
		on_ground_exit.emit()
		_hang_time = max_coyote_time + 0.01

func _actor_integrate_forces(state: PhysicsDirectBodyState2D):
	_was_on_ground = is_on_ground()
	if _manual_unground:
		_manual_unground = false
		return
	var contact_ct = state.get_contact_count()
	for i in range(0, contact_ct):
		var obj = state.get_contact_collider_object(i)
		if obj is CollisionObject2D:
			var col_intersect = actor.collision_mask & obj.collision_layer
			if col_intersect == 0:
				continue
		var local_norm = state.get_contact_local_normal(i)
		var up_axis = -actor.get_last_gravity().normalized()
		var dot = up_axis.dot(local_norm)
		if dot >= slope_threshold:
			_last_ground_norm = local_norm
			if !is_on_ground():
				on_ground_enter.emit()
			_hang_time = 0

func _physics_process(delta):
	var was_on_grnd = is_on_ground()
	_hang_time += delta
	if was_on_grnd && !is_on_ground():
		on_ground_exit.emit()
