extends Node

@export var stretch_target: Node2D = null
@export var position_target: Node2D = null
@export var projectile_parent: Projectile = null

var last_pos: Vector2 = Vector2.ZERO
var _reset: bool = true

func _ready():
	if projectile_parent != null:
		projectile_parent.on_collide.connect(_on_bullet_collide)

func _exit_tree():
	_reset = true

func _process(_delta: float):
	if _reset:
		reset()
	if position_target.global_position.is_equal_approx(last_pos):
		return
	
	stretch_between(last_pos, position_target.global_position)
	last_pos = position_target.global_position
	print("test_1")

func _on_bullet_collide(_obj: CollisionObject2D, point: Vector2, _normal: Vector2):
	stretch_between(last_pos, point)
	print("test_2")

func stretch_between(to: Vector2, from: Vector2):
	var dif := to - from
	stretch_target.global_position = from
	stretch_target.scale.x = max(dif.length(), stretch_target.scale.y)

func reset():
	last_pos = position_target.global_position
	stretch_target.scale.x = 0
	_reset = false
