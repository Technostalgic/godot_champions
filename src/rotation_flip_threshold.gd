extends Node

@export var compare: Node2D = null
@export var flip_target: Node2D = null
@export var threshold: float = PI * 0.5 + 0.5

var thresh_flipped: bool = false

func _physics_process(_delta: float):
	var wrapped_rot := wrapf(compare.rotation, -PI, PI)
	var thresh_passed := absf(wrapped_rot) > get_threshold()
	#if thresh_flipped:
	#	thresh_passed = absf(wrapped_rot) < get_threshold()
	if thresh_passed: 
		set_flipped(true)
	else:
		set_flipped(false)

func get_threshold() -> float:
	if thresh_flipped:
		return PI - threshold
	return threshold

func set_flipped(flip: bool):
	var scale = flip_target.scale
	if flip:
		scale.y = -absf(scale.x)
	else:
		scale.y = absf(scale.x)
	if scale != flip_target.scale:
		flip_target.scale = scale
		thresh_flipped = flip
	
