class_name Inventory
extends Node

signal item_added(item: Item)
signal item_removed(item: Item)

@export var gold: int = 55
var _items: Array[Item] = []
var _items_dirty: bool = true

static func get_inv_owner(node: Node) -> Actor:
	if node is Actor:
		return node
	var parent = node.get_parent();
	if parent != null:
		return get_inv_owner(parent)
	return null

static func find_inventory_child(node: Node) -> Inventory:
	if node is Inventory:
		return node
	# breadth first recursive search
	var children := node.get_children()
	for child in children:
		if child is Inventory:
			return child
	for child in children:
		var inv: Inventory = Inventory.find_inventory_child(child)
		if inv != null:
			return inv
	return null

func _enter_tree():
	child_entered_tree.connect(_on_child_enter_tree)

func _exit_tree():
	child_entered_tree.disconnect(_on_child_enter_tree)

func _on_child_enter_tree(child: Node):
	if child is Item:
		_items_dirty = true

func get_items() -> Array[Item]:
	if _items_dirty:
		refresh_items()
	return _items

func set_items(new_items: Array[Item]):
	var old_items = _items.duplicate()
	for item in get_items():
		remove_child(item)
	for new_item in new_items:
		if new_item.get_parent() == null:
			add_child(new_item)
		else:
			new_item.reparent(self)
	for item in old_items:
		if is_instance_valid(item) && item.get_parent() == null:
			push_warning("Freeing item ", item)
			item.queue_free()
	_items_dirty = true

func add_item(new_item: Item):
	if new_item is Consumable:
		if new_item.auto_use:
			var actor = Inventory.get_inv_owner(self)
			if actor != null:
				new_item.use(actor)
				return
	if new_item.get_parent() == null:
		add_child(new_item)
	else:
		new_item.reparent(self)
	_items_dirty = true

func refresh_items():
	_items.clear()
	for item in get_children():
		if item is Item:
			_items.push_back(item)
	_items_dirty = false
