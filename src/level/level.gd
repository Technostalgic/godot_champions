class_name Level
extends Node2D

@export var title: String = "Level"
@export var is_safe_room: bool = false
@export var spawn_point_container: Node2D = null
@export var chest_spawn_points: Node2D = null
@export var level_portals: Node2D = null

func _enter_tree() -> void:
	if is_instance_valid(level_portals):
		for portal in level_portals.get_children():
			if portal is LevelPortal:
				portal.enabled = false

func _ready():
	WaveState.wave_completed.connect(_on_wave_completed)

func _process(_delta: float):
	Coin.update_pool_cursor()

func _on_wave_completed():
	create_reward_chests()
	if is_instance_valid(level_portals):
		for portal in level_portals.get_children():
			if portal is LevelPortal:
				portal.enabled = true

func create_reward_chests():
	for hero in Global.heros:
		var chest := Global.chest_prefab.instantiate() as Chest
		chest.target_player_id = hero.player_id
		Global.root_removables.add_child(chest)
		chest.global_position = hero.global_position
