@tool
class_name GravityField
extends Node2D

const SUBFIELD_CONTAINER_NAME: StringName = &"Subfields"
const GRAVITY_DEBUG_COLOR: Color = Color8(179, 126, 0, 0)
const GRAVITY_AFFECTED_LAYERS: int = (
	GlobalClass.CollisionLayer.HEROS |
	GlobalClass.CollisionLayer.MOBS |
	GlobalClass.CollisionLayer.PROPS |
	GlobalClass.CollisionLayer.GIBS |
	GlobalClass.CollisionLayer.ETHER_ITEMS
)

# a place to store all bodies being attracted to the attractor
var attracting_bodies: Array[DynamicGravityBody] = []

# the surface of the planet
@export var surface: CollisionObject2D = null
@export var gravity: Area2D = null

# how strong the gravity pulls bodies toward it
@export var gravity_strength: float = 50
@export var gravity_priority: int = 1

@export_group("Subfield Generation")

## How far outwards from the surface that each sub field will expand
@export var subfield_range: float = 10
@export var generate_subfields: bool :
	set(_value):
		create_subfields()

## Which node should be a parent for the generated subfield nodes
@export var subfield_container: Node2D = null
@export var subfields: Array[GravitySubfield] = []

## Dictionary<DynamicPhysicsBody, Array[GravitySubfield]>
var _body_subfield_map: Dictionary = {}

func _ready():
	if is_instance_valid(gravity):
		gravity.body_entered.connect(_enter_gravity)
		gravity.body_exited.connect(_exit_gravity)

func _physics_process(_delta):
	if surface == null: 
		return
	if subfields.size() <= 0:
		_handle_classic_attraction()
	else:
		_handle_subfield_attraction()

func _enter_gravity(node: Node2D):
	if node is DynamicGravityBody:
		attracting_bodies.push_back(node)

func _exit_gravity(node: Node2D):
	if node is DynamicGravityBody:
		var index = attracting_bodies.find(node)
		if index >= 0:
			attracting_bodies.remove_at(index)

func _handle_classic_attraction():
	for body in attracting_bodies: 
		if body is DynamicGravityBody:
			var atr_pt := GravityField.project_on_collider(surface, body.global_position)
			var atr_dif := atr_pt - body.global_position
			var grav_dir: Vector2 = atr_dif.normalized()
			body.set_gravity(self, grav_dir * gravity_strength, atr_dif.length_squared())

func _handle_subfield_attraction():
	for body in _body_subfield_map:
		if body is DynamicGravityBody:
			var body_subfields := _body_subfield_map[body] as Array[GravitySubfield]
			if body_subfields.size() <= 0:
				continue
			var body_pos := body.global_position as Vector2
			var body_loc_pos := surface.global_transform.affine_inverse() * body_pos
			var closest_pt := (
				surface.global_transform * 
				GravityField.project_on_subfields(body_subfields, body_loc_pos)
			)
			var grav_dir := (closest_pt - body_pos).normalized()
			body.set_gravity(
				self, 
				grav_dir * gravity_strength, 
				closest_pt.distance_squared_to(body_pos)
			)

func _create_subfield(
	shape: Shape2D,
	shape_node: Node2D, 
	owner_id: int,
	shape_id: int,
	trans: Transform2D
) -> void:
	var sf := GravitySubfield.create(shape, shape_node, owner_id, shape_id);
	sf.field_parent = self
	sf.monitoring = true
	subfields.push_back(sf)
	subfield_container.add_child(sf)
	sf.owner = owner
	var sf_col_shape := CollisionShape2D.new()
	sf_col_shape.transform = trans
	sf_col_shape.shape = GravityField.expand_shape(shape, subfield_range, 1)
	sf.add_child(sf_col_shape)
	sf_col_shape.owner = sf.owner
	sf_col_shape.debug_color = GRAVITY_DEBUG_COLOR

func create_subfields() -> void:
	
	# clear all previous subfields
	for sf in subfields:
		sf.queue_free()
	subfields = []
	
	# find or create container if not specified
	if !is_instance_valid(subfield_container):
		subfield_container = get_node_or_null("./" + SUBFIELD_CONTAINER_NAME)
	if !is_instance_valid(subfield_container):
		subfield_container = Node2D.new()
		subfield_container.name = SUBFIELD_CONTAINER_NAME
		add_child(subfield_container, false)
		subfield_container.owner = owner
	subfield_container.hide()
	
	# Iterate through each collision shape and create a subfield from it
	var owner_ids := surface.get_shape_owners()
	for id in owner_ids:
		var shape_ct := surface.shape_owner_get_shape_count(id)
		var shape_owner := surface.shape_owner_get_owner(id) as Node2D
		var trans := surface.shape_owner_get_transform(id)
		for i in shape_ct:
			var shape = surface.shape_owner_get_shape(id, i)
			_create_subfield(shape, shape_owner, id, i, trans)

func register_body_subfield(body: DynamicGravityBody, subfield: GravitySubfield) -> void:
	if !_body_subfield_map.has(body):
		_body_subfield_map[body] = [subfield] as Array[GravitySubfield]
	else:
		var arr := _body_subfield_map[body] as Array[GravitySubfield]
		arr.push_back(subfield)

func unregister_body_subfield(body, subfield: GravitySubfield) -> void:
	if _body_subfield_map.has(body):
		var arr := _body_subfield_map[body] as Array[GravitySubfield]
		if arr.size() <= 1:
			if arr.has(subfield):
				_body_subfield_map.erase(body)
		else:
			arr.erase(subfield)

static func project_on_collider(
	collider_b: CollisionObject2D, 
	point: Vector2,
	trans: Transform2D = collider_b.global_transform
) -> Vector2:
	var owners := collider_b.get_shape_owners()
	var closest_dist := INF
	var closest_pt := Vector2.ZERO
	var col_loc_point := trans.affine_inverse() * point
	for owner_id in owners:
		var shape_ct := collider_b.shape_owner_get_shape_count(owner_id)
		# iterate through each shape in the collder and get closest projection pt
		for i in shape_ct:
			var shape_trans := collider_b.shape_owner_get_transform(owner_id)
			var local_pt := shape_trans.affine_inverse() * col_loc_point
			var shape := collider_b.shape_owner_get_shape(owner_id, i)
			var iter_pt := project_on_shape(shape, local_pt)
			var iter_dist := iter_pt.distance_squared_to(local_pt)
			if iter_dist < closest_dist:
				closest_pt = shape_trans * iter_pt
				closest_dist = iter_dist
	return trans * closest_pt

static func project_on_shape(shape: Shape2D, local_pt: Vector2) -> Vector2:
	
	if shape is ConvexPolygonShape2D:
		var vert_ct:int = shape.points.size()
		var closest_dist := INF
		var closest_pt := Vector2.ZERO
		for i in range(0, vert_ct):
			var cur_vert: Vector2 = shape.points[i]
			var nex_vert: Vector2 = shape.points[(i + 1) % vert_ct]
			var pt = Geometry2D.get_closest_point_to_segment(
				local_pt,
				cur_vert,
				nex_vert)
			var dist := pt.distance_squared_to(local_pt)
			if dist < closest_dist:
				closest_pt = pt
				closest_dist = dist
		return closest_pt
		
	if shape is CircleShape2D:
		if local_pt.length_squared() <= shape.radius * shape.radius:
			return local_pt        
		return local_pt.normalized() * shape.radius
		
	if shape is RectangleShape2D:
		var rect := shape.get_rect()
		if rect.has_point(local_pt):
			return local_pt
		var mn := rect.position
		var mx := rect.position + rect.size
		return Vector2(clampf(local_pt.x, mn.x, mx.x), clampf(local_pt.y, mn.y, mx.y))
		
	if shape is CapsuleShape2D:
		var top = Vector2.UP * (shape.height * 0.5 - shape.radius)
		var bottom = Vector2.UP * (shape.height * -0.5 + shape.radius)
		var proj_pt = Geometry2D.get_closest_point_to_segment(local_pt, top, bottom)
		var proj_dif = local_pt - proj_pt
		if proj_dif.length_squared() <= shape.radius * shape.radius:
			return local_pt
		return proj_pt + proj_dif.normalized() * shape.radius
	
	push_error(str("shape type '", shape, "' is not supported for point projection"))
	return Vector2.ZERO

static func project_on_subfields(
	subfield_arr: Array[GravitySubfield], 
	surface_local_pt: Vector2
) -> Vector2:
	var closest_pt := Vector2.ZERO
	var closest_dist_sq := INF
	for sf in subfield_arr:
		var shape_trans := sf.field_parent.surface.shape_owner_get_transform(sf.owner_id)
		var proj_pt := sf.field_parent.surface.global_transform * (shape_trans * project_on_shape(
			sf.field_parent.surface.shape_owner_get_shape(sf.owner_id, sf.shape_id),
			shape_trans.affine_inverse() * surface_local_pt 
		))
		var proj_dist_sq := surface_local_pt.distance_squared_to(proj_pt) as float
		if proj_dist_sq < closest_dist_sq:
			closest_pt = proj_pt
			closest_dist_sq = proj_dist_sq
	return closest_pt

static func get_gravity_at_pos(pos: Vector2) -> Vector2:
	push_warning("not implemented")
	return Vector2.ZERO

static func expand_shape(shape: Shape2D, radius: float, corner_detail: int = 0) -> Shape2D:
	var expanded_shape: Shape2D = null
	if shape is ConvexPolygonShape2D:
		var poly := ConvexPolygonShape2D.new()
		var shape_pt_ct := shape.points.size() as int
		var poly_verts: PackedVector2Array = []
		for i in shape_pt_ct:
			var i_prev := wrapi(i - 1, 0, shape_pt_ct)
			var i_next := wrapi(i + 1, 0, shape_pt_ct)
			var prev_norm := (shape.points[i] - shape.points[i_prev] as Vector2).normalized().rotated(PI * -0.5)
			var next_norm := (shape.points[i_next] - shape.points[i] as Vector2).normalized().rotated(PI * -0.5)
			poly_verts.push_back(shape.points[i] + prev_norm * radius)
			for det in corner_detail:
				var delta_norm := prev_norm.lerp(next_norm, (det + 1.0) / (corner_detail + 1)).normalized()
				poly_verts.push_back(shape.points[i] + delta_norm * radius)
			poly_verts.push_back(shape.points[i] + next_norm * radius)
		poly.points = poly_verts
		expanded_shape = poly
	elif shape is CircleShape2D || shape is CapsuleShape2D:
		expanded_shape = shape.duplicate()
		expanded_shape.radius += radius
	elif shape is RectangleShape2D:
		expanded_shape = shape.duplicate()
		expanded_shape.size.x += radius * 2
		expanded_shape.size.y += radius * 2
	if expanded_shape == null:
		push_error("unable to exand shape ", shape)
	return expanded_shape

static func expand_points(points: PackedVector2Array, radius: float, corner_detail: int = 0) -> PackedVector2Array:
	var shape_pt_ct := points.size() as int
	var poly_verts: PackedVector2Array = []
	for i in shape_pt_ct:
		var i_prev := wrapi(i - 1, 0, shape_pt_ct)
		var i_next := wrapi(i + 1, 0, shape_pt_ct)
		var prev_norm := (points[i] - points[i_prev]).normalized().rotated(PI * -0.5)
		var next_norm := (points[i_next] - points[i]).normalized().rotated(PI * -0.5)
		var ang_dif = prev_norm.angle_to(next_norm)
		if ang_dif >= 0:
			poly_verts.push_back(points[i] + prev_norm * radius)
			for det in corner_detail:
				var delta_norm := prev_norm.lerp(next_norm, (det + 1.0) / (corner_detail + 1)).normalized()
				poly_verts.push_back(points[i] + delta_norm * radius)
			poly_verts.push_back(points[i] + next_norm * radius)
		else:
			poly_verts.push_back(points[i] + (prev_norm + next_norm) * radius)
		
	return poly_verts
