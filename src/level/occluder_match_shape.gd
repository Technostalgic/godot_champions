@tool
class_name OccluderMatchShape
extends LightOccluder2D

const INVERT_EDGE_EXPANSION: float = 10000

@export var collision_shape: CollisionPolygon2D = null :
	set(value):
		collision_shape = value
		if Engine.is_editor_hint():
			match_shape()
@export var shape: SS2D_Shape = null :
	set(value):
		if shape != null:
			if shape.points_modified.is_connected(match_shape):
				shape.points_modified.disconnect(match_shape)
		if value != null:
			if !value.points_modified.is_connected(match_shape):
				value.points_modified.connect(match_shape)
		shape = value
		if Engine.is_editor_hint():
			match_shape()

@export var inverted: bool = false :
	set(value):
		inverted = value
		if Engine.is_editor_hint():
			match_shape()

func _ready() -> void:
	if Engine.is_editor_hint():
		z_index = -1000
		z_as_relative = false
		if shape == null && collision_shape == null:
			var parent = get_parent()
			if parent is SS2D_Shape:
				shape = parent
			elif parent is CollisionPolygon2D:
				collision_shape = parent

func match_shape() -> void:
	if shape == null && collision_shape == null:
		occluder = null
		return
	occluder = OccluderPolygon2D.new()
	if shape != null:
		occluder.polygon = shape.generate_collision_points()
	elif collision_shape != null:
		occluder.polygon =  collision_shape.polygon
	if inverted:
		var iters := 0 as int
		var points := [] as PackedVector2Array
		var corners := [
				Vector2(-INVERT_EDGE_EXPANSION, -INVERT_EDGE_EXPANSION),
				Vector2(INVERT_EDGE_EXPANSION, -INVERT_EDGE_EXPANSION),
				Vector2(INVERT_EDGE_EXPANSION, INVERT_EDGE_EXPANSION),
				Vector2(-INVERT_EDGE_EXPANSION, INVERT_EDGE_EXPANSION),
			] as PackedVector2Array
		print("attempting to invert polygon..")
		while iters < 4 && 	Geometry2D.triangulate_polygon(points).size() <= 0:
			print("failed ", iters, " times")
			points = occluder.polygon.duplicate()
			var points_expanded := [] as PackedVector2Array
			for i in corners.size():
				var ind := wrapi(i + iters, 0, corners.size())
				points_expanded.push_back(corners[ind])
			var count := points_expanded.size()
			points.push_back(points[0])
			var insert_ind := points.size()
			for i in count:
				var index = count - i - 1
				points.push_back(points_expanded[index])
			points.insert(insert_ind, points_expanded[0])
			iters += 1
		occluder.polygon = points
