class_name Trigger
extends Node

signal just_enabled()
signal just_disabled()
signal condition_met()

@export var enabled: bool = true :
	set(value):
		if value && !enabled:
			just_enabled.emit.call_deferred()
		elif !value && enabled:
			just_disabled.emit.call_deferred()
		enabled = value
@export var area: Area2D = null
@export var condition: TriggerCondition = null

var actions: Array[TriggerAction] = []

func _ready() -> void:
	condition = condition.duplicate()
	condition.parent_trigger = self
	condition.initialize()

func _physics_process(delta: float) -> void:
	condition.evaluate(delta)

func meet_condition():
	for action in actions:
		action.do_action()
	condition_met.emit()

func send_action_paramater(paramater: Variant) -> void:
	var i: int = 0
	for action in actions:
		if !is_instance_valid(action):
			actions.remove_at(i)
			continue
		action.receive_paramater(paramater)
		i += 1
