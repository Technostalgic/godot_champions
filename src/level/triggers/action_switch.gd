extends TriggerAction

@export var sub_actions: Array[TriggerAction] = []
@export var curent_action_index: int = 0

func do_action() -> void:
	super.do_action()
	sub_actions[curent_action_index].do_action()
	curent_action_index = wrapi(curent_action_index + 1, 0, sub_actions.size())
