extends TriggerAction

## The active state to set each target trigger to
@export var active_mode: bool = false
@export var target_triggers: Array[Trigger] = []

## If true, this action is removed when used for the first time
@export var auto_remove: bool = true

func do_action() -> void:
	for trigger in target_triggers:
		trigger.enabled = active_mode
	if auto_remove && parent_trigger != null:
			parent_trigger.actions.erase(self)
			queue_free()
