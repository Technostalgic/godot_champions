extends TriggerAction

@export var motion: ActionMotion = null
@export var speed_factor: float = 1

var _is_active: bool = false

func _process(delta: float) -> void:
	if _is_active:
		_handle_motion(delta)

func _handle_motion(delta: float) -> void:
	motion._handle_movement(-delta * speed_factor)
	if motion._anim_delta <= 0:
		motion.target_node.position = motion._from_pos
		_is_active = false
		motion._anim_delta = -1

func do_action() -> void:
	super.do_action()
	_is_active = true
