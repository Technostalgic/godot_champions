class_name ActionMotion
extends TriggerAction

@export var target_node: Node2D = null
@export var offset: Vector2 = Vector2.ZERO

## The total time it takes to move the object
@export var time: float = 1

var _from_pos: Vector2 = Vector2.ZERO
var _anim_delta: float = -1
var _is_active: bool = false

func _process(delta: float) -> void:
	if _is_active:
		_handle_movement(delta)

func _handle_movement(delta: float) -> void:
	_anim_delta += delta / time
	if _anim_delta >= 1:
		target_node.position = _from_pos + offset
		_anim_delta = 1
		_is_active = false
		return
	var off := _from_pos.lerp(offset, _anim_delta)
	target_node.position = off

func do_action() -> void:
	super.do_action()
	_is_active = true
	_from_pos = target_node.position
	_anim_delta = 0
