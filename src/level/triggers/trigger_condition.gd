class_name TriggerCondition
extends Resource

@export var reset_time: float = 1

var parent_trigger: Trigger = null
var condition_met: bool = false
var _reset_timer: float = 0
var _queued_effectors: Array[Node2D] = []

func _body_entered(body: Node2D) -> void:
	if parent_trigger.enabled:
		_body_triggered(body)
	else:
		_queued_effectors.push_back(body)

func _body_exited(body: Node2D) -> void:
	if !parent_trigger.enabled:
		_queued_effectors.erase(body)

func _body_triggered(_body: Node2D) -> void:
	pass

func _just_enabled() -> void:
	for body in _queued_effectors:
		_body_triggered(body)

func initialize() -> void:
	condition_met = false
	if parent_trigger.area != null:
		parent_trigger.area.body_entered.connect(_body_entered)
		parent_trigger.area.body_exited.connect(_body_exited)
	parent_trigger.just_enabled.connect(_just_enabled)

func evaluate(delta: float) -> void:
	if parent_trigger.enabled:
		handle_reset_timer(delta)

func handle_reset_timer(delta: float) -> void:
	if reset_time >= 0 && _reset_timer >= 0:
		_reset_timer -= delta
		if _reset_timer <= 0:
			reset()
			_reset_timer = -1

func meet_condition() -> void:
	if !parent_trigger.enabled:
		return
	if !condition_met:
		parent_trigger.meet_condition()
		condition_met = true
		_reset_timer = reset_time

func reset() -> void:
	condition_met = false
