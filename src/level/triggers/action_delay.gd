extends TriggerAction

@export var delay_seconds: float = 1
@export var sub_actions: Array[TriggerAction] = []

var _countdown: float = -1

func _physics_process(delta: float) -> void:
	if _countdown >= 0:
		_handle_countdown(delta)

func _handle_countdown(delta: float) -> void:
	_countdown -= delta
	if _countdown <= 0:
		_countdown = -1
		for action in sub_actions:
			action.do_action()

func receive_parameter(param: Variant) -> void:
	for action in sub_actions:
		action.receive_paramater(param)

func do_action() -> void:
	_countdown = delay_seconds
