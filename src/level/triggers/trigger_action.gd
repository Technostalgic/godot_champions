class_name TriggerAction
extends Node

var parent_trigger: Trigger = null

func _ready():
	var parent := get_parent()
	if parent is Trigger:
		parent_trigger = parent
		parent.actions.push_back(self)

func do_action() -> void:
	pass

func receive_paramater(paramater: Variant) -> void:
	pass
