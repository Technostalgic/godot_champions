class_name HeroExited
extends TriggerCondition

func _body_exited(body: Node2D) -> void:
	super._body_entered(body)
	if parent_trigger.enabled:
		if body is Hero:
			parent_trigger.send_action_paramater(body)
			meet_condition()
