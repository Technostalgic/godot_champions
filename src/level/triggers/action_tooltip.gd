class_name TriggerActionTooltip
extends TriggerAction

enum TooltipID {
	SHORT_JUMP,
	HIGH_JUMP,
	WALL_WALK,
	JUMP_CEILING,
	INTERACT,
	SHOOT_DOOR_CONTROL,
	WEAPON_MOD,
	KILL_ENEMY,
	INVENTORY,
	ALT_FIRE,
	WEP_ENERGY,
	WEP_ALT_MODE,
	WEP_CARRY_MODS,
	WEP_AMMO,
	WEP_POWER,
	CUSTOM,
}

static func get_tooltip_string(id: TooltipID, _player_id: int) -> String:
	match(id):
		TooltipID.SHORT_JUMP:
			return "Breifly tap 'Space' to jump a short height"
		TooltipID.HIGH_JUMP:
			return "Press and hold 'Space' to jump high"
		TooltipID.WALL_WALK:
			return "Approach the wall and hold 'W' to walk upwards on it"
		TooltipID.JUMP_CEILING:
			return "Avoid the boost pads by jumping onto the opposite wall"
		TooltipID.INTERACT:
			return "Press 'E' to interact"
		TooltipID.SHOOT_DOOR_CONTROL:
			return "Shoot the door control on the ceiling, with 'Mouse' and 'Left Click'"
		TooltipID.WEAPON_MOD:
			return "Equip a weapon mod by holding 'E' while on it"
	return ""

@export var tooltip: TooltipID = TooltipID.SHORT_JUMP
@export var custom_tooltip: String = ""
@export var hide_tooltip: bool = false
@export var tooltip_follow_trigger_node: bool = true
@export var tooltip_offset_direction: Vector2 = Vector2.ZERO
@export var tooltip_offset_position: Vector2 = Vector2.ZERO

var node_param: Node2D = null

func receive_paramater(paramater: Variant) -> void:
	super.receive_paramater(paramater)
	if paramater is Node2D:
		node_param = paramater

func do_action() -> void:
	super.do_action()
	var text := get_tooltip_string(tooltip, 0) if !tooltip == TooltipID.CUSTOM else custom_tooltip
	
	var id := -1
	if node_param is Hero:
		id = node_param.player_id
	
	if hide_tooltip:
		GameTooltipManager.hide_tracked_tooltip(id)
	else:
		var info := GameTooltipManager.show_tracked_tooltip(id, text, 48)
		if tooltip_follow_trigger_node:
			info.follow_node = node_param
		info.offset_dir = tooltip_offset_direction
		info.offset_pos = tooltip_offset_position
	
	node_param = null
