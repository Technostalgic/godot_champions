class_name HeroEntered
extends TriggerCondition

func _body_triggered(body: Node2D) -> void:
	if body is Hero:
		parent_trigger.send_action_paramater(body)
		meet_condition()
