extends TriggerAction

@export var target_nodes: Array[Node] = []

func do_action():
	for node in target_nodes:
		node.queue_free()
	queue_free()
