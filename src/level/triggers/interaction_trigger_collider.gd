class_name InteractionTriggerCollider
extends Area2D

@export var parent_interactible: Interactible
@export var parent_trigger: Trigger

func trigger(triggerer: Actor = null):
	if parent_interactible != null:
		parent_interactible.interact(triggerer)
	if parent_trigger != null:
		parent_trigger.condition.meet_condition()
