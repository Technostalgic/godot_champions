class_name HeroHasMods
extends TriggerCondition

## The amount of weapon mods any player must have for this condition to be met
@export var min_mod_count: int = 1

func evaluate(delta: float) -> void:
	super.evaluate(delta)
	if !parent_trigger.enabled || condition_met:
		return
	for hero in Global.heros:
		var wep := hero.get_equipped_weapon() as ModableWeapon
		var mod_count: int = 0
		if is_instance_valid(wep):
			mod_count += wep.mod_container_1.mods.size() + wep.mod_container_2.mods.size()
		if mod_count >= min_mod_count:
			meet_condition()
