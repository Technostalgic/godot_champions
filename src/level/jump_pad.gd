extends Area2D

@export var strength: float = 250

var effectors: Array[RigidBody2D] = []

func _ready():
	body_entered.connect(_on_body_entered)
	body_exited.connect(_on_body_exited)

func _physics_process(_delta: float):
	var i: int = 0
	for eff in effectors:
		if !is_instance_valid(eff):
			effectors.remove_at(i)
			continue
		eff.angular_velocity = 0
		eff.linear_velocity = Vector2.from_angle(global_rotation) * strength
		i += 1
		if eff is Actor:
			if is_instance_valid(eff.grounded):
				eff.grounded.set_ungrounded()

func _on_body_entered(body: Node2D):
	if body is RigidBody2D:
		effectors.push_back(body)

func _on_body_exited(body: Node2D):
	if body is RigidBody2D:
		effectors.erase(body)
