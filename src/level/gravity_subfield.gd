class_name GravitySubfield
extends Area2D

static func create(shape: Shape2D, shape_node: Node2D, owner_id: int, shape_id: int) -> GravitySubfield:
	var sf := GravitySubfield.new()
	sf.shape = shape
	sf.shape_node = shape_node
	sf.owner_id = owner_id
	sf.shape_id = shape_id
	sf.collision_layer = GlobalClass.CollisionLayer.TERRAIN
	sf.collision_mask = GravityField.GRAVITY_AFFECTED_LAYERS
	return sf

@export var field_parent: GravityField = null
@export var shape: Shape2D = null
@export var shape_node: Node2D = null
@export var owner_id: int = -1
@export var shape_id: int = -1

func _ready() -> void:
	body_entered.connect(_on_body_entered)
	body_exited.connect(_on_body_exited)

func _on_body_entered(body: Node2D) -> void:
	if body is DynamicGravityBody:
		field_parent.register_body_subfield(body, self)

func _on_body_exited(body: Node2D) -> void:
	if body is DynamicGravityBody:
		field_parent.unregister_body_subfield(body, self)
