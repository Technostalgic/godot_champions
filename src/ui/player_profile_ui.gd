class_name PlayerProfileUI
extends Control

@export var title: Label
@export var color_disp: Control
@export var switch_controller_btn: Button
@export var remove_plr_btn: Button

var profile: GlobalClass.PlayerConfig = null

func _ready():
	switch_controller_btn.pressed.connect(_on_controller_switch)
	remove_plr_btn.pressed.connect(_on_remove_player)
	display_profile()

func _on_controller_switch():
	Global.session_params.switch_player_controller(profile)

func _on_remove_player():
	if profile == null:
		Global.create_new_player()
	else:
		Global.remove_last_player()

func display_profile():
	
	if profile == null:
		title.text = "Add Player"
		color_disp.hide()
		switch_controller_btn.hide()
		return
	
	title.text = str("Player ", profile.player_id)
	switch_controller_btn.text = str("Ctrl ", profile.controller_id)
	if profile.controller_id < 0:
		switch_controller_btn.text = "M + KB"
	color_disp.modulate = GlobalClass.PLAYER_COLORS[profile.color]
	
	color_disp.show()
	switch_controller_btn.show()
