class_name Reticle
extends Control

@export var weapon_slot: WeaponSlot = null
@export var energy_indicator: ProgressBar = null
@export var ammo_indicator: ProgressBar = null
@export var shot_indicator_top: ProgressBar = null
@export var shot_indicator_bottom: ProgressBar = null
@export var shot_cursor: Control = null

func _physics_process(_delta: float):
	if !is_instance_valid(weapon_slot):
		hide()
		return
	update_indicators()
	position_reticle()

func position_reticle():
	var wep = weapon_slot.get_equipped_weapons()[0]
	if !is_instance_valid(wep):
		hide()
		return
	if wep.wep_owner is Hero:
		if !(wep.wep_owner as Hero).is_alive():
			hide()
			return
	if !visible:
		show()
		
	rotation = wep.global_rotation
	var dist: float = 100
	var off = Vector2.from_angle(rotation) * dist
	var alpha: float = 1
	if wep is BasicWeapon:
		off = wep.aim * dist
		alpha = wep.aim.length()
	
	global_position = (
		Global.camera.get_viewport_transform() 
		* wep.get_attack_target().global_position 
		+ off
	)
	var alpha_sqrt = sqrt(alpha)
	modulate.a = alpha * 1.5 - 0.34
	scale = Vector2.ONE * alpha_sqrt

func update_indicators():
	var wep = weapon_slot.get_equipped_weapons()[0]
	if !is_instance_valid(wep):
		return
	
	var energy_percent = 0
	var ammo_percent = 0
	var shot_percent = 0
	if wep is BasicWeapon:
		var stats = wep.get_weapon_stats()
		if wep is ModableWeapon:
			energy_percent = wep.current_energy / stats.max_energy
		if wep.is_reloading:
			ammo_percent = 1 - (wep.reload_cooldown / stats.reload_time)
		else:
			ammo_percent = wep.ammo_in_magazine / stats.magazine_size
		
		var shot_stats = wep.get_shot_stats()
		if (!wep.is_reloading && (wep.trigger_charge > 0) && 
		shot_stats.fire_mode == Stats.FireMode.AUTOMATIC):
			shot_percent = 1
		if shot_stats.fire_delay > 0 && wep.trigger_charge <= shot_stats.fire_delay:
			shot_percent = wep.trigger_charge / shot_stats.fire_delay
		if wep.fire_cooldown > 0:
			shot_percent = wep.fire_cooldown / shot_stats.cooldown
		shot_percent = minf(1, shot_percent)
	
	energy_indicator.value = energy_percent
	ammo_indicator.value = ammo_percent
	shot_indicator_top.value = shot_percent
	shot_indicator_bottom.value = shot_percent
