class_name WaveProgressBar
extends Node

@export var reward_selection_prefab: PackedScene = null
@export var wave_label: Label = null
@export var complete_popup: Control = null
@export var next_wave_button: Button = null
@export var bar_anchors: Container = null
@export var reward_selection_container: Container = null

var players: Array[Hero] = []

func _ready():
	update_wave_text()
	WaveState.wave_completed.connect(update_wave_text)
	for child in Global.root_2d_world.get_children():
		if child is Hero:
			players.push_back(child)

func _process(_delta):
	update_wave_progress()

func update_wave_text():
	wave_label.text = str("Wave ", WaveState.wave_number)

func update_wave_progress():
	var percent_complete := 0
	bar_anchors.anchor_right = clampf(percent_complete, 0, 1)
