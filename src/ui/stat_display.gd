class_name StatDisplay
extends Container

@export var stat_name: Label = null
@export var stat_value: Label = null

func set_stat_value(text: String, value: float):
	stat_name.text = text
	stat_value.text = String.num(value, 2)

func set_stat_str(text: String, value: String):
	stat_name.text = text
	stat_value.text = value
