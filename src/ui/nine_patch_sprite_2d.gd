@tool
class_name NinePatchSprite2D
extends Node2D

@export var texture: Texture2D = null :
	set(value):
		texture = value
		clear_sprites()
		arrange_sprites()
@export var texture_scale: float = 1 :
	set(value):
		texture_scale = value
		clear_sprites()
		arrange_sprites()
@export var size: Vector2 = Vector2(50, 50) :
	set(value):
		size = value
		arrange_sprites()
@export var margin_left: int = 0 :
	set(value):
		margin_left = value
		clear_sprites()
		arrange_sprites()
@export var margin_right: int = 0 :
	set(value):
		margin_right = value
		clear_sprites()
		arrange_sprites()
@export var margin_top: int = 0 :
	set(value):
		margin_top = value
		clear_sprites()
		arrange_sprites()
@export var margin_bottom: int = 0 :
	set(value):
		margin_bottom = value
		clear_sprites()
		arrange_sprites()

var columns: int = 1
var rows: int = 1
var middle_width: int :
	get: return texture.get_size().x as int - (margin_left + margin_right)
var middle_height: int :
	get: return texture.get_size().y as int - (margin_top + margin_bottom)

var _sprites: Array[Sprite2D] = []
@export_storage var sprites: Array[Sprite2D] = [] :
	get:
		if _sprites.size() <= 0:
			_initialize_sprites()
		return _sprites

func _initialize_sprites():
	clear_sprites()
	var i = 0
	rows = 1
	if margin_top > 0:
		rows += 1
	if margin_bottom > 0:
		rows += 1
	columns = 1
	if margin_left > 0:
		columns += 1
	if margin_right > 0:
		columns += 1
	while i < 9:
		var rect = get_region_rect(i)
		i += 1
		if rect.size.x <= 0 || rect.size.y <= 0:
			_sprites.push_back(null)
			continue
		var sprt := Sprite2D.new()
		sprt.centered = false
		sprt.texture = texture
		sprt.region_enabled = true
		sprt.region_rect = rect
		add_child(sprt)
		_sprites.push_back(sprt)

func clear_sprites():
	for sprite in _sprites:
		if sprite == null:
			continue
		sprite.queue_free()
	_sprites.clear()

func get_region_rect(spr_index: int) -> Rect2:
	var rect := Rect2(Vector2.ZERO, Vector2.ZERO)
	var rect_size = texture.get_size()
	
	# find y pos and height
	if spr_index < 3: # top row
		rect.position.y = 0
		rect.size.y = margin_top
	elif spr_index >= 6: # bottom row
		rect.position.y = rect_size.y - margin_top
		rect.size.y = margin_bottom
	else: # middle row
		rect.position.y = margin_top
		rect.size.y = middle_height
	
	# find x pos and width
	if spr_index % 3 < 1: # left column
		rect.position.x = 0
		rect.size.x = margin_left
	elif spr_index % 3 > 1: # right column
		rect.position.x = rect_size.x - margin_right
		rect.size.x = margin_right
	else: # middle column
		rect.position.x = margin_left
		rect.size.x = middle_width
	
	return rect

func arrange_sprites():
	var i: int = 0
	for sprite in sprites:
		if sprite == null:
			i += 1
			continue
		
		var column := i % 3
		var row := i / 3
		i += 1
		
		if column == 0:
			sprite.scale.x = texture_scale
			sprite.position.x = -size.x * 0.5
		elif column > 1:
			sprite.scale.x = texture_scale
			sprite.position.x = size.x * 0.5 - margin_right * texture_scale
		else:
			sprite.scale.x = (size.x - ((margin_left + margin_right) * texture_scale)) / middle_width
			sprite.position.x = -size.x * 0.5 + margin_left * texture_scale
		
		if row == 0:
			sprite.scale.y = texture_scale
			sprite.position.y = -size.y * 0.5
		elif row > 1:
			sprite.scale.y = texture_scale
			sprite.position.y = size.y * 0.5 - margin_bottom * texture_scale
		else:
			sprite.scale.y = (size.y - ((margin_top + margin_bottom) * texture_scale)) / middle_height
			sprite.position.y = -size.y * 0.5 + margin_top * texture_scale
