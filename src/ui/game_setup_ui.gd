class_name GameSetupUI
extends Control

@export var player_profile_prefab: PackedScene = null
@export var player_profile_container: Container = null
@export var start_game_btn: Button = null

func _ready():
	Global.players_changed.connect(_on_player_added)
	start_game_btn.pressed.connect(_on_game_start)
	create_player_profile_uis()

func _on_player_added():
	create_player_profile_uis()
	
func _on_game_start():
	Global.start_game_session()

func create_player_profile_uis():
	
	# remove all children
	for child in player_profile_container.get_children():
		child.queue_free()
	
	for prof in Global.session_params.players:
		var prof_ui = player_profile_prefab.instantiate() as PlayerProfileUI 
		prof_ui.profile = prof
		player_profile_container.add_child(prof_ui)
	
	if Global.session_params.players.size() < 7:
		player_profile_container.add_child(player_profile_prefab.instantiate() as PlayerProfileUI)
