class_name PlayerFocusedControl
extends Node

@export var player_id: int = -1

# returns what player the specified control belongs to
static func get_focus_id(control: Control) -> int:
	if !is_instance_valid(control):
		return 99
	if control.get_child_count() > 0:
		var child = control.get_child(0)
		if child is PlayerFocusedControl:
			return child.player_id
	return -1

# set the player id that the control belongs to, creating a playerfocused control if it 
# doesn't have one
static func set_focus_id(control: Control, id: int):
	var was_set := false
	if control.get_child_count() > 0:
		var child = control.get_child(0)
		if child is PlayerFocusedControl:
			child.player_id = id
			was_set = true
	
	# if no player focus control found, create one
	if !was_set && id >= 0:
		var focus_ctrl = PlayerFocusedControl.new()
		focus_ctrl.player_id = id
		control.add_child(focus_ctrl)
		control.move_child(focus_ctrl, 0)

static func find_compatible_button(parent_node: Node, id: int) -> BaseButton:
	if parent_node is BaseButton:
		var targ_id := get_focus_id(parent_node)
		if targ_id < 0 || targ_id == id:
			return parent_node
	for child in parent_node.get_children():
		if child is Control:
			if !child.is_visible_in_tree():
				continue
		var but := find_compatible_button(child, id)
		if but != null:
			return but
	return null

static func find_valid_focus_neighbor(control: Control, id: int, side: int) -> Control:
	var targ = control.find_valid_focus_neighbor(side)
	var targ_id = get_focus_id(targ)
	var iters: int = 0
	while targ_id > 0 && targ_id != id:
		targ = control.find_valid_focus_neighbor(side)
		targ_id = get_focus_id(targ)
		iters += 1
		if iters > 15:
			break
	
	if targ_id < 0 || targ_id == id:
		return targ
	
	return null

static func set_control_buttons_to_id(root_control: Control, id: int):
	if root_control is BaseButton:
		set_focus_id(root_control, id)
	for child in root_control.get_children():
		if child is Control:
			set_control_buttons_to_id(child, id)

static func set_all_controls_to_id(root_node: Node, id: int):
	if root_node is Control:
		set_focus_id(root_node, id)
	for child in root_node.get_children():
		if child is Control:
			set_control_buttons_to_id(child, id)

