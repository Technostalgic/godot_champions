@tool
class_name GameTooltip
extends Node2D

@export var background: NinePatchSprite2D = null
@export var label: Label2D = null
@export var label_line_spacing: float = 1
@export var background_margin: float = 2
@export var size: Vector2 = Vector2(32, 32) :
	set(value):
		size = value
		set_dirty_size()

@export var snap_size_to_pixel: bool = true
@export var anim_speed: float = 1

var _size_dirty: bool = true
var _anim_delta: float = -1
var _anim_target_size: Vector2 = Vector2.ZERO

func _ready() -> void:
	if _size_dirty:
		update_size()

func _process(delta: float) -> void:
	if _anim_delta >= 0:
		handle_animation(delta)

func find_target_height() -> float:
	var lines := label.label.get_line_count()
	var height := lines * label.label.get_line_height()
	var space := label_line_spacing * maxf(2, lines)
	return height * label.label_scale + space

func set_dirty_size() -> void:
	if !_size_dirty:
		update_size.call_deferred()
	_size_dirty = true

func update_size() -> void:
	if !_size_dirty:
		return
	if snap_size_to_pixel:
		label.label_size = (size * label.label_scale).floor() / label.label_scale
	else:
		label.label_size = size
	
	var bg_size := size + Vector2(background_margin, background_margin) * 2
	var min_width := (background.margin_left + background.margin_right) * background.texture_scale
	var min_height := (background.margin_top + background.margin_bottom) * background.texture_scale
	bg_size.x = maxf(bg_size.x, min_width)
	bg_size.y = maxf(bg_size.y, min_height)
	
	background.size = bg_size
	_size_dirty = false

func handle_animation(delta: float) -> void:
	_anim_delta += delta * anim_speed
	if _anim_delta >= 1:
		size.x = _anim_target_size.x
		size.y = _anim_target_size.y
		update_size()
		_anim_delta = -1
		modulate.a = 1
		return
	
	modulate.a = _anim_delta
	var f_size = Vector2.ZERO
	f_size.x = smoothstep(0, 1, minf(_anim_delta * 2, 1)) * _anim_target_size.x
	f_size.y = smoothstep(0, 1, clampf(_anim_delta * 2 - 1, 0, 1)) * _anim_target_size.y
	size = f_size

func play_open_animation(use_target_height: bool) -> void:
	_anim_target_size = size
	if use_target_height:
		update_size()
		_anim_target_size.y = find_target_height()
	_anim_delta = 0
	modulate.a = 0
