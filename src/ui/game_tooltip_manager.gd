class_name GameTooltipManagerClass
extends Node

class TrackedTipInfo:
	static func create(tooltip: GameTooltip) -> TrackedTipInfo:
		var r := TrackedTipInfo.new()
		r.tip = tooltip
		return r
	var tip: GameTooltip = null
	var follow_node: Node2D = null
	var offset_dir: Vector2 = Vector2.ZERO
	var offset_pos: Vector2 = Vector2.ZERO

@export var base_tooltip_prefab: PackedScene = null

var _base_tooltip: GameTooltip = null :
	get:
		if _base_tooltip == null:
			_base_tooltip = base_tooltip_prefab.instantiate() as GameTooltip
		return _base_tooltip

var _tooltip_pool: Array[GameTooltip] = []

# Dictionary<int, TrackedTipInfo>
var _active_tooltip_map: Dictionary = {}

func _process(delta: float) -> void:
	_handle_tracked_tooltips()

func _create_tooltip() -> GameTooltip:
	return _base_tooltip.duplicate()

func _handle_tracked_tooltips():
	if Global.camera == null:
		return
	var cam_zoom := Global.camera.zoom as Vector2
	var scale := 5.0
	for id in _active_tooltip_map:
		var info := _active_tooltip_map[id] as TrackedTipInfo
		info.tip.scale.x = scale / cam_zoom.x
		info.tip.scale.y = scale / cam_zoom.y
		var heros := Global.heros as Array[Hero]
		var follow_pos := Vector2.ZERO
		if is_instance_valid(info.follow_node):
			follow_pos = info.follow_node.global_position
		var off_dir := info.offset_dir
		var off := Vector2(
			(info.tip.size.x * off_dir.x * 0.5 + info.offset_pos.x) * info.tip.scale.x, 
			(info.tip.size.y * off_dir.y * 0.5 + info.offset_pos.y) * info.tip.scale.y
		)
		info.tip.global_position = follow_pos + off

func pick_tooltip() -> GameTooltip:
	if _tooltip_pool.is_empty():
		return _create_tooltip()
	return _tooltip_pool.pop_back()

func pool_tooltip(tooltip: GameTooltip) -> void:
	if tooltip.is_inside_tree():
		tooltip.get_parent().remove_child(tooltip)
	_tooltip_pool.push_back(tooltip)

func show_tracked_tooltip(
	id: int, 
	text: String, 
	width: float, 
	force: bool = false
) -> TrackedTipInfo:
	if !_active_tooltip_map.has(id):
		_active_tooltip_map[id] = TrackedTipInfo.create(pick_tooltip())
	var info := _active_tooltip_map[id] as TrackedTipInfo
	if info.tip.is_inside_tree():
		return info
	else:
		info.tip.modulate.a = 0
		Global.root_2d_world.add_child(info.tip)
	info.tip.label.text = text
	info.tip.size.x = width
	info.tip.update_size()
	info.tip.play_open_animation(true)
	return info

func hide_tracked_tooltip(id: int) -> void:
	if !_active_tooltip_map.has(id):
		return
	var info := _active_tooltip_map[id] as TrackedTipInfo
	if info.tip.is_inside_tree():
		info.tip.get_parent().remove_child.call_deferred(info.tip)
