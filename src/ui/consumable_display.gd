class_name ConsumableDisplay
extends Container

@export var bar_anchors: Control = null
@export var value_text: Label = null
@export var max_text: Label = null

var last_recieved_val: float = -1
var last_recieved_max: float = -1
var current_value: float = 0
var current_max: float = 0

func _process(_delta):
	var update_bar := false
	if last_recieved_val != current_value:
		last_recieved_val = current_value
		update_bar = true
		if value_text != null:
			value_text.text = String.num(current_value, 0)
	if last_recieved_max != current_max:
		last_recieved_max = current_max
		update_bar = true
		if max_text != null:
			max_text.text = String.num(current_max, 0)
	if bar_anchors != null && update_bar:
		bar_anchors.anchor_right = get_percent()

func get_percent() -> float:
	if current_max <= 0:
		return 0
	if current_value >= current_max:
		return 1
	return current_value / current_max
