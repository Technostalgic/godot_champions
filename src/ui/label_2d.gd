@tool
class_name Label2D
extends Node2D

enum Alignment{
	FIRST = HORIZONTAL_ALIGNMENT_LEFT,
	CENTER = HORIZONTAL_ALIGNMENT_CENTER,
	LAST = HORIZONTAL_ALIGNMENT_RIGHT
}

@export var label: Label = null

var _label_size: Vector2 = Vector2(50, 16)

@export var text: String :
	get: 
		if label != null:
			return label.text
		return ""
	set(value):
		if label != null:
			label.text = value
			apply_label_transform()
@export var label_size: Vector2 = Vector2(50, 16) :
	get: return _label_size
	set(value):
		_label_size = value
		apply_label_transform()
@export var space_align_horizontal: Alignment = Alignment.CENTER :
	set(value):
		space_align_horizontal = value
		apply_label_transform()
@export var space_align_vertical: Alignment = Alignment.CENTER :
	set(value):
		space_align_vertical = value
		apply_label_transform()
@export var label_scale: float = 0.25 :
	set(value):
		label_scale = value
		apply_label_transform()

var local_top_left: Vector2 :
	get():
		var vec = Vector2.ZERO
		match space_align_horizontal:
			Alignment.FIRST: vec.x = 0
			Alignment.CENTER: vec.x = -_label_size.x / label_scale * 0.5
			Alignment.LAST: vec.x = -_label_size.x / label_scale
		match space_align_vertical:
			Alignment.FIRST: vec.y = 0
			Alignment.CENTER: vec.y = -_label_size.y / label_scale * 0.5
			Alignment.LAST: vec.y = -_label_size.y / label_scale
		return vec

func apply_label_transform() -> void:
	if label == null:
		return
	label.custom_minimum_size = Vector2.ZERO
	label.size = label_size / label_scale
	_label_size = label.size * label_scale
	label.scale.x = label_scale
	label.scale.y = label_scale
	label.pivot_offset = Vector2.ZERO
	label.position = local_top_left * label_scale
