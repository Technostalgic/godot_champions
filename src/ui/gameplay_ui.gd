class_name GameplayUI
extends Control

@export var player_info_prefab: PackedScene = null
@export var reticle_prefab: PackedScene = null
@export var player_info_container: Container = null
@export var wep_ui_container: GridContainer = null
@export var reticle_container: Control = null
@export var wave_info: WaveProgressBar = null
@export var pause_screen: Control = null

func create_player_infos():
	for child in player_info_container.get_children():
		child.queue_free()
	
	var last_info: PlayerInfoDisplay = null
	for hero in Global.heros:
		var info = player_info_prefab.instantiate() as PlayerInfoDisplay
		info.actor_target = hero
		player_info_container.add_child(info)
		last_info = info
	
	# the last info should not expand it's spacing
	if last_info != null:
		last_info.size_flags_horizontal = Control.SIZE_SHRINK_BEGIN
		pass

func is_complete_wave_popup_open() -> bool:
	return wave_info.complete_popup.is_visible_in_tree()

func open_unused_weapon_ui(player_id: int) -> WeaponUI:
	var wep_ui: WeaponUI = null
	
	for child in wep_ui_container.get_children():
		if child is WeaponUI:
			if !child.visible:
				wep_ui = child
				break
	
	wep_ui.player_id = player_id
	wep_ui.show()
	organize_wep_ui_container()
	return wep_ui

func organize_wep_ui_container():
	var active_count: int = 0
	for child in wep_ui_container.get_children():
		if child is WeaponUI:
			if child.visible:
				active_count += 1
	if active_count <= 0:
		wep_ui_container.get_parent().hide()
	else:
		wep_ui_container.get_parent().show()
	if active_count > 1:
		wep_ui_container.columns = 2
	else:
		wep_ui_container.columns = 1

func create_reticles():
	for hero in Global.heros:
		var reticle = reticle_prefab.instantiate() as Reticle
		reticle.weapon_slot = hero.weapon_slot
		reticle.shot_cursor.modulate = hero.color
		reticle.shot_cursor.modulate.a = 1
		reticle_container.add_child(reticle)

func open_pause_overlay(open: bool = true):
	
	if open:
		pause_screen.show()
	
	else:
		pause_screen.hide()
