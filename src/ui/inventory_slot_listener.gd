class_name InventorySlotListener
extends ModSlotListener

@export var weapon_properties: WeaponUI = null

var _all_items: Array[Item] = []
var _items_dirty: bool = false

func get_inventory():
	var hero = weapon_properties.target_actor
	return Inventory.find_inventory_child(hero)

func _on_item_given(item: Item):
	super._on_item_given(item)
	_items_dirty = true

func _on_item_taken(item: Item):
	super._on_item_taken(item)
	_items_dirty = true

func _process(_delta):
	if _items_dirty:
		retrieve_items_from_slots()

func create_slots_from_inventory(slot_prefab: PackedScene, player_id: int):
	for child in get_children():
		if child is ItemSlot:
			child._item = null
			child.queue_free()
	for item in get_inventory().get_items():
		var slot = slot_prefab.instantiate()
		if slot is ItemSlot:
			PlayerFocusedControl.set_focus_id(slot, player_id)
			add_child(slot)
			slot.set_item(item)
		else:
			push_error("Supplied prefab is not an item slot")
			return
	on_slots_full.emit()

func retrieve_items_from_slots():
	if get_inventory() == null:
		return
	_all_items.clear()
	for item_slot in get_children():
		if item_slot is ItemSlot:
			if item_slot._item != null:
				_all_items.push_back(item_slot._item)
	get_inventory().set_items(_all_items)
	_items_dirty = false

static func search_recursive_inventory(node: Node) -> Inventory:
	if node == null:
		return null
	if node is Inventory:
		return node
	var target: Inventory = null
	for child in node.get_children():
		target = search_recursive_inventory(child)
		if target != null:
			break
	return target
