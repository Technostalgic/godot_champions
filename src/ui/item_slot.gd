class_name ItemSlot
extends BaseButton

# emitted when the button is pressed and passes itself as a parameter
signal pressed_self(slot_self: ItemSlot)
signal on_item_taken(item: Item)
signal on_item_given(item: Item)

@export var ui_image: TextureRect
@export var ui_label: Label
@export var ui_count_label: Label

var _item: Item = null
var _parent_listener: ModSlotListener = null

func _enter_tree():
	_parent_listener = find_parent_listener()
	if _parent_listener != null:
		on_item_taken.connect(_parent_listener._on_item_taken)
		on_item_given.connect(_parent_listener._on_item_given)
	refresh_item_ui()
	focus_entered.connect(_on_focus_entered)
	focus_exited.connect(_on_focus_exit)
	mouse_entered.connect(_on_focus_entered)
	mouse_exited.connect(_on_focus_exit)

func _exit_tree():
	focus_entered.disconnect(_on_focus_entered)
	focus_exited.disconnect(_on_focus_exit)
	mouse_entered.disconnect(_on_focus_entered)
	mouse_exited.disconnect(_on_focus_exit)

func _on_focus_entered():
	if is_instance_valid(_item):
		InfoPopup.try_info_popup(self)

func _on_focus_exit():
	InfoPopup.instance.hide()

func _on_pressed_self():
	pressed_self.emit(self)

func _on_press():
	activate()

func activate():
	_on_pressed_self()
	if _item != null:
		drag_take_item()
	else:
		drag_give_item()

func is_empty() -> bool:
	return !is_instance_valid(_item)

func find_parent_listener(node: Node = self) -> ModSlotListener:
	if node is ModSlotListener:
		return node
	var parent = node.get_parent()
	if parent != null:
		return find_parent_listener(parent)
	return null

func drag_give_item():
	var drag: = find_parent_drag_container()
	if drag != null:
		drag.end_dragging(self)

func drag_take_item():
	var drag := find_parent_drag_container()
	if drag != null:
		if drag.is_dragging():
			drag.end_dragging(self)
		else:
			drag.start_dragging(_item, self)

func set_item(item: Item):
	if item == null:
		if _item != null:
			on_item_taken.emit(_item)
			_item = null
			refresh_item_ui()
			return
	if _item != null:
		if _item is WeaponMod && item is WeaponMod:
			var did_combine := (item as WeaponMod).try_combine_with(_item)
			if did_combine:
				_item.level = 0
				_item.queue_free()
			else:
				on_item_taken.emit(_item)
		else:
			on_item_taken.emit(_item)
		_item = item
		if _item.get_parent() == null:
			add_child(_item)
		on_item_given.emit(_item)
		refresh_item_ui()
		return
	_item = item
	if is_instance_valid(_item) && _item.get_parent() == null:
		add_child(_item)
	on_item_given.emit(_item)
	refresh_item_ui()

func find_parent_drag_container(node: Node = self) -> ItemDragContainer:
	if node is ItemDragContainer:
		return node
	var parent: = node.get_parent()
	if parent != null:
		return find_parent_drag_container(parent)
	return null

func refresh_item_ui():
	var texture: Texture2D = null
	var text: String = ""
	if _item != null:
		text = _item.title
		texture = _item.icon
		var stack_size := _item.get_stack_size()
		if stack_size != 1:
			ui_count_label.text = str(_item.get_stack_size())
			ui_count_label.show()
		else:
			ui_count_label.hide()
	else:
		ui_count_label.hide()
	ui_image.texture = texture
	ui_label.text = text
