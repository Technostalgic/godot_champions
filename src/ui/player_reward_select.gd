class_name PlayerReward
extends Container

@export var parent_reward_select: RewardSelection = null
@export var player_id: int = 0
@export var mod_rewards: Container = null
@export var consumable_rewards: Container = null
@export var reroll_btn: Button = null
@export var mod_slot_count: int = 3
@export var item_slot_count: int = 2

var target_inventory: Inventory = null

func _ready():
	mod_rewards.child_entered_tree.connect(_on_child_entered_tree)
	consumable_rewards.child_entered_tree.connect(_on_child_entered_tree)
	reroll_btn.pressed.connect(reroll_selection)

func _on_child_entered_tree(node: Node):
	if node is ItemSlot:
		node.pressed_self.connect(_on_child_slot_pressed)

func _on_child_slot_pressed(child_slot: ItemSlot):
	var item := child_slot._item
	if is_instance_valid(item):
		target_inventory.add_item(item)
	if child_slot.get_parent() == mod_rewards:
		for slot in mod_rewards.get_children():
			if slot is ItemSlot:
				slot.set_item(null)
	elif child_slot.get_parent() == consumable_rewards:
		for slot in consumable_rewards.get_children():
			if slot is ItemSlot:
				slot.set_item(null)
	check_reroll_available()
	
	var empty = true
	for slot in mod_rewards.get_children():
		if slot is ItemSlot:
			if !slot.is_empty():
				empty = false
				break
	for slot in consumable_rewards.get_children():
		if slot is ItemSlot:
			if !slot.is_empty():
				empty = false
				break
	if empty:
		queue_free()
		Global.heros[player_id].input_behavior.ui_mode = false

func reroll_selection():
	for slot in mod_rewards.get_children():
		if slot is ItemSlot:
			if !slot.is_empty():
				slot._item.queue_free()
				slot.set_item(null)
				var prefab = parent_reward_select.mod_reward_pool.pick_random()
				slot.set_item(prefab.instantiate() as Item)
	for slot in consumable_rewards.get_children():
		if slot is ItemSlot:
			if !slot.is_empty():
				slot._item.queue_free()
				slot.set_item(null)
				var prefab = parent_reward_select.item_reward_pool.pick_random()
				slot.set_item(prefab.instantiate() as Item)
	target_inventory.gold -= RewardSelection.REROLL_COST
	check_reroll_available()

func check_reroll_available():
	PlayerFocusedControl.set_focus_id(reroll_btn, player_id)
	if target_inventory.gold < RewardSelection.REROLL_COST:
		reroll_btn.disabled = true
		return
	for slot in mod_rewards.get_children():
		if slot is ItemSlot:
			if !slot.is_empty():
				reroll_btn.disabled = false
				return
	for slot in consumable_rewards.get_children():
		if slot is ItemSlot:
			if !slot.is_empty():
				reroll_btn.disabled = false
				return
	reroll_btn.disabled = true

func create_random_rewards():
	for child in mod_rewards.get_children():
		if child is ItemSlot:
			child.queue_free()
	for child in consumable_rewards.get_children():
		if child is ItemSlot:
			child.queue_free()
	for i in range(0, mod_slot_count):
		var reward = parent_reward_select.mod_reward_pool.pick_random().instantiate() as Item
		var slot = parent_reward_select.create_slot(reward)
		PlayerFocusedControl.set_focus_id(slot, player_id)
		mod_rewards.add_child(slot)
	for i in range(0, item_slot_count):
		var reward = parent_reward_select.item_reward_pool.pick_random().instantiate() as Item
		var slot = parent_reward_select.create_slot(reward)
		PlayerFocusedControl.set_focus_id(slot, player_id)
		consumable_rewards.add_child(slot)
	check_reroll_available()
