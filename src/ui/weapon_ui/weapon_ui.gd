class_name WeaponUI
extends Control

signal on_open()
signal on_close()

@export var target_actor: Actor = null
@export var mod_slot_prefab: PackedScene = null
@export var weapon_stats_ui: WeaponStatsUI = null
@export var mod_firemode_tab_ui: TabBar = null
@export var usable_mod_slots_ui: InventorySlotListener = null
@export var weapon_mod_slots_ui_1: WeaponContainerSlotListener = null
@export var weapon_mod_slots_ui_2: WeaponContainerSlotListener = null

var player_id: int = 0

func _enter_tree():
	usable_mod_slots_ui.on_slots_full.connect(add_usable_mod_slot)

func _on_visibilty_changed():
	target_actor = Global.heros[player_id]
	if visible:
		Global.suspend_gameplay_input = true
		_on_open()
	else:
		Global.suspend_gameplay_input = false
		_on_close()

func _on_open():
	var wep = get_weapon()
	if wep != null:
		if wep.wep_owner is Actor:
			target_actor = wep.wep_owner
		if wep is ModableWeapon:
			wep.lock_mod_mode = true
			if !wep.mods_changed.is_connected(weapon_stats_ui.refresh_stat_display):
				wep.mods_changed.connect(weapon_stats_ui.refresh_stat_display)
			refresh_mod_slots()
	if target_actor != null:
		usable_mod_slots_ui.create_slots_from_inventory(mod_slot_prefab, player_id)
	on_open.emit()

func _on_close():
	var wep = get_weapon()
	if wep != null:
		wep.lock_mod_mode = false
	on_close.emit()

func get_weapon() -> Weapon:
	if target_actor is Hero:
		return target_actor.weapon_slot.get_equipped_weapons()[0]
	return null

func is_primary_mode() -> bool:
	if mod_firemode_tab_ui != null:
		return mod_firemode_tab_ui.current_tab == 0
	return true

func add_usable_mod_slot():
	usable_mod_slots_ui.add_child(create_mod_slot())

func swap_alt_mod_tab():
	var des_tab = mod_firemode_tab_ui.current_tab + 1
	if des_tab >= mod_firemode_tab_ui.tab_count:
		des_tab = 0
	mod_firemode_tab_ui.current_tab = des_tab

func refresh_mod_slots():
	weapon_mod_slots_ui_1.fill_slots()
	weapon_mod_slots_ui_2.fill_slots()

func create_mod_slot() -> ItemSlot:
	var slot = mod_slot_prefab.instantiate()
	if slot is ItemSlot:
		PlayerFocusedControl.set_focus_id(slot, player_id)
		return slot
	return null
