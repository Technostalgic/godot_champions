class_name FireModeTab
extends TabBar

@export var weapon_ui: WeaponUI = null
@export var label: Label = null

func _enter_tree():
	tab_changed.connect(_on_tab_change)
	weapon_ui.on_close.connect(reset_to_primary_tab)

func _exit_tree():
	tab_changed.disconnect(_on_tab_change)
	weapon_ui.on_close.disconnect(reset_to_primary_tab)

func _on_tab_change(tab: int):
	weapon_ui.get_weapon().set_mod_mode(tab > 0)
	weapon_ui.weapon_stats_ui.refresh_stat_display()
	if label != null:
		if weapon_ui.get_weapon().using_alt_mods:
			label.text = "Secondary Slots:"
		else:
			label.text = "Primary Slots:"

func reset_to_primary_tab():
	current_tab = 0
