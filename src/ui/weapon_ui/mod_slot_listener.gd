class_name ModSlotListener
extends Control

signal on_child_mod_taken(item: Item)
signal on_child_mod_given(item: Item)
signal on_slots_full()

func _on_item_taken(item: Item):
	on_child_mod_taken.emit(item)
	
func _on_item_given(item: Item):
	on_child_mod_given.emit(item)
	var full := true
	for slot in get_children():
		if slot is ItemSlot:
			if slot._item == null:
				full = false
				break
	if full:
		on_slots_full.emit()
