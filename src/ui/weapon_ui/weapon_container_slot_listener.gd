class_name WeaponContainerSlotListener
extends ModSlotListener

@export var weapon_properties: WeaponUI = null
@export var is_alt_mods: bool = false

var _all_mods: Array[WeaponMod]
var _mods_dirty: bool = false

func _on_item_taken(item: Item):
	retrieve_mods_from_slots()
	super._on_item_taken(item)
	_mods_dirty = true

func _on_item_given(item: Item):
	if !(item is WeaponMod):
		push_error("Item is not a weapon mod")
		return
	retrieve_mods_from_slots()
	super._on_item_given(item)
	_mods_dirty = true

func _process(_delta: float):
	if _mods_dirty:
		retrieve_mods_from_slots()
		var wep = weapon_properties.get_weapon() as ModableWeapon
		if is_instance_valid(wep):
			_mods_dirty = false
			var container: WeaponModContainer = wep.mod_container_1
			if is_alt_mods:
				container = wep.mod_container_2
			container.set_mods(_all_mods)

func clear_mods():
	_all_mods.clear()

func fill_slots() -> void:
	clear_mods()
	for slot in get_children():
		if slot is ItemSlot:
			slot._item = null
			slot.queue_free()
	var wep := weapon_properties.get_weapon() as ModableWeapon
	if is_instance_valid(wep):
		var slots_added: int = 0
		var max_slots := wep.get_weapon_stats().mod_slots
		var container := wep.mod_container_1 if !is_alt_mods else wep.mod_container_2
		for mod in container.mods:
			var slot := weapon_properties.create_mod_slot()
			add_child(slot)
			slot.set_item(mod)
			slots_added += 1
		while slots_added < max_slots:
			add_child(weapon_properties.create_mod_slot())
			slots_added += 1

func retrieve_mods_from_slots():
	_all_mods.clear()
	for slot in get_children():
		if slot is ItemSlot:
			if slot._item != null:
				_all_mods.push_back(slot._item)
