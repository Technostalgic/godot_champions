class_name WeaponPropertyControls
extends Container

@export var weapon_ui: WeaponUI = null
@export var weapon_label: Label = null
@export var weapon_image: TextureRect = null
@export var power_stat_container: Container = null
@export var stat_display_prefab: PackedScene = null
@export var fire_mode_selection: Button = null
@export var proj_icon: TextureRect = null
@export var proj_title: Label = null
@export var carry_primary_mods: CheckBox = null

func _ready():
	visibility_changed.connect(_on_visiblilty_changed)
	fire_mode_selection.pressed.connect(_on_alt_mode_select)
	carry_primary_mods.toggled.connect(_on_carry_toggled)

func _on_visiblilty_changed():
	if !visible:
		return
	update_controls_to_weapon()

func _on_carry_toggled(is_toggled: bool):
	var wep = weapon_ui.get_weapon()
	wep.carry_primary_mods = is_toggled
	wep.mods_changed.emit()

func _on_alt_mode_select():
	var wep := weapon_ui.get_weapon()
	if wep is ModableWeapon:
		wep.mod_switch_mode = wrapi(wep.mod_switch_mode + 1, 0, ModableWeapon.ModSwitchMode._TOTAL)
		fire_mode_selection.text = ModableWeapon.mod_switch_mode_to_str(wep.mod_switch_mode)

func update_controls_to_weapon():
	update_power_stats()
	update_wep_header()
	var wep = weapon_ui.get_weapon()
	fire_mode_selection.text = ModableWeapon.mod_switch_mode_to_str(wep.mod_switch_mode)
	var proj := wep.get_shot_stats().projectile.instantiate() as Projectile
	proj_title.text = proj.title
	proj_icon.texture = proj.texture
	proj.queue_free()
	carry_primary_mods.set_pressed_no_signal(wep.carry_primary_mods)

func update_wep_header():
	var wep = weapon_ui.get_weapon()
	var wep_gfx = wep.graphic
	if wep_gfx is Sprite2D:
		weapon_image.texture = wep_gfx.texture
	else:
		weapon_image.texture = null
	if wep is ModableWeapon:
		weapon_label.text = wep.title
	else:
		weapon_label.text = wep.name

func update_power_stats():
	for stat in power_stat_container.get_children():
		stat.queue_free()
	for mod in weapon_ui.get_weapon().power_stat_bonuses.stat_mods:
		var stat_disp := stat_display_prefab.instantiate() as StatDisplay
		var stat_str := Stats.stat_to_str(mod.stat)
		stat_disp.set_stat_str(stat_str, mod.get_modifier_value_str(1))
		power_stat_container.add_child(stat_disp)
