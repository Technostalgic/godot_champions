class_name WeaponStatsUI
extends Container

@export var weapon_ui: WeaponUI
@export var container: Container
@export var stat_ui_prefab: PackedScene
@export var divider_prefab: PackedScene

func _enter_tree():
	weapon_ui.on_open.connect(refresh_stat_display)
	
func _exit_tree():
	weapon_ui.on_open.disconnect(refresh_stat_display)

func refresh_stat_display():
	# clear current stats
	for child in container.get_children():
		child.queue_free()
		
	#create dividers for different stat types
	var div_proj := divider_prefab.instantiate() as LabelDivider
	div_proj.text_label.text = "Projectile"
	var div_shot := divider_prefab.instantiate() as LabelDivider
	div_shot.text_label.text = "Shot"
	var div_wep := divider_prefab.instantiate() as LabelDivider
	div_wep.text_label.text = "Weapon"
	
	# create the stat display prefabs and set their display properties
	var prev_mode: bool = weapon_ui.get_weapon().using_alt_mods
	weapon_ui.get_weapon().set_mod_mode(!weapon_ui.is_primary_mode())
	var stats_wep := weapon_ui.get_weapon().get_weapon_stats()
	var stats_shot := weapon_ui.get_weapon().get_shot_stats()
	weapon_ui.get_weapon().set_mod_mode(prev_mode)
	
	var dmg := stat_ui_prefab.instantiate() as StatDisplay
	dmg.set_stat_value("Damage", stats_shot.projectile_stats.damage)
	var kb := stat_ui_prefab.instantiate() as StatDisplay
	kb.set_stat_value("Knockback", stats_shot.projectile_stats.knockback)
	var aoe_size := stat_ui_prefab.instantiate() as StatDisplay
	aoe_size.set_stat_value("AOE Size", stats_shot.projectile_stats.aoe_size)
	var aoe_dmg := stat_ui_prefab.instantiate() as StatDisplay
	aoe_dmg.set_stat_value("AOE Damage", stats_shot.projectile_stats.aoe_damage)
	var damp := stat_ui_prefab.instantiate() as StatDisplay
	damp.set_stat_value("Damping", stats_shot.projectile_stats.damping)
	
	var fire_mode := stat_ui_prefab.instantiate() as StatDisplay
	fire_mode.set_stat_str("Fire Mode", Stats.fire_mode_to_str(stats_shot.fire_mode))
	var powr := stat_ui_prefab.instantiate() as StatDisplay
	powr.set_stat_value("Power", stats_shot.power)
	var crit_chc := stat_ui_prefab.instantiate() as StatDisplay
	crit_chc.set_stat_value("Crit Chance", stats_shot.crit_chance)
	var cooldown := stat_ui_prefab.instantiate() as StatDisplay
	cooldown.set_stat_value("Cooldown", stats_shot.cooldown)
	var spread := stat_ui_prefab.instantiate() as StatDisplay
	spread.set_stat_value("Spread", stats_shot.spread)
	var speed := stat_ui_prefab.instantiate() as StatDisplay
	speed.set_stat_value("Speed", stats_shot.projectile_speed)
	var speed_var := stat_ui_prefab.instantiate() as StatDisplay
	speed_var.set_stat_value("Speed Var", stats_shot.speed_variance)
	var count := stat_ui_prefab.instantiate() as StatDisplay
	count.set_stat_value("Count", stats_shot.projectile_count)
	var fire_delay := stat_ui_prefab.instantiate() as StatDisplay
	fire_delay.set_stat_value("Fire Delay", stats_shot.fire_delay)
	var recoil := stat_ui_prefab.instantiate() as StatDisplay
	recoil.set_stat_value("Recoil", stats_shot.recoil)
	var ammo_cost := stat_ui_prefab.instantiate() as StatDisplay
	ammo_cost.set_stat_value("Ammo Cost", stats_shot.ammo_cost)
	var enr_cost := stat_ui_prefab.instantiate() as StatDisplay
	enr_cost.set_stat_value("Energy Cost", stats_shot.energy_cost)
	
	var fire_rate := stat_ui_prefab.instantiate() as StatDisplay
	fire_rate.set_stat_value("Fire Rate", stats_wep.fire_rate)
	var crit_mult := stat_ui_prefab.instantiate() as StatDisplay
	crit_mult.set_stat_value("Crit Multiply", stats_wep.crit_mult)
	var mag := stat_ui_prefab.instantiate() as StatDisplay
	mag.set_stat_value("Magazine", stats_wep.magazine_size)
	var reload := stat_ui_prefab.instantiate() as StatDisplay
	reload.set_stat_value("Reload", stats_wep.reload_time)
	var mod_slots := stat_ui_prefab.instantiate() as StatDisplay
	mod_slots.set_stat_value("Mod Slots", stats_wep.mod_slots)
	var max_ammo := stat_ui_prefab.instantiate() as StatDisplay
	max_ammo.set_stat_value("Max Ammo", stats_wep.max_ammo)
	var rchrg := stat_ui_prefab.instantiate() as StatDisplay
	rchrg.set_stat_value("Recharge", stats_wep.energy_regeneration)
	var enr_max := stat_ui_prefab.instantiate() as StatDisplay
	enr_max.set_stat_value("Max Energy", stats_wep.max_energy)
	
	# add stat displays into the stat container
	container.add_child(div_proj)
	container.add_child(dmg)
	container.add_child(kb)
	container.add_child(aoe_dmg)
	container.add_child(aoe_size)
	container.add_child(damp)
	
	container.add_child(div_shot)
	container.add_child(fire_mode)
	container.add_child(powr)
	container.add_child(crit_chc)
	container.add_child(cooldown)
	container.add_child(spread)
	container.add_child(speed)
	container.add_child(speed_var)
	container.add_child(count)
	container.add_child(fire_delay)
	container.add_child(recoil)
	container.add_child(ammo_cost)
	container.add_child(enr_cost)
	
	container.add_child(div_wep)
	container.add_child(fire_rate)
	container.add_child(crit_mult)
	container.add_child(mag)
	container.add_child(reload)
	container.add_child(mod_slots)
	container.add_child(max_ammo)
	container.add_child(rchrg)
	container.add_child(enr_max)
