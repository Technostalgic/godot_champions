class_name ItemDragContainer
extends Control

@export var drag_icon: TextureRect = null
@export var drag_container: Node = null

var dragged_item: Node = null
var dragged_from: Control = null

func _process(_delta):
	if is_dragging():
		drag_icon.global_position = get_global_mouse_position()

func is_dragging():
	return dragged_item != null

func set_dragged_item(item: Node, from: Control):
	dragged_item = item
	dragged_from = from
	if item.get_parent() != null:
		item.reparent(drag_container, false)
	if dragged_item is WeaponMod:
		drag_icon.texture = dragged_item.icon

func start_dragging(item: Node, from: Control):
	set_dragged_item(item, from)
	if dragged_from is ItemSlot:
		dragged_from.set_item(null)

func end_dragging(to: Control):
	if !is_dragging():
		return
	if dragged_item is WeaponMod:
		if to is ItemSlot:
			var exchange_item: Node = null
			if to._item != null:
				exchange_item = to._item
			to.set_item(dragged_item)
			if exchange_item is WeaponMod:
				if exchange_item.level > 0:
					set_dragged_item(exchange_item, to)
				else: 
					dragged_item = null
					dragged_from = null
					drag_icon.texture = null
			elif is_instance_valid(exchange_item):
				set_dragged_item(exchange_item, to)
			else:
				dragged_item = null
				dragged_from = null
				drag_icon.texture = null
