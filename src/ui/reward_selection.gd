class_name RewardSelection
extends Control

const REROLL_COST: int = 25

@export var slot_prefab: PackedScene
@export var player_reward_prefab: PackedScene
@export var player_reward_container: Container

var mod_reward_pool: LootPool = null
var item_reward_pool: LootPool = null

func open():
	Global.set_input_ui_mode_all_players()
	create_random_rewards()

func create_slot(item: Item) -> ItemSlot:
	var slot = slot_prefab.instantiate()
	if slot is ItemSlot:
		slot.set_item(item)
		return slot
	push_error("Prefab is not an item slot ui")
	return null

func create_random_rewards():
	for child in player_reward_container.get_children():
		if child is PlayerReward:
			child.queue_free()
			
	for hero in Global.heros:
		var plr_reward = player_reward_prefab.instantiate() as PlayerReward
		plr_reward.parent_reward_select = self
		plr_reward.player_id = hero.player_id
		plr_reward.target_inventory = Inventory.find_inventory_child(hero)
		player_reward_container.add_child(plr_reward)
		plr_reward.create_random_rewards()
