class_name WeaponSelect
extends Container

@export var target_actor: Actor = null
@export var item_slot_prefab: PackedScene = null
@export var weapon_prefabs: Array[PackedScene] = []
@export var slot_container: Container = null
@export var start_button: Button = null

func _ready():
	create_weapon_select_controls()
	visibility_changed.connect(_on_visibility_changed)
	_on_visibility_changed()
	start_button.pressed.connect(_on_start_button_press)
	WaveState.paused = true
	show()

func _on_visibility_changed():
	if is_visible_in_tree():
		Global.suspend_gameplay_input = true
	else:
		Global.suspend_gameplay_input = false

func _on_start_button_press():
	WaveState.paused = false
	hide()

func _on_weapon_press():
	var wep_index := 0
	for child in slot_container.get_children():
		if child is Control:
			if child.has_focus():
				break
		wep_index += 1
	var wep := weapon_prefabs[wep_index].instantiate() as Weapon
	var wep_slot: WeaponSlot = null
	for child in target_actor.get_children():
		if child is WeaponSlot:
			wep_slot = child
	if wep_slot != null:
		for child in wep_slot.get_children():
			child.queue_free()
	wep_slot.add_child(wep)

func create_weapon_select_controls():
	for child in slot_container.get_children():
		child.queue_free()
	for wep in weapon_prefabs:
		var slot := item_slot_prefab.instantiate() as Button
		slot.get_child(1).text = wep.get_state().get_node_name(0)
		slot.pressed.connect(_on_weapon_press)
		slot_container.add_child(slot)
