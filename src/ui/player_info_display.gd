class_name PlayerInfoDisplay
extends Container

@export var actor_target: Actor = null
@export var health: ConsumableDisplay = null
@export var energy: ConsumableDisplay = null
@export var clip: ConsumableDisplay = null
@export var ammo: ConsumableDisplay = null
@export var gold_value_label: Label = null

var inventory: Inventory = null
var wep_slot: WeaponSlot = null
var health_behavior: Health = null

func _ready():
	if !is_instance_valid(actor_target):
		actor_target = Global.heros[0]
	wep_slot = PlayerInfoDisplay.search_wep_recursive(actor_target)
	health_behavior = PlayerInfoDisplay.search_health_recursive(actor_target)
	inventory = PlayerInfoDisplay.search_inv_recursive(actor_target)

func _process(_delta):
	if actor_target is Hero:
		self_modulate = actor_target.color
	if wep_slot != null:
		var wep := wep_slot.get_equipped_weapons()[0]
		if wep != null:
			var stats := wep.get_weapon_stats()
			clip.current_max = stats.magazine_size
			ammo.current_max = stats.max_ammo
			energy.current_max = stats.max_energy
			if wep is ModableWeapon:
				clip.current_value = wep.ammo_in_magazine
				ammo.current_value = wep.current_ammo
				energy.current_value = wep.current_energy
	if health_behavior != null:
		health.current_max = health_behavior.max_health
		health.current_value = health_behavior.health
	if is_instance_valid(actor_target):
		gold_value_label.text = str(inventory.gold)

static func search_wep_recursive(node: Node) -> WeaponSlot:
	var target: WeaponSlot = null
	for child in node.get_children():
		if child is WeaponSlot:
			target = child
		else: 
			target = search_wep_recursive(child)
		if target != null:
			break
	return target

static func search_health_recursive(node: Node) -> Health:
	var target: Health = null
	for child in node.get_children():
		if child is Health:
			target = child
		else: 
			target = search_health_recursive(child)
		if target != null:
			break
	return target

static func search_inv_recursive(node: Node) -> Inventory:
	var target: Inventory = null
	for child in node.get_children():
		if child is Inventory:
			target = child
		else: 
			target = search_inv_recursive(child)
		if target != null:
			break
	return target
