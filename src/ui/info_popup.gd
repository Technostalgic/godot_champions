class_name InfoPopup
extends Control

@export var title: Label = null
@export var icon: TextureRect = null
@export var count_text: Label = null
@export var description: Label = null
@export var stat_container: Container = null
@export var stat_container_root: Control = null
@export var stat_ui_prefab: PackedScene = null

static var instance: InfoPopup = null
var focus_control: Control = null

func _init():
	if is_instance_valid(InfoPopup.instance):
		InfoPopup.instance.queue_free()
	InfoPopup.instance = self

func show_info(item_slot: ItemSlot, pos: Vector2):
	focus_control = item_slot
	update_header_ui(item_slot._item)
	update_stats(item_slot._item)
	show()
	reset_size()
	stat_container.reset_size()
	set_global_position(pos)
	fit_inside_screen()

static func try_info_popup(control: Control):
	if InfoPopup.instance.focus_control == control && InfoPopup.instance.visible:
		InfoPopup.instance.hide()
		InfoPopup.instance.focus_control = null
		return
	var slot_rect = control.get_global_rect()
	var info_rect = InfoPopup.instance.get_global_rect()
	var offset = slot_rect.size * Vector2.RIGHT - info_rect.size * Vector2.DOWN
	if control is ItemSlot:
		InfoPopup.instance.show_info(control, slot_rect.position + offset)
		return
	
	if InfoPopup.instance.visible:
		InfoPopup.instance.hide()
		InfoPopup.instance.focus_control = null

func fit_inside_screen():
	force_update_transform()
	var glob_rect = get_global_rect()
	var offset = Vector2.ZERO
	if glob_rect.position.x < 0:
		offset.x -= glob_rect.position.x
	if glob_rect.position.y < 0:
		offset.y -= glob_rect.position.y
	set_global_position(glob_rect.position + offset)

func update_header_ui(item: Item):
	if !is_instance_valid(item):
		title.text = "Empty"
		icon.texture = null
		description.text = "It's an ampty slot for an item"
		count_text.hide()
		return
	title.text = item.title
	icon.texture = item.icon
	var stack_size = item.get_stack_size()
	if stack_size != 1:
		count_text.text = str(stack_size)
		count_text.show()
	else:
		count_text.hide()
	description.text = item.description

func update_stats(item: Item):
	for child in stat_container.get_children():
		if child is StatDisplay:
			child.queue_free()
	if !is_instance_valid(item):
		stat_container_root.hide()
		return
	if item is WeaponMod:
		var any_mods := false
		for stat_mod: StatModifier in item.stat_mods:
			any_mods = true
			var stat_disp := create_stat_display()
			var stat_str := Stats.stat_to_str(stat_mod.stat)
			stat_container.add_child(stat_disp)
			stat_disp.set_stat_str(stat_str, stat_mod.get_modifier_value_str(item.get_stack_size()))
		for hit_effect in item.hit_effects:
			for sttr: HitEffect.StatString in hit_effect.get_stat_strings():
				any_mods = true
				var stat_disp := create_stat_display()
				stat_container.add_child(stat_disp)
				stat_disp.set_stat_str(sttr.label, sttr.value)
		for child in item.get_children():
			if child is WeaponEffect:
				for sttr in child.get_stat_string():
					any_mods = true
					var stat_disp := create_stat_display()
					stat_container.add_child(stat_disp)
					stat_disp.set_stat_str(sttr.label, sttr.value)
		if any_mods:
			stat_container_root.show()
		else:
			stat_container_root.hide()
	else:
		stat_container_root.hide()

func create_stat_display() -> StatDisplay:
	var node = stat_ui_prefab.instantiate()
	if node is StatDisplay:
		return node
	return null
